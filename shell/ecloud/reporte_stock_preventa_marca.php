<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');

$dateFrom = $_POST['dateFrom'];
$dateTo = $_POST['dateTo'];

$from = strtotime('+3 hour',strtotime($dateFrom));  
$from = date ('Y-m-d H:i:s', $from); 

$to = strtotime('+3 hour',strtotime($dateTo));
$to = date ('Y-m-d H:i:s', $to); 

$vendedores = Mage::getModel("vendedores/vendedor")->getCollection();
$orders_collection = Mage::getModel('sales/order')->getCollection()
		    ->addAttributeToFilter('created_at', array('from'=>$from , 'to'=>$to))
			->addAttributeToFilter('increment_id', array('like' => '00%'))
			->addAttributeToFilter('status', 'pending');
			// ->joinField(
			//         'postcode',
			//         'sales_flat_order_address',
			//         'postcode',
			//         'entity_id = parent_id',
			//         '{{table}}.postcode IS NOT NULL',
			//         'left');

$marcas = array();
$ventas_vendedores = array();

foreach ($orders_collection as $order) {
		
	$evaluar = 1;
	$postCode = $order->getShippingAddress()->getPostcode();
	$vendedor = $vendedores->getItemByColumnValue('cod_zona', $postCode);

	if ($vendedor) {

		$items = getItems($resource, $readConnection, $order->getEntityId());

		if (!array_key_exists($vendedor['nombre'], $ventas_vendedores)) {
			$ventas_vendedores[$vendedor['nombre']]	= array(
				'stock'=> 0,
				'preventa' => 0
			);
		}

	  	foreach ($items as $item) {
	  		$product = getProduct($resource, $readConnection, $item['sku']);
	  		$carryover 	= getCarryover($resource, $readConnection, $product['entity_id']);
	  		$inventory 	= getCatalogInventory($resource, $readConnection, $product['entity_id']);
	  		
	  		if ($product['value']) {
				if (!in_array($product['value'], $marcas)) {
					array_push($marcas, $product['value']);
				}
			}

			if ($product['value']) {
				if (array_key_exists($product['value'], $ventas_vendedores[$vendedor['nombre']])) {
					$ventas_vendedores[$vendedor['nombre']][$product['value']]	+= (int)$item['qty_ordered'];
				}else{
					$ventas_vendedores[$vendedor['nombre']][$product['value']]	= (int)$item['qty_ordered'];
				}
			}

	  		if($evaluar){
		  		if (!$carryover['value']) {
		  			if ($inventory['manage_stock']) {
		  				$stock_preventa = 'Stock';
		  			}else{
		  				$stock_preventa = 'Preventa';
		  			}
					$evaluar = 0;
					// break;
		  		}else{
		  			$stock_preventa = 'Stock';
		  		}
	  		}
		}

		if ($stock_preventa == 'Stock') {
			$ventas_vendedores[$vendedor['nombre']]['stock'] += 1;
		}else{
			$ventas_vendedores[$vendedor['nombre']]['preventa'] += 1;
		}
	}	
}

function getItems($resource, $readConnection, $order_id){
	$query = 'SELECT `item`.sku , `item`.name, `item`.price, `item`.qty_ordered FROM ' . $resource->getTableName('sales_flat_order_item').' as `item` WHERE `order_id` = ' .$order_id ;

	$results = $readConnection->fetchAll($query);

	if(count($results) > 0){
		return $results;
	} else {
		return false;
	}
}

function getProduct($resource, $readConnection, $sku) {
	$typeId = getEntityType($resource, $readConnection,'catalog_product');
	$attribute_id = getAttributeId($resource, $readConnection, 'marca', $typeId);
	$query = 'SELECT `product`.entity_id, `product`.sku , `varchar`.value  FROM ' . $resource->getTableName('catalog_product_entity') . ' as `product` INNER JOIN '.$resource->getTableName('catalog_product_entity_varchar').' as `varchar` ON `product`.`entity_id` = `varchar`.`entity_id` WHERE `sku` = "'.$sku.'" AND `attribute_id` = "'.$attribute_id.'";';
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getCatalogInventory($resource, $readConnection, $entity_id){
	$query = 'SELECT `cataloginventory`.manage_stock FROM ' . $resource->getTableName('cataloginventory_stock_item') . ' as `cataloginventory` WHERE `product_id` = "'.$entity_id.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getCarryover($resource, $readConnection, $entity_id){
	$typeId = getEntityType($resource, $readConnection,'catalog_product');
	$attribute_id = getAttributeId($resource, $readConnection, 'carryover', $typeId);
	$query = 'SELECT `carryover`.value FROM ' . $resource->getTableName('catalog_product_entity_int') . ' as `carryover` WHERE `entity_id` = "'.$entity_id.'" AND `attribute_id` = "'.$attribute_id.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getAttributeId($resource, $readConnection, $attributeCode, $entityType) {
	$query = 'SELECT `attribute_id` FROM ' . $resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "'.$attributeCode.'" and `entity_type_id` = "'.$entityType.'";'; 
	$results = $readConnection->fetchAll($query);	
	if(count($results) > 0){
		return $results[0]['attribute_id'];
	}
}

function getEntityType($resource, $readConnection, $entityCode) {
	$query = 'SELECT `entity_type_id` FROM ' . $resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "'.$entityCode.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0]['entity_type_id'];
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Reporte de ventas</title>
	<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</head>
<body>
	<h3>Per&#237odo seleccionado: <?php echo $dateFrom.' / '.$dateTo ?></h3>
	<table style="width: 50%;" id="table_id" class="display">
	    <thead>
	        <tr style="text-align:center;">
	            <th>Nombre del vendedor</th>
	            <th>Cantidad de ventas</th>
	            <th>Cantidad de ventas de stock</th>
	            <th>Cantidad de ventas de preventa</th>
	            <?php foreach ($marcas as $marca): ?>
	            	<th><?php echo 'Modulos marca: '.$marca ?></th>
	            <?php endforeach ?>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php foreach ($ventas_vendedores as $nombre => $cantidad): ?>
		        <tr style="text-align:center;">
		            <td><?php echo $nombre; ?></td>
		            <td><?php echo $cantidad['stock']+$cantidad['preventa']; ?></td>
		            <td><?php echo $cantidad['stock']; ?></td>
		            <td><?php echo $cantidad['preventa']; ?></td>
		            <?php foreach ($marcas as $marca): ?>
		            	<?php if (array_key_exists($marca, $cantidad)): ?>
		            		<td><?php echo $cantidad[$marca] ?></td>
		            	<?php else: ?>
		            		<td>0</td>
		            	<?php endif; ?>
		            <?php endforeach; ?>
		        </tr>
	    	<?php endforeach; ?> 
	    </tbody>
	</table>
	<form method="post" action="formulario_reportes.php">
		<div class="f bootstrap-iso orm-group" >
			<button class="btn btn-primary" name="submit" type="submit">Nuevo rango de fechas</button>
		</div>
	</form>
</body>
</html>
<script type="text/javascript">
	$(document).ready( function () {
	    $('#table_id').DataTable();
	} );
</script>



		
