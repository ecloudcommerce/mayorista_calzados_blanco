<?php
require_once(dirname(__DIR__) . '../../../app/Mage.php');

ini_set('max_execution_time', 6000);
umask(0);
Mage::app();
ini_set('display_errors', 1);

const SEP = ",";
class Integration
{

	public $resource;
	public $readConnection;
	public $writeConnection;
	public $logFile;
	public $atributes;
	public $attributeIds;
	public $attributeTypes;
	public $productEntityTypeId;
	public $products;
	public $updatedProductCount;
	public $createdProducts;
	public $notCreatedProducts;
	public $notUpdatedProducts;
	public $baseWebsiteId;
	public $catalogSyncConfig;

	public function __construct()
	{
		// MySQL resources
		$this->resource = Mage::getSingleton('core/resource');
		$this->readConnection = $this->resource->getConnection('core_read');
		$this->writeConnection = $this->resource->getConnection('core_write');

		// Configuración del log
		$format = "Y-m-d_H-i-s";
		$dateStr = (new DateTime())->format($format);
		$this->logFile = $dateStr . ".log";

		//Atributos que se reciben en la consulta
		$this->attributes = array(
			"ProductoId",					// EAV attribute - equivalencia_equis: text / codigo: varchar
			"ProductoIdAnterior",			// SKU
			"Precio",						// EAV attribute - price: decimal
			"PrecioSugerido",				// EAV attribute - msrp: decimal
			"CantDisponibleStock",			// 
			// "Descripcion",				// EAV attribute - name: varchar / short_description: text / description: text
			// "Familia",					// EAV attribute set
			// "Linea",						// EAV attribute - genero: int (options)
			// "SubLinea",					// EAV attribute - linea: varchar
			// "Marca",						// EAV attribute - marca: varchar / manufacturer: int (options)
			// "Proveedor",					// EAV attribute - proveedor: varchar
			// "Material",					// EAV attribute - material: varchar
			// "Color",						// EAV attribute - color: varchar
			// "Curva",						// EAV attribute - talle: varchar / tarea: varchar
			// "CurvaCantidadUnidades",		// 
		);

		$this->createdProducts = array();
		$this->notCreatedProducts = array();
		$this->baseWebsiteId = $this->getWebsiteIdByCode("base");
		$this->updatedProductCount = 0;
		$this->catalogSyncConfig = Mage::getStoreConfig("equisconfig/catalogimport");
	}

	/* ---------------- FUNCIONES GET MYSQL ---------------- */

	protected function getEntityType($entityCode)
	{
		$query = 'SELECT `entity_type_id` FROM ' . $this->resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "' . $entityCode . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['entity_type_id'];
		}
	}

	protected function getAttributeId($attributeCode, $entityType)
	{
		$query = 'SELECT `attribute_id` FROM ' . $this->resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "' . $attributeCode . '" and `entity_type_id` = "' . $entityType . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['attribute_id'];
		}
	}

	protected function getAttributeSetId($attributeSetName, $productType)
	{
		$query = 'SELECT `attribute_set_id` FROM ' . $this->resource->getTableName('eav_attribute_set') . ' WHERE `attribute_set_name` = "' . $attributeSetName . '" and `entity_type_id` = "' . $productType . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['attribute_set_id'];
		}
	}

	protected function getAttributeType($attributeId)
	{
		$query = "SELECT `backend_type` FROM `eav_attribute` WHERE `attribute_id` = '" . $attributeId . "';";
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['backend_type'];
		}
	}

	protected function loadSkuList()
	{
		$query = 'SELECT  `sku`  FROM ' . $this->resource->getTableName('catalog_product_entity');
		$results = $this->readConnection->fetchAll($query);
		$this->skuList = array_map(function ($el) {
			return $el["sku"];
		}, $results);
	}

	protected function loadEquisProduct()
	{
		$query = 'SELECT `value` FROM `catalog_product_entity_varchar`  WHERE `attribute_id` = ' .$this->attributeIds["equivalencia_equis"];
		$results = $this->readConnection->fetchAll($query);
		$this->equisList = array_map(function ($el) {
			return $el["value"];
		}, $results);
	}

	protected function getProduct($sku)
	{
		$query = 'SELECT * FROM ' . $this->resource->getTableName('catalog_product_entity') . ' WHERE `sku` = "' . $sku . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0];
		} else {
			return false;
		}
	}

	protected function getProductByCode($equisCode)
	{
		$attributeTable = "catalog_product_entity_" . $this->attributeTypes["equivalencia_equis"];
		$query = 'SELECT * FROM ' . $this->resource->getTableName('catalog_product_entity') . ' 
		INNER JOIN `' . $attributeTable . '` 
			ON `' . $attributeTable . '`.`entity_id` = ' . $this->resource->getTableName('catalog_product_entity') . '.`entity_id` 
				AND `' . $attributeTable . '`.`attribute_id` = "' . $this->attributeIds["equivalencia_equis"] . '" 
		WHERE `' . $attributeTable . '`.`value` = "' . $equisCode . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0];
		} else {
			return false;
		}
	}

	protected function getParentEntityId($childId)
	{
		$query = 'SELECT `parent_id` FROM `catalog_product_super_link` WHERE `product_id` = "' . $childId . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['parent_id'];
		}
		return null;
	}

	protected function getWebsiteIdByCode($websiteCode)
	{
		$query = 'SELECT `website_id` FROM `core_website` WHERE `code` = "' . $websiteCode . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['website_id'];
		}
		return null;
	}

	protected function getStockItemId($productId)
	{
		$query = 'SELECT `item_id` FROM `cataloginventory_stock_item` WHERE `product_id` = "' . $productId . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['item_id'];
		}
		return null;
	}

	protected function getStockAmount($productId)
	{
		$query = 'SELECT `qty` FROM `cataloginventory_stock_item` WHERE `product_id` = "' . $productId . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['qty'];
		}
		return null;
	}

	protected function getOptionId($optionValue, $attributeId)
	{
		$query = 'SELECT DISTINCT * FROM `eav_attribute_option` 
		INNER JOIN `eav_attribute_option_value` 
			ON `eav_attribute_option`.`option_id` = `eav_attribute_option_value`.`option_id` 
		WHERE `attribute_id` = "' . $attributeId . '" 
			AND `eav_attribute_option_value`.`value` = "' . $optionValue . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['option_id'];
		}
		return null;
	}

	/* ---------------- FUNCIONES UPDATE MYSQL ---------------- */

	protected function updateAttribute($attributeCode, $entityId, $value)
	{
		if (array_key_exists($attributeCode, $this->attributeTypes))
			$attributeType = $this->attributeTypes[$attributeCode];
		else
			$attributeType = $this->defaultAttributeTypes[$attributeCode];
		switch ($attributeType) {
			case "text":
				$this->updateText($this->attributeIds[$attributeCode], $entityId, $value);
				break;
			case "varchar":
				$this->updateVarchar($this->attributeIds[$attributeCode], $entityId, $value);
				break;
			case "int":
				$this->updateInt($this->attributeIds[$attributeCode], $entityId, $value);
				break;
			case "decimal":
				$this->updateDecimal($this->attributeIds[$attributeCode], $entityId, $value);
				break;
			default:
				// No existe el atributo
				break;
		}
	}

	protected function updateText($attributeId, $entityId, $value)
	{
		$selectQuery = 'SELECT `value` FROM `catalog_product_entity_text` WHERE `attribute_id` = "' . $attributeId . '" and `entity_id` = "' . $entityId . '";';
		$selectResults = $this->readConnection->fetchAll($selectQuery);
		if (count($selectResults) > 0) {
			$query = "UPDATE `catalog_product_entity_text` SET `catalog_product_entity_text`.`value` = " . $this->writeConnection->quote($value) . " WHERE `catalog_product_entity_text`.`entity_id` = '" . $entityId . "' and `catalog_product_entity_text`.`attribute_id` =  '" . $attributeId . "'";
		} else {
			$query = "INSERT INTO `catalog_product_entity_text` (`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES (" . $this->productEntityTypeId . ", " . $attributeId . ", " . $entityId . ", '" . $value . "')";
		}
		return $this->writeConnection->exec($query);
	}

	protected function updateVarchar($attributeId, $entityId, $value)
	{
		$selectQuery = 'SELECT `value` FROM `catalog_product_entity_varchar` WHERE `attribute_id` = "' . $attributeId . '" and `entity_id` = "' . $entityId . '";';
		$selectResults = $this->readConnection->fetchAll($selectQuery);
		if (count($selectResults) > 0) {
			$query = "UPDATE `catalog_product_entity_varchar` SET `catalog_product_entity_varchar`.`value` = " . $this->writeConnection->quote($value) . " WHERE `catalog_product_entity_varchar`.`entity_id` = '" . $entityId . "' and `catalog_product_entity_varchar`.`attribute_id` =  '" . $attributeId . "'";
		} else {
			$query = "INSERT INTO `catalog_product_entity_varchar` (`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES (" . $this->productEntityTypeId . ", " . $attributeId . ", " . $entityId . ", '" . $value . "')";
		}
		return $this->writeConnection->exec($query);
	}

	protected function updateInt($attributeId, $entityId, $value)
	{
		$selectQuery = 'SELECT `value` FROM `catalog_product_entity_int` WHERE `attribute_id` = "' . $attributeId . '" and `entity_id` = "' . $entityId . '";';
		$selectResults = $this->readConnection->fetchAll($selectQuery);
		if (count($selectResults) > 0) {
			$query = "UPDATE `catalog_product_entity_int` SET `catalog_product_entity_int`.`value` = '" . $value . "' WHERE `catalog_product_entity_int`.`entity_id` = '" . $entityId . "' and `catalog_product_entity_int`.`attribute_id` =  '" . $attributeId . "'";
		} else {
			$query = "INSERT INTO `catalog_product_entity_int` (`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES (" . $this->productEntityTypeId . ", " . $attributeId . ", " . $entityId . ", " . $value . ")";
		}
		return $this->writeConnection->exec($query);
	}

	protected function updateDecimal($attributeId, $entityId, $value)
	{
		$selectQuery = 'SELECT `value` FROM `catalog_product_entity_decimal` WHERE `attribute_id` = "' . $attributeId . '" and `entity_id` = "' . $entityId . '";';
		$selectResults = $this->readConnection->fetchAll($selectQuery);
		if (count($selectResults) > 0) {
			$query = "UPDATE `catalog_product_entity_decimal` SET `catalog_product_entity_decimal`.`value` = '" . $value . "' WHERE `catalog_product_entity_decimal`.`entity_id` = '" . $entityId . "' and `catalog_product_entity_decimal`.`attribute_id` =  '" . $attributeId . "'";
		} else {
			$query = "INSERT INTO `catalog_product_entity_decimal` (`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES (" . $this->productEntityTypeId . ", " . $attributeId . ", " . $entityId . ", " . $value . ")";
		}
		return $this->writeConnection->exec($query);
	}

	protected function updateAttributeSet($entityId, $entityType,  $value)
	{
		$query = "UPDATE `catalog_product_entity` SET `catalog_product_entity`.`attribute_set_id` = '" . $value . "' WHERE `catalog_product_entity`.`entity_id` = '" . $entityId . "' and `catalog_product_entity`.`entity_type_id` =  '" . $entityType . "'";
		return $this->writeConnection->exec($query);
	}

	function updateStock($id, $qty)
	{
		$query = "UPDATE `cataloginventory_stock_item` SET `qty` = '" . $qty . "'";

		if ($qty == 0) {
			$query .= ", `is_in_stock`='0'";
		} else {
			$query .= ", `is_in_stock`='1'";
		}

		$query .= "WHERE `product_id` = '" . $id . "'";

		return $this->writeConnection->exec($query);
	}

	protected function updateStockMovement($stockItemId, $stockAfter)
	{
		// 'bubble_stock_movement'
		$isInStock = $stockAfter > 0 ? 1 : 0;
		$format = "Y-m-d H:i:s";
		$createdAt = (new DateTime())->format($format);
		$message = "Stock updated on equis sync";
		$query = "INSERT INTO `bubble_stock_movement` (`item_id`, `user`, `user_id`, `qty`, `is_in_stock`, `message`, `created_at`)
		VALUES (" . $stockItemId . ", 'admin', 1, " . $stockAfter . ", " . $isInStock . ", '" . $message  . "', '" . $createdAt . "')";
		return $this->writeConnection->exec($query);
	}

	/* ---------------- FUNCIONES DE LOG ---------------- */

	protected function createLog()
	{
		// Crea el archivo en la carpeta /var/log/equis/
		$folder = Mage::getBaseDir('var') . "/log/equis/";
		if (!is_dir($folder)) {
			mkdir($folder);
		}

		$path = $folder . $this->logFile;

		file_put_contents($path, "", LOCK_EX);
		return;
	}

	protected function logInFile($msj)
	{
		$folder = Mage::getBaseDir('var') . "/log/equis/";
		if (!is_dir($folder)) {
			// No existe la carpeta
			mkdir($folder);
		}
		$path = $folder . $this->logFile;
		// Escribir los contenidos en el fichero,
		// usando la bandera FILE_APPEND para añadir el contenido al final del fichero
		// y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
		file_put_contents($path, $msj, FILE_APPEND | LOCK_EX);
	}

	/* ---------------- OTRAS FUNCIONES ---------------- */

	protected function formatText($text)
	{
		// Elimina etiquetas HTML, espacios al inicio o fin del string, y reemplaza caracteres \n y \r por espacio
		// return strip_tags(str_replace(array("\n", "\r"), " ", trim($text)));
		return str_replace(array("\n", "\r"), " ", trim($text));	// Deja las etiquetas HTML
	}

	protected function checkActiveIntegration()
	{
		$activeIntagration = Mage::getStoreConfigFlag('equisconfig/catalogimport/active');

		if (!$activeIntagration) { // Sincronización desactivada por config
			$msj = "LA SINCRONIZACIÓN NO ESTÁ ACTIVADA";
			echo $msj . "<br/>";
			$this->logInFile($msj . "\n");
			return false;
		}

		return true;
	}

	protected function laodAttributeIds()
	{
		// TypeID de producto
		$this->productEntityTypeId = $this->getEntityType('catalog_product');

		// AttributeID de los campos a actualizar
		$this->attributeIds = array(
			"price" =>	 			$this->getAttributeId('price', $this->productEntityTypeId),
			"msrp" =>	 			$this->getAttributeId('msrp', $this->productEntityTypeId),
			"equivalencia_equis" => $this->getAttributeId('equivalencia_equis', $this->productEntityTypeId),
			"marca" => 				$this->getAttributeId('marca', $this->productEntityTypeId),
			"material" => 			$this->getAttributeId('material', $this->productEntityTypeId),
			"color" => 				$this->getAttributeId('color', $this->productEntityTypeId),
			"linea" => 				$this->getAttributeId('linea', $this->productEntityTypeId),
			"manufacturer" => 		$this->getAttributeId('manufacturer', $this->productEntityTypeId),
			"genero" => 			$this->getAttributeId('genero', $this->productEntityTypeId),
			"codigo" => 			$this->getAttributeId('codigo', $this->productEntityTypeId),
			"talle" =>	 			$this->getAttributeId('talle', $this->productEntityTypeId),
			"tarea" =>	 			$this->getAttributeId('tarea', $this->productEntityTypeId),
			// "proveedor" => 		$this->getAttributeId('proveedor', $this->productEntityTypeId),
		);
	}

	protected function laodAttributeTypes()
	{
		// AttributeTypes default de los campos a actualizar
		$this->defaultAttributeTypes = array(
			"price" =>	 			"decimal",
			"msrp" =>	 			"decimal",
			"equivalencia_equis" => "varchar",
			"marca" => 				"varchar",
			"material" => 			"varchar",
			"color" => 				"varchar",
			"linea" => 				"varchar",
			"manufacturer" => 		"int",
			"genero" => 			"int",
			"codigo" => 			"varchar",
			"talle" =>	 			"varchar",
			"tarea" =>	 			"varchar",
			// "proveedor" => 		"varchar",
		);

		// AttributeTypes de los campos a actualizar
		$this->attributeTypes = array(
			"price" =>	 			$this->getAttributeType($this->attributeIds['price'], $this->productEntityTypeId),
			"msrp" =>	 			$this->getAttributeType($this->attributeIds['msrp'], $this->productEntityTypeId),
			"equivalencia_equis" => $this->getAttributeType($this->attributeIds['equivalencia_equis'], $this->productEntityTypeId),
			"marca" => 				$this->getAttributeType($this->attributeIds['marca'], $this->productEntityTypeId),
			"material" => 			$this->getAttributeType($this->attributeIds['material'], $this->productEntityTypeId),
			"color" => 				$this->getAttributeType($this->attributeIds['color'], $this->productEntityTypeId),
			"linea" => 				$this->getAttributeType($this->attributeIds['linea'], $this->productEntityTypeId),
			"manufacturer" => 		$this->getAttributeType($this->attributeIds['manufacturer'], $this->productEntityTypeId),
			"genero" => 			$this->getAttributeType($this->attributeIds['genero'], $this->productEntityTypeId),
			"codigo" => 			$this->getAttributeType($this->attributeIds['codigo'], $this->productEntityTypeId),
			"talle" =>	 			$this->getAttributeType($this->attributeIds['talle'], $this->productEntityTypeId),
			"tarea" =>	 			$this->getAttributeType($this->attributeIds['tarea'], $this->productEntityTypeId),
			// "proveedor" => 		$this->getAttributeType($this->attributeIds['proveedor'], $this->productEntityTypeId),
		);
	}

	protected function manageCurl($curlResponse, $curlError)
	{
		if ($curlError) { // Error en la conexión
			$msj = "NO SE PUDO COMPLETAR LA SINCRONIZACIÓN POR UN ERROR EN LA CONEXIÓN (curl): ";
			echo $msj . "<br/>";
			echo "cURL Error #: " . $curlError . "<br/>";
			$this->logInFile($msj . "\n");
			$this->logInFile($curlError . "\n");
			return false;
		}

		$objResponse = json_decode($curlResponse, true)["Productos"];
		if (!$objResponse) { // No hay productos para sincronizar
			$msj = "NO HAY PRODUCTOS MODIFICADOS";
			echo $msj . "<br/>";
			$this->logInFile($msj . "\n");
			return false;
		}

		if ($objResponse["Error"] != "") { // Error en la consulta
			$msj = "LA CONSULTA SE EJECUTÓ CON UN ERROR";
			echo $msj . ": " . $objResponse["Error"] . "<br/>";
			$this->logInFile($msj . "\n");
			return false;
		}

		return $objResponse;
	}

	protected function startCurlConnection()
	{
		$curl = curl_init();

		$credentials = Mage::getStoreConfig('equisconfig/credentials');

		$option = Mage::getStoreConfig('equisconfig/catalogimport/parameter_option');
		$condition = Mage::getStoreConfig('equisconfig/catalogimport/parameter_condition');

		$params = array("Opcion" => $option, "Condicion" => $condition);
		if ($condition == "D") {
			$lastModified = Mage::getStoreConfig('equisconfig/catalogimport/parameter_lastModified');
			$params["ModificadoDesde"] = $lastModified;
		}

		$optionArray = array(
			CURLOPT_URL => $credentials["url_catalog"],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $params,
			CURLOPT_HTTPHEADER => array(),
		);

		curl_setopt_array($curl, $optionArray);

		$curlResponse = curl_exec($curl);
		$curlError = curl_error($curl);

		curl_close($curl);

		return $this->manageCurl($curlResponse, $curlError);
	}

	protected function getProductIfExists($sku, $equivalenciaEquis)
	{
		// Buscar el producto por sku equis list
		
		$magentoProduct = array_search($equivalenciaEquis,$this->equisList);
		if (!$magentoProduct) {
			// Buscar el producto por equivalencia_equis en equis list
			$magentoProduct =  array_search($sku,$this->equisList);
			if ($magentoProduct) {
				return $this->getProductByCode($sku);
			} else {
				$magentoProduct = array_search($equivalenciaEquis,$this->skuList);
				if (!$magentoProduct) {
					$magentoProduct = in_array($sku,$this->skuList);
					if ($magentoProduct) {
						return $this->getProduct($sku);
					} else {
						return false;
					}
				} else {
					return $this->getProduct($equivalenciaEquis);
				}
			}
		} else {
		    return $this->getProductByCode($equivalenciaEquis);	
		}
	}
	protected function updateSingleProduct($productId, $productObj)
	{
		// Valores nuevos
		$precio = $productObj['Precio'];
		$precioSugerido = array_key_exists("PrecioSugerido", $productObj) ? $productObj['PrecioSugerido'] : 0;
		$stock = $productObj['CantDisponibleStock'];
		$equivalenciaEquis = $productObj["ProductoId"];
		$stockBefore = $this->getStockAmount($productId);

		// Actualización del producto
		if ($this->catalogSyncConfig["update_stock"] == "1") {
			$this->updateStock($productId, $stock);
			if ($stockBefore != $stock) {
				// Actualiza stock movement
				$stockItemId = $this->getStockItemId($productId);
				$this->updateStockMovement($stockItemId, $stock);
			}
		}
		if ($this->catalogSyncConfig["update_price"] == "1")
			$this->updateAttribute("price", $productId, $precio);
		if ($this->catalogSyncConfig["update_sugested_price"] == "1")
			$this->updateAttribute("msrp", $productId, $precioSugerido);
		if ($this->catalogSyncConfig["update_equivalencia"] == "1")
			$this->updateAttribute("equivalencia_equis", $productId, $equivalenciaEquis);

		// Actualiza el log
		$msj =
			$equivalenciaEquis . SEP .
			$productObj['ProductoIdAnterior'] . SEP .
			$precio . SEP .
			$precioSugerido . SEP .
			$stock;
		$this->logInFile($msj . "\n");
	}

	protected function createSimpleProduct($productObj)
	{
		$sku = $productObj['ProductoIdAnterior'];
		$sku = $sku == "" ? $productObj['ProductoId'] : $sku;
		$precio = $productObj['Precio'];
		$precioSugerido = array_key_exists("PrecioSugerido", $productObj) ? $productObj['PrecioSugerido'] : 0;
		$stock = $productObj['CantDisponibleStock'];
		$equivalenciaEquis = $productObj["ProductoId"];
		$calzadoAttributeSetId = $this->getAttributeSetId("calzado", $this->productEntityTypeId);
		$nombre = $productObj["Descripcion"];
		$curva = $productObj["Curva"];

		$simple = Mage::getModel('catalog/product');
		$simple
			->setWebsiteIds(array($this->baseWebsiteId))
			->setAttributeSetId($calzadoAttributeSetId)
			->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
			->setCreatedAt(strtotime('now'))
			->setName($nombre)
			->setDescription($nombre)
			->setShortDescription($nombre)
			->setSku($sku)
			->setWeight(0)
			->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED)
			->setVolumen(0)
			->setPrice($precio)
			->setMsrp($precioSugerido)
			->setTaxClassId(0) // Tax class (0: none, 1: default, 2: taxable, 4: shipping)
			->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
			->setMediaGallery(array('images' => array(), 'values' => array()));

		//Agregar info de stock
		$stockData = array(
			"manage_stock" => "1",
			"original_inventory_qty" => "0",
			"use_config_min_qty" => "1",
			"use_config_min_sale_qty" => "1",
			"use_config_max_sale_qty" => "1",
			"is_qty_decimal" => "0",
			"is_decimal_divided" => "0",
			"backorders" => "0",
			"notify_stock_qty" => "100",
			"enable_qty_increments" => "0",
			"qty_increments" => "0"
		);

		if ($stock > 0) {
			$stockData["qty"] = $stock;
			$stockData["is_in_stock"] = "1";
		} else {
			$stockData["qty"] = "0";
			$stockData["is_in_stock"] = "0";
		}
		$simple->setStockData($stockData);

		$simple->save();

		// Atributos adicionales
		$this->updateAttribute("linea", $simple->getId(), $productObj["SubLinea"]);
		$this->updateAttribute("marca", $simple->getId(), $productObj["Marca"]);
		$this->updateAttribute("material", $simple->getId(), $productObj["Material"]);
		$this->updateAttribute("color", $simple->getId(), $productObj["Color"]);
		$this->updateAttribute("codigo", $simple->getId(), $productObj["ProductoId"]);
		$this->updateAttribute("equivalencia_equis", $simple->getId(), $equivalenciaEquis);
		if (strpos($curva, ":") !== false) {
			$arrayCurva = explode(":", $curva);
			$talle = trim($arrayCurva[0]);
			$tarea = trim($arrayCurva[1]);
			if ($talle && $tarea) {
				$this->updateAttribute("talle", $simple->getId(), $talle);
				$this->updateAttribute("tarea", $simple->getId(), $tarea);
			}
		}

		// Atributos con opciones
		$manufacturerOptionId = $this->getOptionId($productObj["Marca"], $this->attributeIds["manufacturer"]);
		if ($manufacturerOptionId)
			$this->updateAttribute("manufacturer", $simple->getId(), $manufacturerOptionId);

		$generoOptionId = $this->getOptionId($productObj["Linea"], $this->attributeIds["genero"]);
		if ($generoOptionId)
			$this->updateAttribute("genero", $simple->getId(), $generoOptionId);

		$msj =
			$equivalenciaEquis . SEP .
			$productObj['ProductoIdAnterior'] . SEP .
			$precio . SEP .
			$precioSugerido . SEP .
			$stock;
		$this->logInFile($msj . "\n");
		return $simple->getId();
	}

	protected function updateProducts()
	{
		$this->updatedProductCount = 0;

		foreach ($this->products as $productObj) {
			$sku = $productObj['ProductoIdAnterior'];
			if ($sku == "") {
				if ($productObj['ProductoId'] != "" && $productObj['ProductoId'] != null) {
					$sku = $productObj['ProductoId'];
				} else {
					continue;
				}
			}
			$magentoProduct = $this->getProductIfExists($sku, $productObj['ProductoId']);
			if ($magentoProduct) {
				try {
					// Actualiza el producto
					$this->updatedProductCount++;
					$productId = $magentoProduct['entity_id'];
					$this->updateSingleProduct($productId, $productObj);
				} catch (Exception $e) {
					$this->notUpdatedProducts[] = $sku;
					$msj = "Error actualizando el producto con SKU: " . $sku;
					$this->logInFile($msj . "\n");
					$this->logInFile($e->getMessage() . "\n");
					continue;
				}
			} else {
				//Crear producto
				if (!Mage::getStoreConfigFlag("equisconfig/catalogimport/create_products")) {
					continue;
				}
				try {
					$productId = $this->createSimpleProduct($productObj);
					$this->createdProducts[] = $sku;
				} catch (Exception $e) {
					$this->notCreatedProducts[] = $sku;
					$msj = "Error creando el producto con SKU: " . $sku;
					$this->logInFile($msj . "\n");
					$this->logInFile($e->getMessage() . "\n");
					continue;
				}
			}
		}
	}

	public function reindex()
	{
		$indexCodes = array(
			"catalog_product_attribute",
			"catalog_product_price",
			"catalog_product_flat",
			"catalog_category_flat",
			"catalog_category_product",
			"catalogsearch_fulltext",
			"cataloginventory_stock"
		);
		foreach ($indexCodes as $index) {
			$this->logInFile('Reindexando  ' . $index . "\n");
			try {
				$process = Mage::getModel('index/indexer')->getProcessByCode($index);
				$process->reindexAll();
			} catch (Exception $e) {
				echo 'Hubo un error reindexando el índice: ' . $index . '<br/>';
				$this->logInFile($e->getMessage() . "\n");
			}
		}
	}

	protected function end($reindex = false)
	{
		$format = "m-d-Y H:i:s";
		$endDateTime = Mage::getModel('core/date')->date($format);
		Mage::getModel('core/config')->saveConfig('equisconfig/catalogimport/parameter_lastModified', $endDateTime);

		if ($reindex) {
			$this->reindex();
		}

		// Pie del log
		$format = "Y-m-d H:i:s";
		$endDate = Mage::getModel('core/date')->date($format);
		$msj = "SINCRONIZACIÓN TERMINADA: " . $endDate;
		$msj2 = "(" .
			count($this->products) . " productos recibidos, " .
			$this->updatedProductCount . " actualizados, " .
			count($this->createdProducts) . " creados, " .
			count($this->notUpdatedProducts) . " no actualizados, " .
			count($this->notCreatedProducts) . " no creados)";
		echo $msj . "<br/>";
		echo $msj2 . "<br/>";
		$this->logInFile($msj . "\n");
		$this->logInFile($msj2 . "\n");

		if (count($this->createdProducts)) {
			$msj = "PRODUCTOS CREADOS: \n";
			foreach ($this->createdProducts as $sku) {
				$msj .= $sku . SEP . " ";
			}
			$this->logInFile($msj . "\n");
		}

		if (count($this->notCreatedProducts)) {
			$msj = "PRODUCTOS NO CREADOS: \n";
			foreach ($this->notCreatedProducts as $sku) {
				$msj .= $sku . SEP . " ";
			}
			$this->logInFile($msj . "\n");
		}

		if (count($this->notUpdatedProducts)) {
			$msj = "PRODUCTOS NO ACTUALIZADOS: \n";
			foreach ($this->notUpdatedProducts as $sku) {
				$msj .= $sku . SEP . " ";
			}
			$this->logInFile($msj . "\n");
		}

		$msj = "<br/>" . "Detalles de la integración en: log/equis/";
		$msj .= $this->logFile;
		echo $msj . "<br/>";

		return;
	}

	public function main()
	{
		$this->createLog();

		if (!$this->checkActiveIntegration())
			return $this->end();

		$this->products = $this->startCurlConnection();
		if (!$this->products)
			return $this->end();

		// Cabecera del log
		$format = "Y-m-d H:i:s";
		$startDate = Mage::getModel('core/date')->date($format);
		$logHeader = "SINCRONIZACIÓN INICIADA: " . $startDate;
		$msj = "Se van a actualiar " . count($this->products) . " productos";
		$this->logInFile($logHeader . "\n");
		$this->logInFile($msj . "\n");


		// Loguea detalles de atributos a actualizar
		$msj = "Atributos a actualizar: ";
		if ($this->catalogSyncConfig["update_stock"] == "1")
			$msj .= "STOCK, ";
		if ($this->catalogSyncConfig["update_price"] == "1")
			$msj .= "PRECIO, ";
		if ($this->catalogSyncConfig["update_sugested_price"] == "1")
			$msj .= "PRECIO SUGERIDO, ";
		if ($this->catalogSyncConfig["update_equivalencia"] == "1")
			$msj .= "EQUIVALENCIA EQUIS, ";
		echo $msj . "<br/>";
		$this->logInFile($msj . "\n");

		// Cabecera para mostrar datos de productos actualizados
		foreach ($this->attributes as $attr) {
			$msj = $attr . SEP . " ";
			$this->logInFile($msj);
		}
		$this->logInFile("\n");

		$this->laodAttributeIds();
		$this->laodAttributeTypes();
		$this->loadSkuList();
		$this->loadEquisProduct();

		$this->updateProducts();

		$this->end(true);
	}
}



$integracion = new Integration();
$integracion->main();
