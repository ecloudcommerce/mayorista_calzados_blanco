<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

	//Ruta del archivo a procesar
	$file    = file(Mage::getBaseDir('var').'/data/clientes-viajantes-marcas.csv');

	//Ruta del archivo LOG - se crea uno por dia
	$logFile = "import_clientes-vendedores-marcas-".date("Ymd").".log";
	//Separador del log
	const SEP = ",";
	//Encabezado del log
	$msj = '"cod_cliente"'.SEP.'"cliente"'.SEP.'"cod_zona"'.SEP.'"zona"'.SEP.'"marca"';
	gerleroLog($msj,$logFile);
	
	//Columnas del archivo por atributo - comenzando desde 0
	$index = array();
	$index["cod_cliente"] = 0;
	$index["cliente"] = 1;
	$index["cod_zona"] = 2;
	$index["zona"] = 3;
	$index["marca"] = 4;

	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');
	$writeConnection = $resource->getConnection('core_write');

	//primero hay que buscar el entity_type_id
	$typeId = getEntityType($resource, $writeConnection, $readConnection,'customer');

	//obtener los ids de los atributos de cliente
	//pedirle a eav_attribute el attribute_id del atributo que tenga code "vendedor_athix", "vendedor_diadora" y "vendedor_calzados"
	$attributes = array();
	$attributes["cod_cta"] = getAttributeId($resource, $writeConnection, $readConnection,'cod_cta',$typeId);
	$attributes["vendedor_athix"] = getAttributeId($resource, $writeConnection, $readConnection,'vendedor_athix',$typeId);
	$attributes["vendedor_diadora"] = getAttributeId($resource, $writeConnection, $readConnection,'vendedor_diadora',$typeId);
	$attributes["vendedor_calzados"] = getAttributeId($resource, $writeConnection, $readConnection,'vendedor_calzados',$typeId);
	
	$contador = 0;

	//Para validar que sea el primer cliente
	$cliente = "";

	//Acumulador cliente-marca
	$cliente_anterior = '';
	$marca_anterior = '';
	$cliente_marcas = array();

	foreach($file as $row){
		$filedata = explode(",",$row);
		
		//Deben saltarse las primeras dos filas del archivo.
		if($contador != 0 && $contador>=1){
			
			//Valido que no sea el primer producto y que sea un producto distinto al anterior
			//if($cliente!="" && $cliente== $filedata[$index["cod_cliente"]]) {
				//Es un producto igual al anterior
				//Me salto esta fila y sigo con la siguiente
			//	continue;
			//}

			//Limpio el campo del archivo
			$filedata = preg_replace('/[^a-zA-Z0-9\/_|+ .-]/', '', $filedata);
			//Seteo el nuevo sku del producto y todos sus otros atributos
			//Aplico trim por si quedan espacios en blanco antes y despues
			$cod_cliente = trim($filedata[$index["cod_cliente"]]);
			$cliente = trim($filedata[$index["cliente"]]);
			$cod_zona = trim($filedata[$index["cod_zona"]]);
			$zona = trim($filedata[$index["zona"]]);
			$marca = trim($filedata[$index["marca"]]);

			$cliente = '';

			//Busco el cliente con el model de magento
			$cliente = getCliente($resource,$writeConnection,$readConnection,$cod_cliente,$attributes["cod_cta"]);

			if($cliente){
				
				if($contador == 1){
					$cliente_anterior 	= $cod_cliente;
					$marca_anterior 	= $marca;
				}else{
					if($cliente_anterior != $cod_cliente){
						array_push($cliente_marcas, $marca_anterior);
						$cliente_anterior = getCliente($resource,$writeConnection,$readConnection,$cliente_anterior,$attributes["cod_cta"]);
						actualizarGrupoCliente($resource, $writeConnection, $readConnection, $cliente_anterior, $cliente_marcas);
						$cliente_marcas = array();
						$cliente_anterior = $cod_cliente;	
						$marca_anterior = $marca;	
					}else{
						array_push($cliente_marcas, $marca);
					}
				}


				if($marca!=''){
					switch ($marca) {
					   case "CALZADOS":
					         $vendedores_cliente = setVendedor($resource,$writeConnection,$readConnection,$cliente['entity_id'],$attributes["vendedor_calzados"], $cod_zona, $typeId);
					         break;
					   case "DIADORA":
					         $vendedores_cliente = setVendedor($resource,$writeConnection,$readConnection,$cliente['entity_id'],$attributes["vendedor_diadora"], $cod_zona, $typeId);
					         break;
					   case "ATHIX":
					         $vendedores_cliente = setVendedor($resource,$writeConnection,$readConnection,$cliente['entity_id'],$attributes["vendedor_athix"], $cod_zona, $typeId);
					         break;
					}
				}

				
				if($vendedores_cliente){
					try {
						//Logs
						$msj = '"'.$cod_cliente.'"'.SEP.'"'.$cliente['email'].'"'.SEP.'"'.$cod_zona.'"'.SEP.'"'.$zona.'"'.SEP.'"'.$marca.'"';
						gerleroLog($msj,$logFile);

					} catch (Exception $e) {
						Mage::log($e->getMessage());
					}
				}
			}
			
			$contador++;
		} else {
			$contador++;
		}
	}

	//HAY QUE HACER UN REINDEX DE PRECIOS Y DE STOCK



//FUNCIONES

function getAttributeId($resource, $writeConnection, $readConnection, $attributeCode, $entityType) {
	$query = 'SELECT `attribute_id` FROM ' . $resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "'.$attributeCode.'" and `entity_type_id` = "'.$entityType.'";'; 
	$results = $readConnection->fetchAll($query);	
	if(count($results) > 0){
		return $results[0]['attribute_id'];
	}
}

function getEntityType($resource, $writeConnection, $readConnection, $entityCode) {
	$query = 'SELECT `entity_type_id` FROM ' . $resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "'.$entityCode.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0]['entity_type_id'];
	}
}

function gerleroLog($msj,$archivo) {
	// Mage::log($msj,null,$archivo);
	escribirArchivo($msj."\n",$archivo);
	return;
}

function escribirArchivo($msj,$archivo) {
	$carpeta = Mage::getBaseDir('var')."/log/import/";
	if (!is_dir($carpeta)) {
		// No existe la carpeta
		mkdir($carpeta);
	}

	$ruta = $carpeta.$archivo;
	// Escribir los contenidos en el fichero,
	// usando la bandera FILE_APPEND para añadir el contenido al final del fichero
	// y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
	file_put_contents($ruta, $msj, FILE_APPEND | LOCK_EX);
}

function getCliente($resource, $writeConnection, $readConnection,$cod_cliente, $attribute_cod_cta) {
	$query = 'SELECT * FROM ' . $resource->getTableName('customer_entity') . ' INNER JOIN ' . $resource->getTableName('customer_entity_varchar') . ' ON ' . $resource->getTableName('customer_entity') . '.entity_id = ' . $resource->getTableName('customer_entity_varchar') . '.entity_id WHERE `attribute_id` = "'.$attribute_cod_cta.'" AND `value` = "'.$cod_cliente.'";';

	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function setVendedor($resource, $writeConnection, $readConnection,$entity_id, $attribute_id_vendedor, $cod_zona, $typeId) {
	$query = 'SELECT * FROM ' . $resource->getTableName('customer_entity_text') . ' WHERE `attribute_id` = "'.$attribute_id_vendedor.'" AND `entity_id` = "'.$entity_id.'";';

	$results = $readConnection->fetchAll($query);

	if(count($results) > 0){
		//tiene vendedor de la marca asociado, ahora chequear si esta el de la fila actual
		$vendedor_actual = $results[0]['value'];

		if($vendedor_actual){
			//Agregar cod_zona a los valores actuales
			actualizarVendedores($resource, $writeConnection, $readConnection,$typeId,$cod_zona,$entity_id,$attribute_id_vendedor);
		}

		return true;
	} else {
		insertarVendedor($resource, $writeConnection, $readConnection, $typeId, $entity_id, $attribute_id_vendedor,$cod_zona);

		return true;
	}
}

function insertarVendedor($resource, $writeConnection, $readConnection, $typeId, $entity_id, $attribute_id_vendedores, $cod_zona ) {

	$query = 'INSERT INTO ' . $resource->getTableName('customer_entity_text') . ' (entity_type_id, attribute_id, entity_id, value) VALUES ("'.$typeId.'","'.$attribute_id_vendedores.'", "'.$entity_id.'", "'.$cod_zona.'");';

	$writeConnection->query($query);

	return true;
	
}

function actualizarVendedores($resource, $writeConnection, $readConnection, $typeId, $cod_zona, $entity_id, $attribute_id_vendedores) {

	$query_update = 'UPDATE ' . $resource->getTableName('customer_entity_text') . ' set value = "'.$cod_zona.'" WHERE entity_id = "' .$entity_id. '" and attribute_id = "'.$attribute_id_vendedores.'"'; 
	$writeConnection->query($query_update);

	return true;
}

function actualizarCliente($resource, $writeConnection, $readConnection, $entity_id, $customer_group_id) {

	$query_update = 'UPDATE ' . $resource->getTableName('customer_entity') . ' set group_id = "'.$customer_group_id.'" WHERE entity_id = "' .$entity_id. '"'; 
	$writeConnection->query($query_update);

	return true;
}

function actualizarGrupoCliente($resource, $writeConnection, $readConnection, $cliente_anterior, $cliente_marcas) {

	$athix = false;
	$diadora = false;
	$calzados = false;
	
	foreach ($cliente_marcas as $marca) {
		if($marca == 'ATHIX'){
			$athix = true;
		}
		if($marca == 'DIADORA'){
			$diadora = true;
		}
		if($marca == 'CALZADOS'){
			$calzados = true;
		}
	}

	if($athix && $diadora && $calzados){
		$customer_group_code = "General";
		$customer_group_id = getCustomerGroupId($resource, $writeConnection, $readConnection, $customer_group_code);
		actualizarCliente($resource, $writeConnection, $readConnection, $cliente_anterior['entity_id'], $customer_group_id); 
	}

	if($athix && $diadora && !$calzados){
		$customer_group_code = "Athix-Diadora";
		$customer_group_id = getCustomerGroupId($resource, $writeConnection, $readConnection, $customer_group_code);
		actualizarCliente($resource, $writeConnection, $readConnection, $cliente_anterior['entity_id'], $customer_group_id); 
	}

	if($athix && !$diadora && !$calzados){
		$customer_group_code = "Athix";
		$customer_group_id = getCustomerGroupId($resource, $writeConnection, $readConnection, $customer_group_code);
		actualizarCliente($resource, $writeConnection, $readConnection, $cliente_anterior['entity_id'], $customer_group_id); 
	}

	if(!$athix && $diadora && !$calzados){
		$customer_group_code = "Diadora";
		$customer_group_id = getCustomerGroupId($resource, $writeConnection, $readConnection, $customer_group_code);
		actualizarCliente($resource, $writeConnection, $readConnection, $cliente_anterior['entity_id'], $customer_group_id); 
	}

	if(!$athix && !$diadora && $calzados){
		$customer_group_code = "Calzados";
		$customer_group_id = getCustomerGroupId($resource, $writeConnection, $readConnection, $customer_group_code);
		actualizarCliente($resource, $writeConnection, $readConnection, $cliente_anterior['entity_id'], $customer_group_id); 
	}

	if(!$athix && $diadora && $calzados){
		$customer_group_code = "Calzados-Diadora";
		$customer_group_id = getCustomerGroupId($resource, $writeConnection, $readConnection, $customer_group_code);
		actualizarCliente($resource, $writeConnection, $readConnection, $cliente_anterior['entity_id'], $customer_group_id); 
	}

	if($athix && !$diadora && $calzados){
		$customer_group_code = "Calzados-Athix";
		$customer_group_id = getCustomerGroupId($resource, $writeConnection, $readConnection, $customer_group_code);
		actualizarCliente($resource, $writeConnection, $readConnection, $cliente_anterior['entity_id'], $customer_group_id); 
	}

	return true;
}

function getCustomerGroupId($resource, $writeConnection, $readConnection, $customer_group_code) {
	$query = 'SELECT customer_group_id FROM ' . $resource->getTableName('customer_group') . ' WHERE `customer_group_code` = "'.$customer_group_code.'";';

	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0]['customer_group_id'];
	} else {
		return false;
	}
}

?>