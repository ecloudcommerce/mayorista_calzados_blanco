<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

if(isset($_GET['zoneCode']) && !empty($_GET['zoneCode'])) {
    $cod_zona = $_GET["zoneCode"];
}else{
	$cod_zona = null;
	Mage::log('Llamada al endpoint con zoneCode vacío');
}

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$order_collection = getOrders($resource, $readConnection, $cod_zona);

// $order_collection 	= Mage::getModel('sales/order')->getCollection()->addFieldToFilter('status','pending');
// $model_product 		= Mage::getModel('catalog/product');
// $inventory_product 	= Mage::getModel('cataloginventory/stock_item');
$orders 			= array();
$contador = 0;

foreach ($order_collection as $order) {

	$contador++;
	// $order_Address = $order->getShippingAddress()->getData();
	$order_items = array();	
	$cantidad_modulos = 0;
	$evaluar = 1;
	$items = getItems($resource, $readConnection, $order['entity_id']);

  	foreach ($items as $item) {

  		// $producto 	= $model_product->load($item->getProductId());
  		// $inventory 	= $inventory_product->loadByProductId($product);
  		$product 	= getProduct($resource, $readConnection, $item['sku']);
  		$carryover 	= getCarryover($resource, $readConnection, $product['entity_id']);
  		$inventory 	= getCatalogInventory($resource, $readConnection, $product['entity_id']);
  		$small_image = getSmallImage($resource, $readConnection, $product['entity_id']);

  		if($evaluar){
	  		if (!$carryover['value']) {
	  			if ($inventory['manage_stock']) {
	  				$stock_preventa = 'Stock';
	  			}else{
	  				$stock_preventa = 'Preventa';
	  			}
				$evaluar = 0;
	  		}else{
	  			$stock_preventa = 'Stock';
	  		}
  		}

  		$cantidad_modulos = $cantidad_modulos + $item['qty_ordered'];

    	$order_items[] = array(
        	'sku'           => $product['sku'],
        	'name'          => $item['name'],
        	'price'         => $item['price'],
        	'qty_ordered'   => $item['qty_ordered'],
        	'image'			=> $small_image['value']
    	);

   	}

	$orders[$order['entity_id']]['increment_id'] 	 = $order['increment_id'];
	$orders[$order['entity_id']]['grand_total']		 = $order['grand_total'];
	$orders[$order['entity_id']]['total_item_count'] = $order['total_item_count'];
	$orders[$order['entity_id']]['created_at'] 		 = $order['created_at'];
	$orders[$order['entity_id']]['status'] 			 = $order['status'];
	$orders[$order['entity_id']]['firstname'] 		 = $order['customer_firstname'];
	$orders[$order['entity_id']]['lastname'] 		 = $order['customer_lastname'];
	$orders[$order['entity_id']]['customer_email']	 = $order['customer_email'];
	$orders[$order['entity_id']]['telephone']	 	 = $order['telephone'];
	$orders[$order['entity_id']]['postcode']	 	 = $order['postcode'];
	$orders[$order['entity_id']]['stock_preventa']	 = $stock_preventa;
	$orders[$order['entity_id']]['cantidad_modulos'] = $cantidad_modulos;
	$orders[$order['entity_id']]['items'] 	 	     = $order_items;

	if($cod_zona == null){
		if($contador == 1000){
			break;
		}
	}
}                    

$rta = json_encode($orders, JSON_FORCE_OBJECT);
echo $rta;

function getProduct($resource, $readConnection, $sku) {
	$query = 'SELECT `product`.entity_id, `product`.sku  FROM ' . $resource->getTableName('catalog_product_entity') . ' as `product` WHERE `sku` = "'.$sku.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getCatalogInventory($resource, $readConnection, $entity_id){
	$query = 'SELECT `cataloginventory`.manage_stock FROM ' . $resource->getTableName('cataloginventory_stock_item') . ' as `cataloginventory` WHERE `product_id` = "'.$entity_id.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getCarryover($resource, $readConnection, $entity_id){
	$typeId = getEntityType($resource, $readConnection,'catalog_product');
	$attribute_id = getAttributeId($resource, $readConnection, 'carryover', $typeId);
	$query = 'SELECT `carryover`.value FROM ' . $resource->getTableName('catalog_product_entity_int') . ' as `carryover` WHERE `entity_id` = "'.$entity_id.'" AND `attribute_id` = "'.$attribute_id.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getAttributeId($resource, $readConnection, $attributeCode, $entityType) {
	$query = 'SELECT `attribute_id` FROM ' . $resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "'.$attributeCode.'" and `entity_type_id` = "'.$entityType.'";'; 
	$results = $readConnection->fetchAll($query);	
	if(count($results) > 0){
		return $results[0]['attribute_id'];
	}
}

function getEntityType($resource, $readConnection, $entityCode) {
	$query = 'SELECT `entity_type_id` FROM ' . $resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "'.$entityCode.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0]['entity_type_id'];
	}
}

function getSmallImage($resource, $readConnection, $entity_id){
	$typeId = getEntityType($resource, $readConnection,'catalog_product');
	$attribute_id = getAttributeId($resource, $readConnection, 'small_image', $typeId);
	$query = 'SELECT `catalog_product`.value FROM ' . $resource->getTableName('catalog_product_entity_varchar') . ' as `catalog_product` WHERE `entity_id` = "'.$entity_id.'" AND `attribute_id` = "'.$attribute_id.'";';
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}


function getOrders($resource, $readConnection, $cod_zona=null){
	$query = 'SELECT DISTINCT `orders`.increment_id, `orders`.entity_id, `orders`.grand_total, `orders`.total_item_count, `orders`.created_at, `orders`.status, `orders`.customer_firstname, `orders`.customer_lastname, `orders`.customer_email, `shippingAddress`.telephone ,`shippingAddress`.postcode FROM ' . $resource->getTableName('sales_flat_order') . ' as `orders` INNER JOIN '. $resource->getTableName('sales_flat_order_address'). ' as `shippingAddress` ON `orders`.`entity_id` = `shippingAddress`.`parent_id` WHERE `orders`.status = "pending" ';
	if (!($cod_zona === null)) {
		$query .= ' AND `shippingAddress`.postcode = '.$cod_zona;
	}

	if($cod_zona == ''){
		$query .= ' ORDER BY `orders`.created_at DESC';
	}

	$results = $readConnection->fetchAll($query);
	
	if(count($results) > 0){
		return $results;
	} else {
		return false;
	}
}


function getItems($resource, $readConnection, $order_id){
	$query = 'SELECT `item`.sku , `item`.name, `item`.price, `item`.qty_ordered FROM ' . $resource->getTableName('sales_flat_order_item').' as `item` WHERE `order_id` = ' .$order_id ;

	$results = $readConnection->fetchAll($query);

	if(count($results) > 0){
		return $results;
	} else {
		return false;
	}
}
?>