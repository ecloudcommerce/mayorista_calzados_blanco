<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

$order_model = Mage::getModel('sales/order');
$vendedores = Mage::getModel("vendedores/vendedor")->getCollection();
$orders_collection = Mage::getModel('sales/order')->getCollection()
		    ->addAttributeToFilter('created_at', array('from'=>$_POST['dateFrom'], 'to'=>$_POST['dateTo']))
			->addAttributeToFilter('increment_id', array('like' => '00%'));
			// ->joinField(
			//         'postcode',
			//         'sales_flat_order_address',
			//         'postcode',
			//         'entity_id = parent_id',
			//         '{{table}}.postcode IS NOT NULL',
			//         'left');

// die(var_dump($orders_collection->getSelect()->__toString()));

$ventas_vendedores = array();

foreach ($orders_collection as $order) {
		$postCode = $order->getShippingAddress()->getPostcode();
		$vendedor = $vendedores->getItemByColumnValue('cod_zona', $postCode);

		if ($vendedor) {
			if (array_key_exists($vendedor['nombre'], $ventas_vendedores)) {
				$ventas_vendedores[$vendedor['nombre']]	+= 1;
			}else{
				$ventas_vendedores[$vendedor['nombre']]	= 1;
			}
		}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Reporte de ventas</title>
	<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</head>
<body>
	<table style="width: 50%;" id="table_id" class="display">
	    <thead>
	        <tr style="text-align:center;">
	            <th>Nombre del vendedor</th>
	            <th>Cantidad de ventas en el rango ingresado</th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php foreach ($ventas_vendedores as $nombre => $cantidad): ?>
		        <tr style="text-align:center;">
		            <td><?php echo $nombre; ?></td>
		            <td><?php echo $cantidad; ?></td>
		        </tr>
	    	<?php endforeach; ?> 
	    </tbody>
	</table>
	<form method="post" action="formulario_reportes.php">
		<div class="f bootstrap-iso orm-group" >
			<button class="btn btn-primary" name="submit" type="submit">Nuevo rango de fechas</button>
		</div>
	</form>
</body>
</html>
<script type="text/javascript">
	$(document).ready( function () {
	    $('#table_id').DataTable();
	} );
</script>



		
