<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

	//Ruta del archivo a procesar
	$file    = file(Mage::getBaseDir('var').'/data/control.txt');

	//Ruta del archivo LOG - se crea uno por dia
	$logFile = "import_".date("Ymd").".log";
	//Separador del log
	const SEP = ",";
	//Encabezado del log
	$msj = '"sku"'.SEP.'"stock"'.SEP.'"precio"'.SEP.'"es_nuevo"';
	gerleroLog($msj,$logFile);
	
	//Columnas del archivo por atributo - comenzando desde 0
	$index = array();
	$index["sku"] = 0;
	$index["codigo"] = 1;
	$index["nombre"] = 2;
	$index["stock"] = 3;
	$index["precio"] = 4;

	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');
	$writeConnection = $resource->getConnection('core_write');

	//primero hay que buscar el entity_type_id
	$typeId = getEntityType($resource, $writeConnection, $readConnection,'catalog_product');

	//obtener los ids de atributo de precio
	//pedirle a eav_attribute el attribute_id del atributo que tenga code "price"
	$attributes = array();
	$attributes["precio"] = getAttributeId($resource, $writeConnection, $readConnection,'price',$typeId);
	
	$contador = 0;

	//Para validar que sea el primer producto
	$sku = "";

	foreach($file as $row){
		$filedata = explode(",",$row);
		
		//Deben saltarse las primeras dos filas del archivo.
		if($contador != 0 && $contador>=2){

			//Bandera para el log
			$esProductoNuevo = 0;
			
			//Valido que no sea el primer producto y que sea un producto distinto al anterior
			if($sku!="" && $sku == $filedata[$index["sku"]]) {
				//Es un producto igual al anterior
				//Me salto esta fila y sigo con la siguiente
				continue;
			}
			//Limpio el campo del archivo
			$filedata = preg_replace('/[^a-zA-Z0-9\/_|+ .-]/', '', $filedata);
			//Seteo el nuevo sku del producto y todos sus otros atributos
			//Aplico trim por si quedan espacios en blanco antes y despues
			$sku = trim($filedata[$index["sku"]]);
			$codigo = trim($filedata[$index["codigo"]]);
			$stock = trim($filedata[$index["stock"]]);
			$precio = trim($filedata[$index["precio"]]);

			//Busco el producto con el model de magento
			$producto = getProduct($resource,$writeConnection,$readConnection,$sku);
			//No existe el producto?
			if(!$producto){
				//Crear producto
				try {
					$attributeSet = 4;
					$websiteId = 1;
					$nombre = trim($filedata[$index["nombre"]]);
					$descripcion = $nombre;
					$simple = Mage::getModel('catalog/product');
					$simple
						->setWebsiteIds(array($websiteId)) //website ID the product is assigned to, as an array
						->setAttributeSetId($attributeSet) //ID of a attribute set named 'default'
						->setTypeId('simple') //product type
						->setCreatedAt(strtotime('now')) //product creation time
						->setSku($sku) //SKU
						->setName($nombre) //product name
						->setWeight(0)
						->setStatus(1) //product status (1 - enabled, 2 - disabled)
						->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
						->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE) //catalog and search visibility
						->setPrice($precio) //price in form 11.22
						->setDescription($descripcion)
						->setShortDescription($descripcion)
						->setMediaGallery(array('images' => array(), 'values' => array())); //media gallery initialization
						// ->setCategoryIds(array($categoryId)); //assign product to categories

					//Agregar info de stock
					$stockData = array(
							"manage_stock" => "1",
							"original_inventory_qty" => "0",
							"use_config_min_qty" => "1",
							"use_config_min_sale_qty" => "1",
							"use_config_max_sale_qty" => "1",
							"is_qty_decimal" => "0",
							"is_decimal_divided" => "0",
							"backorders" => "0",
							"notify_stock_qty" => "100",
							"enable_qty_increments" => "0",
							"qty_increments" => "0"
						);

					//Ver si la fila del csv tiene stock asignado
					if($stock > 0) {
						$stockData["qty"] = $stock;
						$stockData["is_in_stock"] = "1";
					} else {
						$stockData["qty"] = "0";
						$stockData["is_in_stock"] = "0";
					}
					$simple->setStockData($stockData);
					
					//Guarda el producto simple
					$simple->save();

					$esProductoNuevo = 1;

				} catch (Exception $e) {
					Mage::log($e->getMessage());
				}

				//Logs
				$msj = '"'.$sku.'"'.SEP.'"'.$stock.'"'.SEP.'"'.$precio.'"'.SEP.'"'.$esProductoNuevo.'"';
				gerleroLog($msj,$logFile);
			
			}
			
			$contador++;
		} else {
			$contador++;
		}
	}

	//HAY QUE HACER UN REINDEX DE PRECIOS Y DE STOCK



//FUNCIONES

function getAttributeId($resource, $writeConnection, $readConnection, $attributeCode, $entityType) {
	$query = 'SELECT `attribute_id` FROM ' . $resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "'.$attributeCode.'" and `entity_type_id` = "'.$entityType.'";'; 
	$results = $readConnection->fetchAll($query);	
	if(count($results) > 0){
		return $results[0]['attribute_id'];
	}
}

function getEntityType($resource, $writeConnection, $readConnection, $entityCode) {
	$query = 'SELECT `entity_type_id` FROM ' . $resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "'.$entityCode.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0]['entity_type_id'];
	}
}

function gerleroLog($msj,$archivo) {
	// Mage::log($msj,null,$archivo);
	escribirArchivo($msj."\n",$archivo);
	return;
}

function escribirArchivo($msj,$archivo) {
	$carpeta = Mage::getBaseDir('var')."/log/import/";
	if (!is_dir($carpeta)) {
		// No existe la carpeta
		mkdir($carpeta);
	}

	$ruta = $carpeta.$archivo;
	// Escribir los contenidos en el fichero,
	// usando la bandera FILE_APPEND para añadir el contenido al final del fichero
	// y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
	file_put_contents($ruta, $msj, FILE_APPEND | LOCK_EX);
}

function getProduct($resource, $writeConnection, $readConnection,$sku) {
	$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity') . ' WHERE `sku` = "'.$sku.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

?>