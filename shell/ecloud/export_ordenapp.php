<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$writeConnection = $resource->getConnection('core_write');

//primero hay que buscar el entity_type_id
$typeId = getEntityType($resource, $writeConnection, $readConnection,'catalog_product');

//obtener los ids de atributo de precio
//pedirle a eav_attribute el attribute_id del atributo que tenga code "price"
$attributes = array();
$attributes["ordenapp"] = getAttributeId($resource, $writeConnection, $readConnection,'ordenapp',$typeId);

$products = Mage::getModel('catalog/product')->getCollection()
        ->addAttributeToSelect(array('sku','price'))
        ->joinField('price','catalog/product_index_price','price','entity_id=entity_id',null,'left')
        ->joinField('value','catalog_product_entity_varchar','value','entity_id=entity_id', 'attribute_id='.$attributes["ordenapp"],'left'); 

$products->getSelect()->group('e.entity_id');

$p = array();

foreach ($products as $product) {
    $atributos = array ($product->getPrice(),$product->getValue());
    $p[$product->getSku()] = $atributos;
}

$stock_productos = json_encode($p, JSON_FORCE_OBJECT);

echo $stock_productos;

function getAttributeId($resource, $writeConnection, $readConnection, $attributeCode, $entityType) {
    $query = 'SELECT `attribute_id` FROM ' . $resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "'.$attributeCode.'" and `entity_type_id` = "'.$entityType.'";'; 
    $results = $readConnection->fetchAll($query);   
    if(count($results) > 0){
        return $results[0]['attribute_id'];
    }
}

function getEntityType($resource, $writeConnection, $readConnection, $entityCode) {
    $query = 'SELECT `entity_type_id` FROM ' . $resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "'.$entityCode.'";'; 
    $results = $readConnection->fetchAll($query);
    if(count($results) > 0){
        return $results[0]['entity_type_id'];
    }
}
?>