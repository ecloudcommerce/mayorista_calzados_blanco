<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

$cod_zona = $_POST['vendedor'];
$dateFrom = $_POST['dateFrom'];
$dateTo = $_POST['dateTo'];

$from = strtotime('+3 hour',strtotime($dateFrom));  
$from = date ('Y-m-d H:i:s', $from); 

$to = strtotime('+3 hour',strtotime($dateTo));
$to = date ('Y-m-d H:i:s', $to); 

$vendedor = Mage::getModel("vendedores/vendedor")->getCollection()->getItemByColumnValue('cod_zona', $cod_zona);
$orders_collection 	= Mage::getModel('sales/order')->getCollection()
			// ->addAttributeToSelect(array('postcode'))
			// ->joinField(
			//     'address',
			//     'sales_flat_order_address',
			//     'postcode',
			//     'entity_id = parent_id',
			//     'address.postcode = '.$cod_zona,
			//     'left'
			// );
		    ->addAttributeToFilter('created_at', array('from'=>$from, 'to'=>$to))
			->addAttributeToFilter('increment_id', array('like' => '00%'))
			->addAttributeToFilter('status', 'pending');
			
$productos = array();
$catalog_product = Mage::getModel('catalog/product');
$total_articulos = 0;
$total_modulos = 0;
$total_ventas = 0;

foreach ($orders_collection as $order) {

		$postCode = $order->getShippingAddress()->getPostcode();

		if ($postCode == $cod_zona) {

			$total_ventas += 1;

			foreach ($order->getAllItems() as $item) {
				$product = $catalog_product->loadByAttribute('sku',$item->getSku());
				if ($product) {	
					if (array_key_exists($item->getSku() , $productos)) {
						$productos[$item->getSku()]['qty'] += $item->getQtyOrdered();
						$productos[$item->getSku()]['qty_mod'] += $item->getQtyOrdered() * 12;
						$total_articulos += $item->getQtyOrdered();
						$total_modulos += $item->getQtyOrdered() * 12;

					}else{
						$productos[$item->getSku()]['nombre'] = $product->getName();
						$productos[$item->getSku()]['talle'] = $product->getTalle();
						$productos[$item->getSku()]['color'] = $product->getColor();
						$productos[$item->getSku()]['genero'] = $product->getAttributeText('genero');
						$productos[$item->getSku()]['disciplina'] = $product->getAttributeText('disciplina');
						$productos[$item->getSku()]['qty'] = (int)$item->getQtyOrdered();
						$productos[$item->getSku()]['qty_mod'] = (int)($item->getQtyOrdered() * 12);
						$total_articulos += $item->getQtyOrdered();
						$total_modulos += $item->getQtyOrdered() * 12;
					}
				}	
			}
		}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Reporte de ventas</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js" ></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

	<!-- <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> -->
	<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css"> -->
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
	<!-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> -->
</head>
<body>
	<h2>Vendedor: <?php echo $vendedor->getNombre() ?></h2>
	<h3>Per&#237odo seleccionado: <?php echo $dateFrom.' / '.$dateTo ?></h3>
	<div style="width: 50%">
		<table class="table table-striped table-bordered" id="table_id" class="display">
		    <thead>
		        <tr>
		            <th>Art&iacuteculo</th>
		            <th>Talle</th>
		            <th>Color</th>
		            <th>G&#233nero</th>
		            <th>Disciplina</th>
		            <th>Cantidad por m&#243dulos</th>
		            <th>Cantidad por pares</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($productos as $producto): ?>
			        <tr>
			            <td><?php echo $producto['nombre']; ?></td>
			            <td><?php echo $producto['talle']; ?></td>
			            <td><?php echo $producto['color']; ?></td>
			            <td><?php echo $producto['genero']; ?></td>
			            <td><?php echo $producto['disciplina']; ?></td>
			            <td><?php echo $producto['qty']; ?></td>
			            <td><?php echo $producto['qty_mod']; ?></td>
			        </tr>
		    	<?php endforeach; ?> 
		    </tbody>
		    	<tr>
		    		<h3>Total de Ventas: <?php echo $total_ventas ?></h3>
		    		<h3>Total de M&#243dulos: <?php echo $total_articulos ?></h3>	
		    		<h3>Total de Pares: <?php echo $total_modulos ?></h3>	
		    	</tr>
		    <tfoot class="first-foot">
		    	<tr>
	                <th colspan="2" style="text-align:right">Total modulos:</th>
	                <th colspan="5" ></th>
	            </tr>
	        </tfoot>
		</table>
	</div>
	<form method="post" action="formulario_reportes.php">
		<div class="f bootstrap-iso orm-group" >
			<button class="btn btn-primary" name="submit" type="submit">Nuevo rango de fechas</button>
		</div>
	</form>
</body>
</html>
<script type="text/javascript">
	$(document).ready( function () {
	    $('#table_id').DataTable(
	    	{
	    	        "footerCallback": function ( row, data, start, end, display ) {
	    	            var api = this.api(), data, columns = [3, 5];;
	    	 
	    	            // Remove the formatting to get integer data for summation
	    	            var intVal = function ( i ) {
	    	                return typeof i === 'string' ?
	    	                    i.replace(/[\$,]/g, '')*1 :
	    	                    typeof i === 'number' ?
	    	                        i : 0;
	    	            };
	    	            // Total over all pages
	    	            total = api
	    	                .column( 5 )
	    	                .data()
	    	                .reduce( function (a, b) {
	    	                    return intVal(a) + intVal(b);
	    	                }, 0 );
	    	 
	    	            // Total over this page
	    	            pageTotal = api
	    	                .column( 5, { page: 'current'} )
	    	                .data()
	    	                .reduce( function (a, b) {
	    	                    return intVal(a) + intVal(b);
	    	                }, 0 );

	    	            $( api.column( 3 ).footer() ).html(
	    	                pageTotal +' ('+ total +' total)'
	    	            );
	    	        }
	    	    }
	    	);
	} );
</script>
<!-- 
<style type="text/css">
	div#table_id2_wrapper .row:first-child{
		display: none;
	}
	table#table_id2 thead, table#table_id2 tbody{
		display: none;
	}
	div#table_id2_wrapper .row:last-child{
		display: none;
	}
	table#table_id {
	    border-bottom: 1px solid #ddd;
	}
	table#table_id tfoot tr th {
    	border-bottom: none !important;
    	border-right: none !important;
    	text-align: left !important;
	}
	tfoot.first-foot, tfoot.second-foot{
		float: left;
		width: 100%;
		margin: 5px;
	}
</style>  -->

