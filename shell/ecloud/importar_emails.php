<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

	//Ruta del archivo a procesar
	$file    = file(Mage::getBaseDir('var').'/data/mails_clientes.csv');

	//Ruta del archivo LOG - se crea uno por dia
	$logFile = "import_customer".date("Ymd").".log";
	//Separador del log
	const SEP = ",";
	//Encabezado del log
	$msj = '"customer_id"'.SEP.'"razon_social"'.SEP.'"email"';
	gerleroLog($msj,$logFile);
	
	//Columnas del archivo por atributo - comenzando desde 0
	$index = array();
	$index["cuenta"] = 0;
	$index["razon_social"] = 1;
	$index["email"] = 2;

	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');
	$writeConnection = $resource->getConnection('core_write');

	//primero hay que buscar el entity_type_id
	$typeId = getEntityType($resource, $writeConnection, $readConnection,'customer');

	//obtener los ids de atributo de precio
	//pedirle a eav_attribute el attribute_id del atributo que tenga code "price"
	$attributes = array();
	$attributes["cod_cta"] = getAttributeId($resource, $writeConnection, $readConnection,'cod_cta',$typeId);
	
	$contador = 0;

	//Para validar que sea el primer producto
	$mail = "";

	foreach($file as $row){
		$filedata = explode(",",$row);
		
		//Deben saltarse las primeras dos filas del archivo.

		if($contador != 0 && $contador>=2){
			
			//Valido que no sea el primer producto y que sea un producto distinto al anterior
			//echo $mail;
			//echo $filedata[$index["email"]];

			if($mail!="" && $mail == $filedata[$index["email"]]) {
				//Es un producto igual al anterior
				//Me salto esta fila y sigo con la siguiente
				continue;
			}
			//Limpio el campo del archivo
			//$filedata = preg_replace('/[^a-zA-Z0-9\/_|+ .-]/', '', $filedata);

			//Seteo el nuevo sku del producto y todos sus otros atributos
			//Aplico trim por si quedan espacios en blanco antes y despues
			$cuenta = trim($filedata[$index["cuenta"]]);
			$razon_social = trim($filedata[$index["razon_social"]]);
			$email = trim($filedata[$index["email"]]);

			if($cuenta != '' && $email != ''){
				//Busco el producto con el model de magento

				$mail = $email;

				$customer_id = getCustomerId($resource, $writeConnection, $readConnection,$cuenta, $attributes["cod_cta"]);

				if($customer_id != NULL){

					$existe_customer = getCustomerEmail($resource, $writeConnection, $readConnection,$email);

					$customer_id = $customer_id['entity_id'];

					if(!$existe_customer){

						$customer = getCustomer($resource, $writeConnection, $readConnection,$customer_id);

						if($customer != NULL){

							updateEmail($resource, $writeConnection, $readConnection,$customer_id, $email);
							//Logs
							$msj = '"'.$customer_id.'"'.SEP.'"'.$razon_social.'"'.SEP.'"'.$email.'"';
							gerleroLog($msj,$logFile);
						}	
					}else{
						$msj = '"'.$customer_id.'"'.SEP.'"'.$razon_social.'"'.SEP.'"No Actualizo"';
						gerleroLog($msj,$logFile);
					}
				}
			}
		}
		echo $contador.' ';
		$contador++;
	}


//FUNCIONES

function getAttributeId($resource, $writeConnection, $readConnection, $attributeCode, $entityType) {
	$query = 'SELECT `attribute_id` FROM ' . $resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "'.$attributeCode.'" and `entity_type_id` = "'.$entityType.'";'; 
	$results = $readConnection->fetchAll($query);	
	if(count($results) > 0){
		return $results[0]['attribute_id'];
	}
}

function getEntityType($resource, $writeConnection, $readConnection, $entityCode) {
	$query = 'SELECT `entity_type_id` FROM ' . $resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "'.$entityCode.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0]['entity_type_id'];
	}
}

function updateEmail($resource, $writeConnection, $readConnection, $customer_id, $email) {
	//Tabla catalog_product_entity_decimal
	
	//Setear valores: UPDATE -> value = $precioGrilla
	$query = "UPDATE ".$resource->getTableName('customer_entity')." SET `email`=".$email." WHERE ";
	//Agrego la parte del where.
	$query .= "`entity_id`=".$customer_id.";";			
	
	//Devuelvo la cantidad de filas afectadas
	return $writeConnection->exec($query);
}

function gerleroLog($msj,$archivo) {
	// Mage::log($msj,null,$archivo);
	escribirArchivo($msj."\n",$archivo);
	return;
}

function escribirArchivo($msj,$archivo) {
	$carpeta = Mage::getBaseDir('var')."/log/import/";
	if (!is_dir($carpeta)) {
		// No existe la carpeta
		mkdir($carpeta);
	}

	$ruta = $carpeta.$archivo;
	// Escribir los contenidos en el fichero,
	// usando la bandera FILE_APPEND para añadir el contenido al final del fichero
	// y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
	file_put_contents($ruta, $msj, FILE_APPEND | LOCK_EX);
}

function getCustomerId($resource, $writeConnection, $readConnection,$codcta, $attribute_id) {
	$query = 'SELECT * FROM ' . $resource->getTableName('customer_entity_varchar') . ' WHERE `attribute_id` = "'.$attribute_id.'" AND `value` = "'.$codcta.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getCustomerEmail($resource, $writeConnection, $readConnection,$email) {
	$query = 'SELECT * FROM ' . $resource->getTableName('customer_entity') . ' WHERE `email` = '.$email.';'; 
	
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getCustomer($resource, $writeConnection, $readConnection,$entity_id) {
	$query = 'SELECT * FROM ' . $resource->getTableName('customer_entity') . ' WHERE `entity_id` = "'.$entity_id.'";'; 

	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

?>