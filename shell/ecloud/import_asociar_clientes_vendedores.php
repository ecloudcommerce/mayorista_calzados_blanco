<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

	//Ruta del archivo a procesar
	$file    = file(Mage::getBaseDir('var').'/data/CLIENTEZONAMARCA.CSV');

	//Ruta del archivo LOG - se crea uno por dia
	$logFile = "import_".date("Ymd").".log";
	//Separador del log
	const SEP = ",";
	//Encabezado del log
	$msj = '"cod_cliente"'.SEP.'"cliente"'.SEP.'"cod_zona"'.SEP.'"zona"';
	gerleroLog($msj,$logFile);
	
	//Columnas del archivo por atributo - comenzando desde 0
	$index = array();
	$index["cod_cliente"] = 0;
	$index["cliente"] = 1;
	$index["cod_zona"] = 2;
	$index["zona"] = 3;

	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');
	$writeConnection = $resource->getConnection('core_write');

	//primero hay que buscar el entity_type_id
	$typeId = getEntityType($resource, $writeConnection, $readConnection,'customer');

	//obtener los ids de los atributos de cliente
	//pedirle a eav_attribute el attribute_id del atributo que tenga code "vendedores" y "cod_cta"
	$attributes = array();
	$attributes["cod_cta"] = getAttributeId($resource, $writeConnection, $readConnection,'cod_cta',$typeId);
	$attributes["vendedores"] = getAttributeId($resource, $writeConnection, $readConnection,'vendedores',$typeId);
	
	$contador = 0;

	//Para validar que sea el primer producto
	$cliente = "";

	foreach($file as $row){
		$filedata = explode(";",$row);
		
		//Deben saltarse las primeras dos filas del archivo.
		if($contador != 0 && $contador>=2){

			//Bandera para el log
			$esProductoNuevo = 0;
			
			//Valido que no sea el primer producto y que sea un producto distinto al anterior
			if($cliente!="" && $cliente== $filedata[$index["cod_cliente"]]) {
				//Es un producto igual al anterior
				//Me salto esta fila y sigo con la siguiente
				continue;
			}
			//Limpio el campo del archivo
			$filedata = preg_replace('/[^a-zA-Z0-9\/_|+ .-]/', '', $filedata);
			//Seteo el nuevo sku del producto y todos sus otros atributos
			//Aplico trim por si quedan espacios en blanco antes y despues
			$cod_cliente = trim($filedata[$index["cod_cliente"]]);
			$cliente = trim($filedata[$index["cliente"]]);
			$cod_zona = trim($filedata[$index["cod_zona"]]);
			$zona = trim($filedata[$index["zona"]]);

			$cliente = '';

			//Busco el cliente con el model de magento
			$cliente = getCliente($resource,$writeConnection,$readConnection,$cod_cliente,$attributes["cod_cta"]);

			if($cliente){
				
				//busco si el cliente tiene el vendedor asociado
				$vendedores_cliente = getVendedores($resource,$writeConnection,$readConnection,$cliente['entity_id'],$attributes["vendedores"], $cod_zona, $typeId);

				//Si el cliente no tiene vendedor asignado, se lo creamos
				if(!$vendedores_cliente){
					try {
						insertarVendedor($resource,$writeConnection,$readConnection,$cliente['entity_id'],$attributes["vendedores"], $cod_zona, $typeId);

					} catch (Exception $e) {
						Mage::log($e->getMessage());
					}
				}
			}
			
			//Logs
			$msj = '"'.$cod_cliente.'"'.SEP.'"'.$cliente.'"'.SEP.'"'.$cod_zona.'"'.SEP.'"'.$zona.'"';
			gerleroLog($msj,$logFile);
			
			$contador++;
		} else {
			$contador++;
		}
	}

	//HAY QUE HACER UN REINDEX DE PRECIOS Y DE STOCK



//FUNCIONES

function getAttributeId($resource, $writeConnection, $readConnection, $attributeCode, $entityType) {
	$query = 'SELECT `attribute_id` FROM ' . $resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "'.$attributeCode.'" and `entity_type_id` = "'.$entityType.'";'; 
	$results = $readConnection->fetchAll($query);	
	if(count($results) > 0){
		return $results[0]['attribute_id'];
	}
}

function getEntityType($resource, $writeConnection, $readConnection, $entityCode) {
	$query = 'SELECT `entity_type_id` FROM ' . $resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "'.$entityCode.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0]['entity_type_id'];
	}
}

function gerleroLog($msj,$archivo) {
	// Mage::log($msj,null,$archivo);
	escribirArchivo($msj."\n",$archivo);
	return;
}

function escribirArchivo($msj,$archivo) {
	$carpeta = Mage::getBaseDir('var')."/log/import/";
	if (!is_dir($carpeta)) {
		// No existe la carpeta
		mkdir($carpeta);
	}

	$ruta = $carpeta.$archivo;
	// Escribir los contenidos en el fichero,
	// usando la bandera FILE_APPEND para añadir el contenido al final del fichero
	// y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
	file_put_contents($ruta, $msj, FILE_APPEND | LOCK_EX);
}

function getCliente($resource, $writeConnection, $readConnection,$cod_cliente, $attribute_cod_cta) {
	$query = 'SELECT * FROM ' . $resource->getTableName('customer_entity') . ' INNER JOIN ' . $resource->getTableName('customer_entity_varchar') . ' ON ' . $resource->getTableName('customer_entity') . '.entity_id = ' . $resource->getTableName('customer_entity_varchar') . '.entity_id WHERE `attribute_id` = "'.$attribute_cod_cta.'" AND `value` = "'.$cod_cliente.'";';

	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getVendedores($resource, $writeConnection, $readConnection,$entity_id, $attribute_id_vendedores, $cod_zona, $typeId) {
	$query = 'SELECT * FROM ' . $resource->getTableName('customer_entity_text') . ' WHERE `attribute_id` = "'.$attribute_id_vendedores.'" AND `entity_id` = "'.$entity_id.'";';

	$results = $readConnection->fetchAll($query);

	if(count($results) > 0){
		//tiene vendedores asociados, ahora chequear si esta el de la fila actual
		$vendedores = explode(",",$results[0]['value']);

		$debe_actualizar = true;

		foreach ($vendedores as $id_vendedor) {
			$vendedor = getVendedor($resource, $writeConnection, $readConnection, $cod_zona);
			if($vendedor){
				if($vendedor['id'] == $id_vendedor){
					$debe_actualizar = false;
				}
			}
		}
		if($debe_actualizar){
			//Agregar cod_zona a los valores actuales
			actualizarVendedores($resource, $writeConnection, $readConnection,$typeId,$cod_zona,$entity_id,$attribute_id_vendedores);
		}
		return true;
	} else {
		return false;
	}
}

function getVendedor($resource, $writeConnection, $readConnection, $cod_zona) {
	$query = 'SELECT * FROM ' . $resource->getTableName('vendedor') . ' WHERE `cod_zona` = "'.$cod_zona.'";';
	
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function insertarVendedor($resource, $writeConnection, $readConnection, $entity_id, $attribute_id_vendedores, $cod_zona, $typeId) {

	$vendedor = getVendedor($resource, $writeConnection, $readConnection, $cod_zona);

	if($vendedor){
		$query = 'INSERT INTO ' . $resource->getTableName('customer_entity_text') . ' (entity_type_id, attribute_id, entity_id, value) VALUES ("'.$typeId.'","'.$attribute_id_vendedores.'", "'.$entity_id.'", "'.$vendedor['id'].'");';

		$writeConnection->query($query);

		return true;
	}else{
		return false;
	}
}

function actualizarVendedores($resource, $writeConnection, $readConnection, $typeId, $cod_zona, $entity_id, $attribute_id_vendedores) {

	$vendedor = getVendedor($resource, $writeConnection, $readConnection, $cod_zona);

	if($vendedor){
		$query = 'SELECT  `value` FROM ' . $resource->getTableName('customer_entity_text') . ' WHERE `attribute_id` = "'.$attribute_id_vendedores.'" AND `entity_id` = "'.$entity_id.'";';

		$results = $readConnection->fetchAll($query);

		$value_vendedores = $results[0]['value'].','.$vendedor['id'];

		$query_update = 'UPDATE ' . $resource->getTableName('customer_entity_text') . ' set value = "'.$value_vendedores.'" WHERE entity_id = "' .$entity_id. '" and attribute_id = "'.$attribute_id_vendedores.'"'; 
		$writeConnection->query($query_update);

		return true;

	}else{
		return false;
	}
}

?>