<?php
	
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

// $resource = Mage::getSingleton('core/resource');
// $readConnection = $resource->getConnection('core_read');

$dateFrom = $_POST['dateFrom'];
$dateTo = $_POST['dateTo'];

// $dateFrom = date('Y-m-d 00:00:00');
// $dateTo = date('Y-m-d 23:59:59');

$from = strtotime('+3 hour',strtotime($dateFrom));  
$from = date ('Y-m-d H:i:s', $from); 

$to = strtotime('+3 hour',strtotime($dateTo));
$to = date ('Y-m-d H:i:s', $to); 

$orders_collection = Mage::getModel('sales/order')->getCollection()
		    ->addAttributeToFilter('created_at', array('from'=>$from , 'to'=>$to))
			->addAttributeToFilter('increment_id', array('like' => '00%'))
			->addAttributeToFilter('status', 'pending');

$customer_model = Mage::getModel('customer/customer');
$catalog_product = Mage::getModel('catalog/product');
$vendedores_collection = Mage::getModel("vendedores/vendedor")->getCollection();

$vendedores = array();
$marcas = array();

foreach ($vendedores_collection as $vendedor) {
	
	$clientes = array();
	// $disciplinas = array();	

	foreach ($orders_collection as $order) {

		$postCode = $order->getShippingAddress()->getPostcode();
		$cod_zona = $vendedor->getCodZona();

		if ($postCode == $cod_zona) {

			$customer = $customer_model->load($order->getCustomerId());
			if (!array_key_exists($customer->getFirstname(), $clientes)) {
				$clientes[$customer->getFirstname()]	= array(
					'total_items'=> 0
				);
			}
			if ($guardarcod_zona) {
				$vendedores[$vendedor->getNombre()]['cod_zona'] = $vendedor->getCodZona();
				$guardarcod_zona = false;
			}

			$total_items = 0;
			foreach ($order->getAllItems() as $item) {
				
				$producto = $catalog_product->loadByAttribute('sku',$item->getSku());
				// $disciplina = $producto->getAttributeText('disciplina');
				if ($producto) {
					$total_items += (int)$item->getQtyOrdered();
			  		if ($producto->getAttributeText('manufacturer')) {
			  			if (!in_array($producto->getAttributeText('manufacturer'), $marcas)) {
			  				array_push($marcas, $producto->getAttributeText('manufacturer'));
			  			}


			  			// if (!in_array($disciplina, $disciplinas)) {
			  			// 	array_push($disciplinas, $disciplina);
			  			// }
			  			
			  			if (array_key_exists($producto->getAttributeText('manufacturer'), $clientes[$customer->getFirstname()])) {
			  				$clientes[$customer->getFirstname()][$producto->getAttributeText('manufacturer')]	+= (int)$item->getQtyOrdered();
			  			}else{
			  				$clientes[$customer->getFirstname()][$producto->getAttributeText('manufacturer')]	= (int)$item->getQtyOrdered();
			  			}

			  			// if (array_key_exists($disciplina, $clientes[$customer->getFirstname()])) {
			  			// 	$clientes[$customer->getFirstname()][$disciplina] += (int)$item->getQtyOrdered();
			  			// }else{
			  			// 	$clientes[$customer->getFirstname()][$disciplina] = (int)$item->getQtyOrdered();
			  			// }
			  		}
				}
			}

			$clientes[$customer->getFirstname()]['total_items'] += $total_items; 
			
			$vendedores[$vendedor->getNombre()] = $clientes;
		}
	}
	
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Reporte de ventas por vendedor</title>
	<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</head>
<body>
	<h3>Per&#237odo seleccionado: <?php echo $dateFrom.' / '.$dateTo ?></h3>

<?php
$filas = array();
$filas[] = array('Reporte de ventas por vendedor');
$filas[] = array('Per&#237odo seleccionado: '.$dateFrom.' / '.$dateTo);
$filas[] = array('');
?>

<?php foreach ($vendedores as $vendedor => $clientes): ?>
<?php $totalvendedor = 0; ?>

<?php
	$fila_h = array();
	$filas_v = array();
	array_push($fila_h, 'Nombre del cliente');
?>

		<h2>Vendedor: <?php echo $vendedor ?></h2>
		<table style="width: 50%;" id="<?php echo $cod_zona ?>" class="display table_id">
		    <thead>
		        <tr style="text-align:center;">
		            <th>Nombre del cliente</th>
		            <?php foreach ($marcas as $marca): ?>
		            	<th><?php echo 'Modulos marca: '.$marca ?></th>
		            
		            	<?php array_push($fila_h, 'Modulos marca: '.$marca); ?>

		            <?php endforeach ?>
		            <!-- <?php //foreach ($disciplinas as $disciplina): ?> -->
		            	<!-- //<th><?php //echo 'Modulos disciplina: '.$disciplina ?></th> -->
		            <!-- <?php //endforeach ?> -->
		            <th>Modulos totales por cliente</th>
		            <?php array_push($fila_h, 'Modulos totales por cliente'); ?>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($clientes as $cliente => $cantidad): ?>
		    		<?php $total = 0 ?>
		    		<?php $fila_c = array(); ?>
			        <tr style="text-align:center;">	
			            <td><?php echo $cliente; ?></td>
			            <?php array_push($fila_c, $cliente); ?>

			            <?php foreach ($marcas as $marca): ?>
			            	<?php if (array_key_exists($marca, $cantidad)): ?>

			            		<td><?php echo $cantidad[$marca] ?></td>
			            		<?php array_push($fila_c, $cantidad[$marca]); ?>

			            		<?php $total += $cantidad[$marca] ?>
			            	<?php else: ?>

			            		<td>0</td>
			            		<?php array_push($fila_c, 0); ?>

			            	<?php endif; ?>
			            <?php endforeach; ?>
			            <!-- <?php //foreach ($disciplinas as $disciplina): ?>
			            	<?php //if (array_key_exists($disciplina, $cantidad)): ?>
			            		<td><?php //echo $cantidad[$disciplina] ?></td>
			            	<?php //else: ?>
			            		<td>0</td>
			            	<?php //endif; ?>
			            <?php //endforeach; ?> -->
			            <td><?php echo $total ?></td>
			            <?php array_push($fila_c, $total); ?>
		    			<?php $totalvendedor += $total ?>
		    			<?php array_push($filas_v, $fila_c); ?>

			        </tr>
		    	<?php endforeach; ?> 

		    </tbody>
		    <tfoot>
		    	<h4>Total de módulos: <?php echo $totalvendedor; ?></h4> 	
		    </tfoot>
		</table>
		<form method="post" action="formulario_reportes.php">
			<div class="f bootstrap-iso orm-group" >
				<button class="btn btn-primary" name="submit" type="submit">Nuevo rango de fechas</button>
			</div>
		</form>

		<?php 

		$filas[] = array($vendedor);
		$filas[] = array('Módulos totales: ', $totalvendedor);
		$filas[] = array('');
		$filas[] = $fila_h;
		foreach ($filas_v as $fc) {
		 	$filas[] = $fc;
		 } 

		$filas[] = array('');
		$filas[] = array(''); 

		?>

	<?php endforeach ?>

<script type="text/javascript">
	$(document).ready( function () {
	    $('.table_id').DataTable();
	} );
</script>

<br>
<form action="export_reporte_por_vendedor.php" method="post">
	<textarea name="filas" style="display: none"><?php echo json_encode($filas) ?></textarea>
<input type="submit" style="color: #ffffff; background-color: #337ab7; border-color: #2e6da4; border: none; width: 170px; padding: 10px;
    border-radius: 4px; font-size: 14px; cursor: pointer; margin-bottom: 40px;" class="btn btn-primary" value='Exportar'>
</form>

</body>
</html>   