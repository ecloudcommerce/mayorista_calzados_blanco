<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');

$cod_zona = $_POST['vendedor'];
$dateFrom = $_POST['dateFrom'];
$dateTo = $_POST['dateTo'];

$from = strtotime('+3 hour',strtotime($dateFrom));  
$from = date ('Y-m-d H:i:s', $from); 

$to = strtotime('+3 hour',strtotime($dateTo));
$to = date ('Y-m-d H:i:s', $to); 

$orders_collection = Mage::getModel('sales/order')->getCollection()
		    ->addAttributeToFilter('created_at', array('from'=>$from , 'to'=>$to))
			->addAttributeToFilter('increment_id', array('like' => '00%'))
			->addAttributeToFilter('status', 'pending');
$total_ventas = 0;
$clientes = array();
$marcas = array();
$disciplinas = array();
$customer_model = Mage::getModel('customer/customer');
$catalog_product = Mage::getModel('catalog/product');
$vendedor = Mage::getModel("vendedores/vendedor")->getCollection()->getItemByColumnValue('cod_zona', $cod_zona);

foreach ($orders_collection as $order) {

	$postCode = $order->getShippingAddress()->getPostcode();
	$evaluar = 1;

	if ($postCode == $cod_zona) {

		$customer = $customer_model->load($order->getCustomerId());
		if (!array_key_exists($customer->getFirstname(), $clientes)) {
			$clientes[$customer->getFirstname()]	= array(
				'stock'=> 0,
				'preventa' => 0
				);
		}

		$total_ventas += 1;

		foreach ($order->getAllItems() as $item) {

			$producto = $catalog_product->loadByAttribute('sku',$item->getSku());
			$disciplina = $producto->getAttributeText('disciplina');
	  		$carryover 	= getCarryover($resource, $readConnection, $producto->getEntityId());
	  		$inventory 	= getCatalogInventory($resource, $readConnection, $producto->getEntityId());

	  		if ($producto->getAttributeText('manufacturer')) {
	  			if (!in_array($producto->getAttributeText('manufacturer'), $marcas)) {
	  				array_push($marcas, $producto->getAttributeText('manufacturer'));
	  			}

	  			if (!in_array($disciplina, $disciplinas)) {
	  				array_push($disciplinas, $disciplina);
	  			}
	  			
	  			if (array_key_exists($producto->getAttributeText('manufacturer'), $clientes[$customer->getFirstname()])) {
	  				$clientes[$customer->getFirstname()][$producto->getAttributeText('manufacturer')]	+= (int)$item->getQtyOrdered();
	  			}else{
	  				$clientes[$customer->getFirstname()][$producto->getAttributeText('manufacturer')]	= (int)$item->getQtyOrdered();
	  			}

	  			if (array_key_exists($disciplina, $clientes[$customer->getFirstname()])) {
	  				$clientes[$customer->getFirstname()][$disciplina] += (int)$item->getQtyOrdered();
	  			}else{
	  				$clientes[$customer->getFirstname()][$disciplina] = (int)$item->getQtyOrdered();
	  			}

	  			if($evaluar){
	  				if (!$carryover['value']) {
	  					if ($inventory['manage_stock']) {
	  						$stock_preventa = 'Stock';
	  					}else{
	  						$stock_preventa = 'Preventa';
	  					}
	  					$evaluar = 0;
	  				// break;
	  				}else{
	  					$stock_preventa = 'Stock';
	  				}
	  			}
	  		}
		}	

		if ($stock_preventa == 'Stock') {
			$clientes[$customer->getFirstname()]['stock'] += 1;
		}else{
			$clientes[$customer->getFirstname()]['preventa'] += 1;
		}
	}
}

function getProduct($resource, $readConnection, $sku) {
	$typeId = getEntityType($resource, $readConnection,'catalog_product');
	$attribute_id = getAttributeId($resource, $readConnection, 'marca', $typeId);
	$query = 'SELECT `product`.entity_id, `product`.sku , `varchar`.value  FROM ' . $resource->getTableName('catalog_product_entity') . ' as `product` INNER JOIN '.$resource->getTableName('catalog_product_entity_varchar').' as `varchar` ON `product`.`entity_id` = `varchar`.`entity_id` WHERE `sku` = "'.$sku.'" AND `attribute_id` = "'.$attribute_id.'";';
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	}else {
		return false;
	}
}

function getAttributeId($resource, $readConnection, $attributeCode, $entityType) {
	$query = 'SELECT `attribute_id` FROM ' . $resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "'.$attributeCode.'" and `entity_type_id` = "'.$entityType.'";'; 
	$results = $readConnection->fetchAll($query);	
	if(count($results) > 0){
		return $results[0]['attribute_id'];
	}
}

function getEntityType($resource, $readConnection, $entityCode) {
	$query = 'SELECT `entity_type_id` FROM ' . $resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "'.$entityCode.'";';
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0]['entity_type_id'];
	}
}

function getCatalogInventory($resource, $readConnection, $entity_id){
	$query = 'SELECT `cataloginventory`.manage_stock FROM ' . $resource->getTableName('cataloginventory_stock_item') . ' as `cataloginventory` WHERE `product_id` = "'.$entity_id.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getCarryover($resource, $readConnection, $entity_id){
	$typeId = getEntityType($resource, $readConnection,'catalog_product');
	$attribute_id = getAttributeId($resource, $readConnection, 'carryover', $typeId);
	$query = 'SELECT `carryover`.value FROM ' . $resource->getTableName('catalog_product_entity_int') . ' as `carryover` WHERE `entity_id` = "'.$entity_id.'" AND `attribute_id` = "'.$attribute_id.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Reporte de ventas</title>
	<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</head>
<body>
	<h2>Vendedor: <?php echo $vendedor->getNombre() ?></h2>
	<h3>Per&#237odo seleccionado: <?php echo $dateFrom.' / '.$dateTo ?></h3>

	<table style="width: 50%;" id="table_id" class="display">
	    <thead>
	        <tr style="text-align:center;">
	            <th>Nombre del cliente</th>
	        	<th>Cantidad de ventas</th>
	        	<th>Cantidad de ventas preventa</th>
	        	<th>Cantidad de ventas stock</th>
	            <?php foreach ($marcas as $marca): ?>
	            	<th><?php echo 'Modulos marca: '.$marca ?></th>
	            <?php endforeach ?>
	            <?php foreach ($disciplinas as $disciplina): ?>
	            	<th><?php echo 'Modulos disciplina: '.$disciplina ?></th>
	            <?php endforeach ?>
	            <th>Modulos totales por cliente</th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php foreach ($clientes as $cliente => $cantidad): ?>
	    		<?php $total = 0 ?>
		        <tr style="text-align:center;">
		            <td><?php echo $cliente; ?></td>
		            <td><?php echo $cantidad['stock']+$cantidad['preventa']; ?></td>
		            <td><?php echo $cantidad['preventa']; ?></td>
		            <td><?php echo $cantidad['stock']; ?></td>
		            <?php foreach ($marcas as $marca): ?>
		            	<?php if (array_key_exists($marca, $cantidad)): ?>
		            		<td><?php echo $cantidad[$marca] ?></td>
		            		<?php $total += $cantidad[$marca] ?>
		            	<?php else: ?>
		            		<td>0</td>
		            	<?php endif; ?>
		            <?php endforeach; ?>
		            <?php foreach ($disciplinas as $disciplina): ?>
		            	<?php if (array_key_exists($disciplina, $cantidad)): ?>
		            		<td><?php echo $cantidad[$disciplina] ?></td>
		            	<?php else: ?>
		            		<td>0</td>
		            	<?php endif; ?>
		            <?php endforeach; ?>
		            <td><?php echo $total ?></td>
		        </tr>
	    	<?php endforeach; ?> 
	    </tbody>
	    <tfoot>
    		<h3>Total de ventas del vendedor en el per&#237odo: <?php echo $total_ventas ?></h3>
	    </tfoot>
	</table>
	<form method="post" action="formulario_reportes.php">
		<div class="f bootstrap-iso orm-group" >
			<button class="btn btn-primary" name="submit" type="submit">Nuevo rango de fechas</button>
		</div>
	</form>
</body>
</html>
<script type="text/javascript">
	$(document).ready( function () {
	    $('#table_id').DataTable();
	} );
</script>


