<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();
?>
<!DOCTYPE html>
<html>
<head>
	<title>CBmayo Reportes</title>
	
	<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</head>
<body>
	<?php $vendedores = Mage::getModel("vendedores/vendedor")->getCollection(); ?>

	<div class="bootstrap-iso">
		<label><h3>Ingrese un rango de fechas</h3></label>
		<form method="post" action="reportes.php">
			<div class="form-group"> 
				<label class="control-label" for="date">Desde:</label><br/>
				<input style="width: 25%" class="form-control" name="dateFrom" value="<?php echo date('Y-m-d');?>" type="date"/> <br/>
				<label class="control-label" for="date">Hasta:</label><br/>
				<input style="width: 25%" class="form-control" name="dateTo" value="<?php echo date('Y-m-d');?>"  type="date"/> <br/>
			</div>
			<select name="vendedor">
				<option>Seleccione el Vendedor</option>
				<?php foreach ($vendedores as $vendedor):?>
					<option value="<?php echo $vendedor['cod_zona'] ?>"> <?php echo $vendedor->getNombre(); ?></option>
				<?php endforeach; ?>
			</select>
			<br/><br/>
			<div class="form-group"> 
				<button class="btn btn-primary" name="submit" type="submit">Reporte por productos</button>
			</div>
			<div class="form-group">
				<button  class="btn btn-primary" type="submit" formaction="reporte_por_clientes.php">Reporte por cliente</button>
			</div>
			<div class="form-group">
				<button  class="btn btn-primary" type="submit" formaction="reporte_stock_preventa_marca.php">Reporte stock preventa</button>
			<br/><br/>
			<div class="form-group">
				<button  class="btn btn-primary" type="submit" formaction="reporte_por_vendedor.php">Reporte por vendedor</button>
			</div>
			</div>
		</form>
	</div>
</body>
</html>
