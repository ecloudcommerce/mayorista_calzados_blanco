<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

$products = Mage::getModel('catalog/product')
    ->getCollection()
    //->addAttributeToSelect('*')
    ->addAttributeToSelect(array('name', 'sku', 'presale'))
    ->joinField(
        'qty',
        'cataloginventory/stock_item',
        'qty',
        'product_id=entity_id',
        '{{table}}.stock_id=1',
        'left'
    );

$products->addFieldToFilter(array(
    array('attribute'=>'presale','eq'=>'0')     
));

$p = array();

foreach ($products as $product) {
    $p[$product->getSku()] = $product->getQty();
}

$stock_productos = json_encode($p, JSON_FORCE_OBJECT);

echo $stock_productos;

?>