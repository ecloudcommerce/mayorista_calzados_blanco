<?php  

require_once(dirname(__DIR__).'/../app/Mage.php');

ini_set('max_execution_time', 6000);
umask(0);
Mage::app();
ini_set('display_errors', 1);

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$writeConnection = $resource->getConnection('core_write');

$folder = Mage::getBaseDir('var') . "/log";
if (!is_dir($folder)) {
    mkdir($folder);
}
$path = $folder . "/stock_movements.log";

// Crea el archivo si no existe y le da permisos
file_put_contents($path, "", FILE_APPEND);
chmod($path, 0777);

try{
    $current = date('Y-m-d');
    $time_config = Mage::getStoreConfig('limpieza/stock_move/tiempo');
    $fecha_hasta =  date('Y-m-d H:i:s', strtotime("-".$time_config." days"));
    $query = "DELETE from `bubble_stock_movement` where `created_at` < '".$fecha_hasta."'";
    $writeConnection->Query($query);
    Mage::log('Corrio correctamente. Fecha: '.$current, null, "stock_movements.log", true);
}
catch (Exception $e) {
    Mage::log($e->getMessage().'. Fecha: '.$current, null, "stock_movements.log", true);
}