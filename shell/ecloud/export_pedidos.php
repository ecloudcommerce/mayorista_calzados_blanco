<?php
require_once('../../app/Mage.php'); //Path to Magento
ini_set('max_execution_time', 6000);
umask(0);
Mage::app();

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$order_collection 	= Mage::getModel('sales/order')->getCollection()->addFieldToFilter('status','pending');
// $model_product 		= Mage::getModel('catalog/product');
// $inventory_product 	= Mage::getModel('cataloginventory/stock_item');
$orders 			= array();

foreach ($order_collection as $order) {

	$order_Address = $order->getShippingAddress()->getData();
	$order_items = array();	
	$cantidad_modulos = 0;
	$evaluar = 1;
	
  	foreach ($order->getAllItems() as $item) {

  		// $product 	= $model_product->load($item->getProductId());
  		// $inventory 	= $inventory_product->loadByProductId($product);
  		$product 	= getProduct($resource, $readConnection, $item->getSku());
  		$carryover 	= getCarryover($resource, $readConnection, $product['entity_id']);
  		$inventory 	= getCatalogInventory($resource, $readConnection, $product['entity_id']);

  		if($evaluar){
	  		if (!$carryover['value']) {
	  			if ($inventory['manage_stock']) {
	  				$stock_preventa = 'Stock';
	  			}else{
	  				$stock_preventa = 'Preventa';
	  			}
				$evaluar = 0;
	  		}else{
	  			$stock_preventa = 'Stock';
	  		}
  		}

  		$cantidad_modulos = $cantidad_modulos + $item->getQtyOrdered();

    	// $order_items[] = array(
     //    	'sku'           => $item->getSku(),
     //    	'name'          => $item->getName(),
     //    	'price'         => $item->getPrice(),
     //    	'qty_ordered'   => $item->getQtyOrdered(),
        	// 'image'			=> $product->getSmallImage()
    	// );
   	}


	$orders[$order['entity_id']]['increment_id'] 	 = $order['increment_id'];
	$orders[$order['entity_id']]['grand_total']		 = $order['grand_total'];
	$orders[$order['entity_id']]['total_item_count'] = $order['total_item_count'];
	$orders[$order['entity_id']]['created_at'] 		 = $order['created_at'];
	$orders[$order['entity_id']]['status'] 			 = $order['status'];
	$orders[$order['entity_id']]['firstname'] 		 = $order['customer_firstname'];
	$orders[$order['entity_id']]['lastname'] 		 = $order['customer_lastname'];
	$orders[$order['entity_id']]['customer_email']	 = $order['customer_email'];
	$orders[$order['entity_id']]['telephone']	 	 = $order_Address['telephone'];
	$orders[$order['entity_id']]['postcode']	 	 = $order_Address['postcode'];
	$orders[$order['entity_id']]['stock_preventa']	 = $stock_preventa;
	$orders[$order['entity_id']]['cantidad_modulos'] = $cantidad_modulos;
	// $orders[$order['entity_id']]['items'] 	 	     = $order_items;
}                    

$rta = json_encode($orders, JSON_FORCE_OBJECT);

echo $rta;

function getProduct($resource, $readConnection, $sku) {
	$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity') . ' WHERE `sku` = "'.$sku.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getCatalogInventory($resource, $readConnection, $entity_id){
	$query = 'SELECT * FROM ' . $resource->getTableName('cataloginventory_stock_item') . ' WHERE `product_id` = "'.$entity_id.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getCarryover($resource, $readConnection, $entity_id){
	$typeId = getEntityType($resource, $readConnection,'catalog_product');
	$attribute_id = getAttributeId($resource, $readConnection, 'carryover', $typeId);
	$query = 'SELECT * FROM ' . $resource->getTableName('catalog_product_entity_int') . ' WHERE `entity_id` = "'.$entity_id.'" AND `attribute_id` = "'.$attribute_id.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0];
	} else {
		return false;
	}
}

function getAttributeId($resource, $readConnection, $attributeCode, $entityType) {
	$query = 'SELECT `attribute_id` FROM ' . $resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "'.$attributeCode.'" and `entity_type_id` = "'.$entityType.'";'; 
	$results = $readConnection->fetchAll($query);	
	if(count($results) > 0){
		return $results[0]['attribute_id'];
	}
}

function getEntityType($resource, $readConnection, $entityCode) {
	$query = 'SELECT `entity_type_id` FROM ' . $resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "'.$entityCode.'";'; 
	$results = $readConnection->fetchAll($query);
	if(count($results) > 0){
		return $results[0]['entity_type_id'];
	}
}
?>