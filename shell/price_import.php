<?php
require_once 'brandlive_abstract.php';

class Integration_Price_Import extends Brandlive_Shell_Abstract
{
    public function run(){
 		Mage::helper('winwin_opsintegration/data')->setWinWinUserIsCron(true);
		Mage::getModel('winwin_opsintegration/Priceimporterp')->getCsvPriceFileToMagento();
    }
   
}

$shell = new Integration_Price_Import();
$shell->run();

