<?php

require_once 'brandlive_abstract.php';

class Integration_Stock_Import extends Brandlive_Shell_Abstract
{
    public function run(){
 		Mage::helper('winwin_opsintegration/data')->setWinWinUserIsCron(true);
		Mage::getModel('winwin_opsintegration/Stockimporterp')->getCsvStockFileToMagento();
    }
   
}

$shell = new Integration_Stock_Import();
$shell->run();
