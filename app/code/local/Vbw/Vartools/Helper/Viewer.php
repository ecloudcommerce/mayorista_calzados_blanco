<?php



class Vbw_Vartools_Helper_Viewer
    extends Mage_Core_Helper_Abstract
{

    /**
     * use to validate the input and throw exception to deliver the message error.
     *
     * @param array $input
     * @throws Exception
     */
    public function validateInput ($input)
    {
        /** @var $helper Vbw_Vartools_Helper_Data */
        $helper = Mage::helper('vbw_vartools');
        if (isset($input['directory']) && !empty($input['directory'])) {
            if (!$helper->isVarDirectory($input['directory'])) {
                Mage::throwException('Requested directory is not a var directory.');
            }
            if (isset($input['file']) && !empty($input['file'])) {
                if (!$helper->isVarFile($input['directory'],$input['file'])) {
                    Mage::throwException('Requested file is not a var directory/file.');
                }
            }
        }
    }

    /**
     * get the directories allowed to be accessed by this script.
     * @todo possibly alter to show a limited set.
     *
     * @return array
     */
    public function getLogDirectories ()
    {
        /** @var $helper Vbw_Vartools_Helper_Data */
        $helper = Mage::helper('vbw_vartools');
        $directories = $helper->getVarDirectories();
        return $directories;
    }

    /**
     * execute the grep and return the results
     *
     * @param $file
     * @param $patterns
     * @return string
     */
    public function grep ($file,$patterns)
    {
        $cmd = $this->getGrepCmd($file,$patterns);
        $output = $this->execute($cmd);
        return $output;
    }

    /**
     * execute a simple command
     *
     * @param $command
     * @return string
     */
    public function execute ($command)
    {
        $output = '';
        $process = popen($command,'r');
        if ($process) {
            while (!feof($process)) {
                $output .= fread($process,256);
            }
            pclose($process);
        }
        return preg_split("/(\r|\n|\r\n)/",$output);
    }

    public function getGrepCmd ($file,$patterns)
    {
        $return  = $this->getLess($file);
        $return .= $this->getGrep($patterns);
        return $return;
    }

    /**
     * get the less/view part of the statement
     *
     * @param $file
     * @return string
     */
    public function getLess ($file)
    {
        $return = "less \"{$file}\" ";
        return $return;
    }

    /**
     * get the "grep" part of the statements
     *
     * @param $patterns
     * @return string
     */
    public function getGrep ($patterns)
    {
        $return = '';
        $patterns = (array) $patterns;
        foreach ($patterns AS $pattern) {
            if (!empty($pattern)) {
                $return .= "| grep \"{$this->sanitizeGrep($pattern)}\" ";
            }
        }
        return $return;
    }

    /**
     * sanitize pattern for safety when executing.
     *
     * @param $pattern
     * @return string
     */
    public function sanitizeGrep ($pattern)
    {
        return escapeshellcmd($pattern);
    }

}
