<?php



class Vbw_Vartools_Helper_Manager
    extends Mage_Core_Helper_Abstract
{

    /**
     * @return Vbw_Vartools_Helper_Data
     */
    public function getHelper ()
    {
        return Mage::helper('vbw_vartools');
    }

    /**
     * use to validate the input and throw exception to deliver the message error.
     *
     * @param array $input
     * @throws Exception
     */
    public function validateInput ($input)
    {
        /** @var $helper Vbw_Vartools_Helper_Data */
        $helper = Mage::helper('vbw_vartools');
        if (isset($input['directory']) && !empty($input['directory'])) {
            if (!$helper->isVarDirectory($input['directory'])) {
                Mage::throwException('Requested directory is not a var directory.');
            }
            if (isset($input['file']) && !empty($input['file'])) {
                if (!$helper->isVarFile($input['directory'],$input['file'])) {
                    Mage::throwException('Requested file is not a var directory/file.');
                }
            }
        }
    }

    /**
     * get the directories allowed to be accessed by this script.
     * @todo possibly alter to show a limited set.
     *
     * @return array
     */
    public function getManagedDirectories ()
    {
        /** @var $helper Vbw_Vartools_Helper_Data */
        $helper = Mage::helper('vbw_vartools');
        $directories = $helper->getVarDirectories();
        return $directories;
    }


    public function deleteFiles ($dir,$files)
    {
        $helper = $this->getHelper();
        $files = (array) $files;
        if ($helper->isVarFile($dir,$files)) {
            foreach ($files as $file) {
                $src = $helper->getVarFilePath($dir,$file);
                unlink($src);
            }
            return true;
        }
        return false;
    }

    public function renameFile($dir,$file,$newName)
    {
        $helper = $this->getHelper();
        if ($helper->isVarFile($dir,$file)
                && $helper->isVarLocation($dir,$newName)) {
            $from = $helper->getVarFilePath($dir,$file);
            $to = $helper->getVarFilePath($dir,$newName);
            rename($from,$to);
            return true;
        }
        return false;
    }

    /**
     * @param $dir
     * @param $file
     * @param Zend_Action_Http_Response $response
     */
    public function downloadFile($dir,$file,$response)
    {
        $helper = $this->getHelper();
        if ($helper->isVarFile($dir,$file)) {
            $filePath = $helper->getVarFilePath($dir,$file);

            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename = " . $file);
            header("Cache-Control: must-revalidate");
            header("Pragma: public");
            header("Expires: 0");
            header("Content-Length: " . filesize($filePath));

            ob_clean();     //Just in case
            flush();
            echo file_get_contents($filePath);
            exit;


        }
        return false;
    }

    /**
     * @param $dir
     * @param $files
     */
    public function compressFiles ($dir,$files)
    {
        $helper = $this->getHelper();
        $files = (array) $files;
        if ($helper->isVarFile($dir,$files)) {
            if (class_exists('ZipArchive')) {

            }
        }
    }
}
