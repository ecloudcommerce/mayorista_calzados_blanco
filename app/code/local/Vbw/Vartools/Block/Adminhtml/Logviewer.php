<?php

class Vbw_Vartools_Block_Adminhtml_Logviewer
    extends Mage_Adminhtml_Block_Template
{

    /**
     * actually execute the grep
     * only execute if all the needed data is there.
     *
     * @return null|string
     */
    public function getGrepResults ()
    {
        $directory = $this->getDirectory();
        $file = $this->getFile();

        /** @var $dataHelper Vbw_Vartools_Helper_Data */
        $dataHelper = Mage::helper('vbw_vartools');
        if (!empty($directory)
                && !empty($file)
                && $dataHelper->isVarDirectory($directory)
                && $dataHelper->isVarFile($directory,$file)) {

            $helper = $this->getViewerHelper();
            $grep = $this->getGrep();

            $filePath = $dataHelper->getVarFilePath($directory,$file);
            $results = $helper->grep($filePath,$grep);

            if (empty($results)) {
                return "[no matches]";
            }
            return $results;
        }
        return null;
    }

    /**
     * @return mixed|null
     */
    public function getDirectory ()
    {
        $request = Mage::app()->getRequest();
        if ($request->has('directory')) {
            return $request->getParam('directory');
        }
        return null;
    }

    /**
     * @return mixed|null
     */
    public function getFile ()
    {
        $request = Mage::app()->getRequest();
        if ($request->has('file')) {
            return $request->getParam('file');
        }
        return null;
    }

    /**
     * @return mixed|null
     */
    public function getGrep ()
    {
        $request = Mage::app()->getRequest();
        if ($request->has('grep')) {
            return $request->getParam('grep');
        }
        return null;
    }

    /**
     * @return Vbw_Vartools_Helper_Viewer
     */
    public function getViewerHelper()
    {
        return Mage::helper('vbw_vartools/viewer');
    }

    public function getLogDirectories ()
    {
        return $this->getViewerHelper()->getLogDirectories();
    }

    public function getDirectoryFiles ($directory = null)
    {
        if (!empty($directory)) {
            /** @var $helper Vbw_Vartools_Helper_Data */
            $helper = Mage::helper('vbw_vartools');
            if ($helper->isVarDirectory($directory)) {
                $files = $helper->getVarDirectoryFiles($directory);
                return $files;
            }
        }
        return null;
    }
    
}