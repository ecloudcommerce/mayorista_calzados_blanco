<?php

class Vbw_Vartools_Block_Adminhtml_Filemanager
    extends Mage_Adminhtml_Block_Template
{

    /**
     * @return mixed|null
     */
    public function getDirectory ()
    {
        $request = Mage::app()->getRequest();
        if ($request->has('directory')) {
            return $request->getParam('directory');
        }
        return null;
    }

    /**
     * @return mixed|null
     */
    public function getFile ()
    {
        $request = Mage::app()->getRequest();
        if ($request->has('file')) {
            return $request->getParam('file');
        }
        return null;
    }

    /**
     * @return Vbw_Vartools_Helper_Viewer
     */
    public function getManagerHelper()
    {
        return Mage::helper('vbw_vartools/manager');
    }

    public function getManagedDirectories ()
    {
        return $this->getManagerHelper()->getManagedDirectories();
    }

    public function getDirectoryFiles ($directory = null)
    {
        if (!empty($directory)) {
            /** @var $helper Vbw_Vartools_Helper_Data */
            $helper = Mage::helper('vbw_vartools');
            if ($helper->isVarDirectory($directory)) {
                $files = $helper->getVarDirectoryFiles($directory);
                return $files;
            }
        }
        return null;
    }

    public function formatSize ($size)
    {
        $size = (int) $size;
        $suffix = 'bytes';
        if ($size > 1000) {
            $suffix = 'KB';
            $size = $size/1000;
            if ($size > 1000) {
                $suffix = 'MB';
                $size = $size/1000;
            }
            $size = number_format($size,2);
        }
        return $size ." ". $suffix;
    }

}