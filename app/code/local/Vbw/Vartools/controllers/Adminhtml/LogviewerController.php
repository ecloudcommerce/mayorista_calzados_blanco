<?php

class Vbw_Vartools_Adminhtml_LogviewerController
    extends Mage_Adminhtml_Controller_Action
{

    protected function _construct()
    {
        // Define module dependent translate
        // $this->setUsedModuleName('Mage_ImportExport');
    }

    /**
     * Initialize layout.
     *
     * @return Mage_ImportExport_Adminhtml_ImportController
     */
    protected function _initAction()
    {
        $this->_title($this->__('Log Viewer'))
            ->loadLayout();
        return $this;
    }

    /**
     * Check access (in the ACL) for current user.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true; //Mage::getSingleton('admin/session')->isAllowed('system/convert/import');
    }

    /**
     * Index action.
     *
     * @return void
     */
    public function indexAction()
    {
        try {
            Mage::helper('vbw_vartools/viewer')->validateInput($this->getRequest()->getParams());
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_initAction();

        $this->renderLayout();
    }

    /**
     *Delete the selected files.
     *
     */
    private function _delete() {
        if (isset($_GET['file_names'])){
                    $files = $_GET['file_names']; //($this->getRequest()->getParam('file_names'));
        }
        else {
            //Error -- log it
            Mage::log("Error: _GET was null.\n", null, "file_errors.log", true);
            return;
        }
        $dir = Mage::getRoot() . '/../var/' . $this->getRequest()->getParam('dir') . '/';

        foreach ($files as $file) {
            $success = unlink($dir . $file);
            if (!$success) {
                //Error -- for now, just log it.
                Mage::log("Error: " . $dir . $file . " does not exist.\n", null, "file_errors.log", true);
            }
        }
    }

    /**
     *  Download the selected file. (Only supports a single file at the moment.)
     */
    private function _download(){
        if (isset($_GET['file_names'])){
            $files = $_GET['file_names']; //($this->getRequest()->getParam('file_names'));
        }
        else {
            //Error -- log it
            Mage::log("Error: _GET was null.\n", null, "file_errors.log", true);
            return;
        }
        $dir = Mage::getRoot() . '/../var/' . $this->getRequest()->getParam('dir') . '/';

        /** Eventually, it would be nice if you could download multi files as a zip.
       *  If it does change, change $files[0] to just $files in header() section below.
            if (is_array($files)){

            $dl = new ZipArchive();
            if ($dl->open($dir, true ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false; //Fail
            }
            foreach ($files as $file) {
                $dl->addFile($dir . $file, $dir . $file);
            }

            $dl->close(); //Done
        }
        else {
            $dl = $dir . $files;
        } **/

//            header("Content-Type: application/zip");
  //          header("Content-Length: " . filesize($dl));
        //header("Content-Disposition:attachment;filename='downloads" . date("Y-m-d") . "'");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename = " . $files[0]);
            header("Cache-Control: must-revalidate");
            header("Pragma: public");
            header("Expires: 0");
            header("Content-Length: " . filesize($dir . $files[0]));

            ob_clean();     //Just in case
            flush();
            echo file_get_contents($dir . $files[0]);
            exit;
    }

    /**
     *Rename the selected file--can only do one a time.
     * */
    private function _rename(){
        if (isset($_GET['file_names'])){
            $files = $_GET['file_names']; //($this->getRequest()->getParam('file_names'));
        }

        else {
            //Error -- log it
            Mage::log("Error: _GET was null.\n", null, "file_errors.log", true);
            return;
        }

        $dir = Mage::getRoot() . '/../var/' . $this->getRequest()->getParam('dir') . '/';
        $fileInfo = pathinfo($this->getRequest()->getParam('renamed_file'));
        $fileName = $fileInfo['filename'];
        if (isset($fileInfo['extension'])){
            $fileExt = $fileInfo['extension'];
        }
        else {
            $fileInfo = pathinfo($files[0]);
            if (isset($fileInfo['extension'])) {
                $fileExt = $fileInfo['extension'];
            }
            else {
                $fileExt = "txt"; //Because it has to be something.
            }
        }
        if (file_exists($dir . $files[0])) {
            rename($dir . $files[0], $dir . $fileName . "." .  $fileExt);
        }
        else {
            return; //Error
        }


    }

}