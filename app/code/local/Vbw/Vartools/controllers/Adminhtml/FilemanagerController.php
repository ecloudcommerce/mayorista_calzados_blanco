<?php

class Vbw_Vartools_Adminhtml_FilemanagerController
    extends Mage_Adminhtml_Controller_Action
{

    protected function _construct()
    {
        // Define module dependent translate
        // $this->setUsedModuleName('Mage_ImportExport');
    }

    /**
     * Initialize layout.
     *
     * @return Mage_ImportExport_Adminhtml_ImportController
     */
    protected function _initAction()
    {
        $this->_title($this->__('File Manager'))
            ->loadLayout();

        return $this;
    }

    /**
     * Check access (in the ACL) for current user.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true; //Mage::getSingleton('admin/session')->isAllowed('system/convert/import');
    }

    /**
     * Index action.
     *
     * @return void
     */
    public function indexAction()
    {
        if (!empty($_FILES)){
            /**
             * @var $managerHelper Vbw_Vartools_Helper_Manager
             * @var $dataHelper Vbw_Vartools_Helper_Data
             */
            $managerHelper = Mage::helper('vbw_vartools/manager');
            $dataHelper = Mage::helper('vbw_vartools');
            $directory = $this->getRequest()->getParam('directory');
            if (!empty($directory)
                    && $dataHelper->isVarDirectory($directory)) {
                foreach ($_FILES as $file) {
                    $temp_location = $file['tmp_name'];
                    $name = $file['name'];
                    if ($dataHelper->isVarLocation($directory,$name)) {
                        $newLocation = $dataHelper->getVarFilePath($directory,$name);
                        if(move_uploaded_file($temp_location,$newLocation)){
                            $this->_getSession()->addSuccess(
                                $this->__($file['name'] . " was successfully uploaded.\n"));
                        }
                    }
                }
            }
        }

        if ($this->getRequest()->has('file_action')) {
            /**
             * @var $managerHelper Vbw_Vartools_Helper_Manager
             * @var $dataHelper Vbw_Vartools_Helper_Data
             */
            $managerHelper = Mage::helper('vbw_vartools/manager');
            $dataHelper = Mage::helper('vbw_vartools');
            $directory = $this->getRequest()->getParam('directory');
            if (!empty($directory)) {
                $selected_files = $this->getRequest()->getParam('selection');
                if (empty($selected_files)) {
                    $this->_getSession()->addError('No files were selected.');
                } elseif ($dataHelper->isVarFile($directory,$selected_files)) {
                    //Get the action from the button clicked
                    $fileAction = $this->getRequest()->getParam('file_action');
                    try {
                        switch ($fileAction) {
                            case 'delete' :
                                if ($managerHelper->deleteFiles($directory,$selected_files)) {
                                    $this->_getSession()->addSuccess('Selected file(s) were removed.');
                                }
                                break;
                            case 'rename' :
                                if (count($selected_files) > 1) {
                                    throw new Exception('Rename can only be used with one file at a time.');
                                }
                                $selected_file = $selected_files[0];
                                $newName = $this->getRequest()->getParam('renamed_file');
                                if (!empty($newName)) {
                                    if ($managerHelper->renameFile($directory,$selected_file,$newName)) {
                                        $this->_getSession()->addSuccess('Selected file was renamed.');
                                    }
                                }
                                break;
                            case 'download' :
                                if (count($selected_files) > 1) {
                                    throw new Exception('Download can only be use to download one file at a time.');
                                }
                                $selected_file = $selected_files[0];
                                $response = $managerHelper->downloadFile($directory,$selected_file,$this->getResponse());
                                if ($response) {
                                   return;
                                } else {
                                    throw new Exception('An error occurred preparing your file.');
                                }
                                break;
                            case 'compress' :
                                Mage::throwException('Compression function is not yet available');
                                break;
                            default :
                                Mage::throwException('File action not found.');
                        }
                    } catch (Exception $e) {
                        $this->_getSession()->addError('File action error : '. $e->getMessage());
                    }
                } else {
                    $this->_getSession()->addError('Selected directory/file(s) are not valid for actions.');
                }
            } else {
                $this->_getSession()->addError('No var directory is defined.');
            }
        }

        $this->_initAction()
            ->_title($this->__('Var File Manager'));
            // ->_addBreadcrumb($this->__('Import'), $this->__('Import'));

        $this->renderLayout();
    }

}