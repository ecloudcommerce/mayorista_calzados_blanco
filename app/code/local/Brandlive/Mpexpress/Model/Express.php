<?php
/**
 *
 * @category   Brandlive
 * @package    Brandlive_Mpexpress
 * @author     Gastón De Mársico <gdemarsico@ulula.net>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Brandlive_Mpexpress_Model_Express extends Mpexpress_Model_Express{

    /*
    *   Rewirte function getInitPoint
    *   Added "auto_return":"approved" parameter.
    *   Added failure Url parameter.
    */
    public function getInitPoint() {

        $orderIncrementId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $name = '#' . $orderIncrementId . ' - ';
        $model = Mage::getModel('catalog/product');

        $quote = Mage::getSingleton('checkout/session')->getQuote();

        foreach ($order->getAllVisibleItems() as $item) {
            //modificado por e-values para permitir el manejo de kits
            if (strpos($item->getSku(), '-') !== false) {
                $skus = explode("-", $item->getSku());
                $prod = $model->loadByAttribute('sku', $skus[0]);
            } else {
                $prod = $model->loadByAttribute('sku', $item->getSku());
            }

            //get methods and each find getImage
            $methods = get_class_methods($prod);
            foreach($methods as $method):
                if($method == "getImageUrl"):
                    $image[] = $prod->getImageUrl();
                endif;
            endforeach;


            $name .= $item->getName();
        }


        //Shipping
        //check method exist :)
        if(method_exists($order->getShippingAddress(), "getData")){
          $shipping = $order->getShippingAddress()->getData();
          $shipments = array(
              "receiver_address" => array(
              "floor" => "-",
              "zip_code" => $shipping['postcode'],
              "street_name" => $shipping['street'] . " - " . $shipping['city'] . " - " . $shipping['country_id'],
              "apartment" => "-",
              "street_number" => "-"
              )
          );
        }

        //Force format YYYY-DD-MMTH:i:s
        $date_creation_user = date('Y-m-d',$customer->getCreatedAtTimestamp()) . "T" . date('H:i:s',$customer->getCreatedAtTimestamp());

        $billing_address = $order->getBillingAddress();
        $billing_address = $billing_address->getData();

        $payer = array(
            "name" => htmlentities($customer->getFirstname()),
            "surname" => htmlentities($customer->getLastname()),
            "email" => htmlentities($customer->getEmail()),
            "date_created" => $date_creation_user,
            "phone" => array(
                "area_code" => "-",
                "number" => $shipping['telephone']
            ),
            "address" => array(
                "zip_code" => $billing_address['postcode'],
                "street_name" => $billing_address['street'] . " - " . $billing_address['city'] . " - " . $billing_address['country_id'],
                "street_number" => "-"
            ),
            "identification" => array(
                "number" => "null",
                "type" => "null"
            )
        );

        //items
        $item_price = $order->getBaseGrandTotal();
        if (!$item_price) {
            $item_price = $order->getBasePrice() + $order->getBaseShippingAmount();
        }

        $item_price = number_format($item_price, 2, '.', '');


        //case no exist function getImage in the $prod no generate item on the array
        $image_items = "";
        if (count($image) > 0):
            $image_items = $image[0];
        endif;

        $items = array(
            array (
            "id" => $orderIncrementId,
            "title" => utf8_encode($name),
            "description" => utf8_encode($name),
            "quantity" => 1,
            "unit_price" => round($item_price, 2),
            "currency_id" => $this->getConfigData('currency'),
            "picture_url"=> $image_items,
            "category_id"=> $this->getConfigData('category_id')
            )
        );

        $installments = (int)$this->getConfigData('installments');

        //send null installment case send 0 or empty
        if($installments == 0 || $installments == ""):
            $installments = null;
        endif;


        //payment_methods
        $exclude = $this->getConfigData('excluded_payment_methods');
        if($exclude != ''):
        //case exist exclude methods
            $excludemethods = array();
            $methods_excludes = preg_split("/[\s,]+/", $exclude);
            foreach ($methods_excludes as $exclude ){
                $excludemethods[] = array('id' => $exclude);
            }

            $payment_methods = array(
                "installments" => $installments,
                "excluded_payment_methods" => $excludemethods
            );
        else:
            //case not exist exclude methods
            $payment_methods = array(
                "installments" => $installments
            );
        endif;


        //set back url
        $back_urls = array(         
            "failure" => $this->getConfigData('url_failure'),
            "pending" => $this->getConfigData('url_success'),
            "success" => $this->getConfigData('url_process')
        );

        
        //mount array pref
        $pref = array();
        $pref['external_reference'] = 'mpexpress-' . $orderIncrementId;
        $pref['payer'] = $payer;
        $pref['shipments'] = $shipments;
        $pref['items'] = $items;
        $pref['auto_return'] = 'approved';
        $pref['back_urls'] = $back_urls;
        $pref['payment_methods'] = $payment_methods;
        $sponsor_id = $this->getConfigData('sponsor_id');

        if ($sponsor_id) $pref['sponsor_id'] = (int)$sponsor_id;

        if  ((bool)$this->getConfigData('expires')){
                                    
            $expiration_duration = $this->getConfigData('expiration_duration');
            
            if (is_numeric($expiration_duration)){
                $default_timezone = date_default_timezone_get();
                date_default_timezone_set(Mage::getStoreConfig('general/locale/timezone'));
                $P = date('P');
                date_default_timezone_set($default_timezone);
                $current_timestamp    = Mage::getModel('core/date')->timestamp(time());
                $expiration_timestamp = strtotime("+" . $expiration_duration .  " minutes", $current_timestamp);
                $expiration_date_from = date('Y-m-d', $current_timestamp) . 'T' . date('H:i:s',$current_timestamp) . '.000' . $P;
                $expiration_date_to   = date('Y-m-d', $expiration_timestamp) . 'T' . date('H:i:s',$expiration_timestamp) . '.000' . $P;
                $pref['expires'] = true;                    
                $pref['expiration_date_from'] = $expiration_date_from;
                $pref['expiration_date_to']   = $expiration_date_to;
            } 

        }
        

        
        $sandbox = $this->getConfigData('sandbox_checkout') == 1 ? true: false;

        return Mage::getModel('mpexpress/checkout')->GetCheckout($pref, $sandbox);
    }
}

