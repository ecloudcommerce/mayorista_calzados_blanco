<?php
require_once(Mage::getBaseDir('lib') . '/mercadopago/mercadopago.php');
class Brandlive_Mpexpress_Model_Observer{

	const LOG_FILE = 'cancelation_cron.log';
	const RECOVER_CRON_LOG_FILE = 'mp_ordenes_no_pagadas.log';
	const MP_EMAIL_SENDED = 1;
	const MP_EMAIL_NOT_SENDED = 0;

	public function cancelOrdersAction(){
		//$this->_cancelOrders(2,2);
		foreach (Mage::app()->getStores() as $store) {		
			$this->_cancelOrders($store->getWebsiteId(), $store->getId());			
		}		
	}


	public function recoverPendingPaymentOrdersAction(){
		//$this->_recoverPendingPaymentOrders(2,2);
		foreach (Mage::app()->getStores() as $store) {	
			$this->_recoverPendingPaymentOrders($store->getWebsiteId(), $store->getId());			
		}		
	}

	protected function _cancelOrders($website, $store){

		Mage::app()->setCurrentStore($store);			
			
		$helper = Mage::helper('brandlive_mpexpress/data');
		if (! $helper->getCancelationCronEnabled($website) || ! $helper->getExpiresEnabled($website)) return false;
		
		$expirationDuration = $helper->getExpirationDuration($website) + $helper->getCancelationCronExpiration($website);		

		$logEnabled = $helper->getCancelationCronLogEnabled($website);

		/* Format our dates */
		$date    = Mage::getModel('core/date');
		$currentTimestamp    = $date->timestamp(time());
		$expirationTimestamp = strtotime("-" . $expirationDuration .  " minutes", $currentTimestamp);
		$toDate = $date->gmtDate('Y-m-d H:i:s', $expirationTimestamp);

		$limit = $helper->getCancelationCronCollectionLimit($website);
					
		/* Get the collection */
		$orders = Mage::getModel('sales/order')->getCollection()				
				->join(
        				array('payment' => 'sales/order_payment'),
        					'main_table.entity_id = payment.parent_id', array('payment_method' => 'payment.method')
    				  )	
				->addAttributeToFilter('payment.method', array('eq' => 'mpexpress'))
	    		->addAttributeToFilter('created_at', array('to' => $toDate))
	    		->addAttributeToFilter('store_id', array('eq' => $store))
	    		->addAttributeToFilter('status', array('in' => $helper->getCancelationCronOrderStatuses()))
	    		->addAttributeToSort('increment_id', 'DESC')
	    		->setPageSize($limit);
	    		    	
	    if ($orders->count() > 0){	    		    	
			$ipn = Mage::getModel('mpexpress/Checkout');    	    	

		   	foreach ($orders as $order) {		   	
		   		$incrementId = $order->getIncrementId();

		   		$externalId  = 'mpexpress-' . $incrementId;

		   		$payments = null;
		   		
		   		try{
		   			$payments = $ipn->SearchPayment($externalId);		   			
		   		}catch (Exception $e) {
		   			if ($logEnabled) Mage::log('Error geting payments for order: ' . $incrementId . ' - ' . $e->getMessage(), Zend_Log::ERR, self::LOG_FILE);
			    	continue;
		    	}

		    	//Si MP no me devolvio nada no toco la orden
		    	if ( $payments === null || $payments === false ){
					if ($logEnabled) Mage::log('Error geting payments for order: ' . $incrementId . ' - Nothing returned', Zend_Log::ERR, self::LOG_FILE);		    		
		    		continue;	
		    	} 

		    	//Si P devolvio algo para no hay transaccion asociado, cancelo la orden
		    	if ( count($payments['results']) == 0 ){ 
		    		$this->_cancelOrder($order, $logEnabled);
		    		continue;
		    	}

		    	//Ordeno los pagos para obtener el ultimo
		    	$results = $payments['results'];		    	

		    	if ( count($results) > 1 ){
			    	usort($results, function($a, $b) {
					   return strtotime($b['collection']['last_modified']) - strtotime($a['collection']['last_modified']);
					});
				}

				$last_payment = reset($results);

				//si el ultmo pago de la orden es cancelado, cancelo la orden.
				//si es aprobado, la apruebo
				if ($last_payment['collection']['status'] == 'cancelled'){
					$this->_cancelOrder($order, $logEnabled);				
				}elseif ($last_payment['collection']['status'] == 'approved' && $last_payment['collection']['status_detail'] == 'accredited') {
					$this->_aproveOrder($order, $logEnabled);
				}elseif ($logEnabled){
					Mage::log('Orden NO modificada: ' . $incrementId . ' status: ' .  $last_payment['collection']['status'] . ' status_detail: ' . $last_payment['collection']['status_detail'], Zend_Log::INFO, self::LOG_FILE);
				}			
		   	}
		   	
		}				
	}

	protected function _cancelOrder($order, $logEnabled){	
		$incrementId = $order->getIncrementId();
		if ($order->canCancel()){
			try{
				//Copiado del método Cancel de la clase Order. Lo hago asi para poder enviar un comentario custom
				$order->getPayment()->cancel();
	            $order->registerCancellation('Orden cancelada por cron', true);						
				$order->save();					
				if ($logEnabled) Mage::log('Order cancelled: ' . $incrementId , Zend_Log::INFO, self::LOG_FILE);
			}catch (Exception $e){
				if ($logEnabled) Mage::log('Error canceling order: ' . $incrementId . ' - ' . $e->getMessage(), Zend_Log::ERR, self::LOG_FILE);
			}			
		}else{
			if ($logEnabled) Mage::log('Order cannot be cancelled: ' . $incrementId, Zend_Log::ERR, self::LOG_FILE);			
		}

	}	

	protected function _aproveOrder($order, $logEnabled){	
		$incrementId = $order->getIncrementId();
		
		$mpexpress = Mage::getModel('mpexpress/Express');            

		$status = $mpexpress->getConfigData('order_status_approved');
		$state  = Mage::getResourceModel('sales/order_status_collection')->joinStates()->addFieldToFilter('main_table.status', $status)->getFirstItem()->getState();                         
    	    			
		try{		
			
			$order->setState($state, $status, 'Orden aprobada por cron.', true);
			$order->save();
			$order->sendNewOrderEmail();

			if ($logEnabled) Mage::log('Orden aprobada por cron: ' . $incrementId , Zend_Log::INFO, self::LOG_FILE);
		}catch (Exception $e){
			if ($logEnabled) Mage::log('Error aproving order: ' . $incrementId . ' - ' . $e->getMessage(), Zend_Log::ERR, self::LOG_FILE);
		}
	}	

	

	/*
	*	Funcion para recuperar ordenes no pagadas. 
	*   Envia mail al usuario con el link de MercadoPago
	*/
	protected function _recoverPendingPaymentOrders($website, $store){

		Mage::app()->setCurrentStore($store);		

		$helper = Mage::helper('brandlive_mpexpress/data');

		if(! $helper->getRecoverUnpaymentOrderEnabled($website) ) return false;

		$logEnabled = $helper->getRecoverCronLogEnabled($website);

		/* Format our dates */
		$date    = Mage::getModel('core/date');
		$currentTimestamp    = $date->timestamp(time());

		// Get to date time		
		$pendingTimeOrderTo   = $helper->getRecoverUnpaymentOrderEmailTime($website);
		$toTimestamp   = strtotime("-" . $pendingTimeOrderTo .  " minutes", $currentTimestamp);		
		$toDate   = $date->gmtDate('Y-m-d H:i:s', $toTimestamp);		

		// Si esta habilitado la expiracion de pago
		if ($helper->getExpiresEnabled($website)){

			if($logEnabled):
				Mage::log('La opción Expiración de Pago está habilitada', null, self::RECOVER_CRON_LOG_FILE);
			endif;

			// Verifico si está habilitada la cancelación de ordenes
			if($helper->getCancelationCronEnabled($website)){
				// Get from date time 
				$pendingTimeOrderFrom = $helper->getExpirationDuration($website) + $helper->getCancelationCronExpiration($website)  + $pendingTimeOrderTo;		
			}else{
				$pendingTimeOrderFrom = $helper->getExpirationDuration($website) + $pendingTimeOrderTo;
			}

			$fromTimestamp = strtotime("-" . $pendingTimeOrderFrom .  " minutes", $toTimestamp);
			$fromDate = $date->gmtDate('Y-m-d H:i:s', $fromTimestamp);
		}else{

			if($logEnabled):
				Mage::log('La Expiración de Pago no está habilitada', null, self::RECOVER_CRON_LOG_FILE);
			endif;

			$pendingTimeOrderFrom = $helper->getRecoverUnpaymentOrderLimitFromTime($website);			
			$fromTimestamp = strtotime("-" . $pendingTimeOrderFrom, $toTimestamp);
			$fromDate = $date->gmtDate('Y-m-d H:i:s', $fromTimestamp);
		}		

		$currentDate = $date->gmtDate('Y-m-d H:i:s', $currentTimestamp);

		if($logEnabled):
			Mage::log('getExpirationDuration: '.$helper->getExpirationDuration($website), null, self::RECOVER_CRON_LOG_FILE);
			Mage::log('getCancelationCronExpiration: '.$helper->getCancelationCronExpiration($website), null, self::RECOVER_CRON_LOG_FILE);
			Mage::log('currentDate: '.$currentDate, null, self::RECOVER_CRON_LOG_FILE);
			Mage::log('fromDate_date: '.$fromDate, null, self::RECOVER_CRON_LOG_FILE);
			Mage::log('toDate_date: '.$toDate, null, self::RECOVER_CRON_LOG_FILE);
		endif;
		
		$limit = $helper->getRecoverCronCollectionLimit($website);

		// Get Orders
		$orders = Mage::getModel('sales/order')->getCollection()				
				->join(
        				array('payment' => 'sales/order_payment'),
        					'main_table.entity_id = payment.parent_id', array('payment_method' => 'payment.method')
    			)	
				->addAttributeToFilter('payment.method', array('eq' => 'mpexpress'))
				->addAttributeToFilter('created_at', array('from' => $fromDate))
	    		->addAttributeToFilter('created_at', array('to' => $toDate))
	    		->addAttributeToFilter('store_id', array('eq' => $store))
	    		->addAttributeToFilter('status', array('in' => $helper->getRecoverUnpaymentOrderStatuses()))
	    		->addAttributeToFilter('mp_email_sended', array('eq' => self::MP_EMAIL_NOT_SENDED))
	    		->addAttributeToSort('increment_id', 'DESC')
	    		->setPageSize($limit);	     	

		if($logEnabled):
	    	Mage::log("Query ejecutada: ".(string) $orders->getSelect(), null, self::RECOVER_CRON_LOG_FILE);  
	    endif;

	    if ($orders->count() > 0){	 	    	
	    	$ipn = Mage::getModel('mpexpress/Checkout');    	    	
		   	foreach ($orders as $order) {	
		   		$incrementId = $order->getIncrementId();
		   		$externalId  = 'mpexpress-' . $incrementId;

		   		$payments = null;
		   		
		   		try{
		   			$payments = $ipn->SearchPayment($externalId);		   			
		   		}catch (Exception $e) {
		   			if ($logEnabled) Mage::log('Error geting payments for order: ' . $incrementId . ' - ' . $e->getMessage(), null, self::RECOVER_CRON_LOG_FILE);
			    	continue;
		    	}

		    	//Si MP no me devolvio nada no hago nada
		    	if ( $payments === null || $payments === false ){
					if ($logEnabled) Mage::log('Error geting payments for order: ' . $incrementId . ' - Nothing returned', null, self::RECOVER_CRON_LOG_FILE);		    		
		    		continue;	
		    	} 

		    	//Si P devolvio algo para no hay transaccion asociado, cancelo la orden
		    	if ( count($payments['results']) == 0 ){ 
		    		if($logEnabled):	   	
		   				Mage::log('Se enviará un mail a: '. $order->getCustomerEmail() .' con el detalle de la orden: #'.$order->getIncrementId().' y el siguiente link: '.$order->getMpPaymentLink(), null, self::RECOVER_CRON_LOG_FILE);
		   			endif;
		   			$this->_sendMailToUser($order,$website);
		    		continue;
		    	}

		    	//Ordeno los pagos para obtener el ultimo
		    	$results = $payments['results'];		    	

		    	if ( count($results) > 1 ){
			    	usort($results, function($a, $b) {
					   return strtotime($b['collection']['last_modified']) - strtotime($a['collection']['last_modified']);
					});
				}

				$last_payment = reset($results);

				//si el ultmo pago de la orden es cancelado, cancelo la orden.
				//si es aprobado, la apruebo
				if ($last_payment['collection']['status'] == 'approved' && $last_payment['collection']['status_detail'] == 'accredited') {
					if($logEnabled):	   	
		   				Mage::log('Orden ya se encuentra aprobada: '. $incrementId .' se pasa a la aprobacion', null, self::RECOVER_CRON_LOG_FILE);
		   			endif;
					$this->_aproveOrder($order, $logEnabled);
				}			
		   	}
		   	
		}else{
		   	if($logEnabled):
		   		Mage::log('No se encontraron ordenes no pagadas', null, self::RECOVER_CRON_LOG_FILE);
		   	endif;
		}

	}

	protected function _sendMailToUser($order, $website){

		$helper = Mage::helper('brandlive_mpexpress/data');
		$logEnabled = $helper->getRecoverCronLogEnabled($website);

		if(!$order) return false;

   		$incrementId 	 = $order->getIncrementId();
		$mp_payment_link = $order->getMpPaymentLink();	

		if($helper->getDefaultTemplateEmail($website)){

			if($logEnabled):
            	Mage::log('Se va a enviar con el template por defecto', null, self::RECOVER_CRON_LOG_FILE);
        	endif;

			$emailTemplate  = Mage::getModel('core/email_template')->loadDefault('brandlive_mpexpress_unpayment_orders');			
			$emailTemplate->setTemplateSubject($helper->getMailSubject($website));   

		}else{

			$emailTemplate = Mage::getModel('core/email_template')->loadByCode('Ordenes No Pagadas');

			if(!$emailTemplate->getTemplateId()){
	    		if($logEnabled):
	            	Mage::log('No existe el template Ordenes No Pagadas por lo cual se va a enviar con el template por defecto', null, self::RECOVER_CRON_LOG_FILE);
	        	endif;
				$emailTemplate  = Mage::getModel('core/email_template')->loadDefault('brandlive_mpexpress_unpayment_orders');			
				$emailTemplate->setTemplateSubject($helper->getMailSubject($website));      	
			}else{

				if($logEnabled):
	        		Mage::log('Se va a enviar con el template actualizado', null, self::RECOVER_CRON_LOG_FILE);
	        	endif;

				$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);            
	            $emailTemplate->setTemplateSubject($emailTemplate->getTemplateSubject());  
	            $emailTemplate->setBody($processedTemplate);				
			}
        }				

		$emailTemplateVariables = array();
		
		//Create an array of variables to assign to template
		$emailTemplateVariables['mp_payment_link'] = $mp_payment_link;        

        $emailTemplate->setSenderName($helper->getSenderName($website));
        $emailTemplate->setSenderEmail($helper->getSenderEmail($website));
        
		if ($order->getCustomerEmail()) {
				try {
					$emailTemplate->send($order->getCustomerEmail(), null, $emailTemplateVariables);
					
					if($logEnabled):
						Mage::log('Se envió el mail correctamente', null, self::RECOVER_CRON_LOG_FILE);
					endif;

					// Agrego comentario al historial
	                $comment = 'Se envió mail al usuario para que complete el pago de la orden';
	                // Actualizo el campo indicando que ya se envio la notificacion para la orden 
	                $order->setData('mp_email_sended', self::MP_EMAIL_SENDED);            
	                
	                $history = $order->addStatusHistoryComment($comment);
	                $history->setIsCustomerNotified(true);
	                
	                $order->save();
				}
				catch (Exception $e) {
					//catch an unsuccessful email
					if($logEnabled):
						Mage::log('Ocurrió un error al enviar el mail', null, self::RECOVER_CRON_LOG_FILE);
					endif;
				}        
		}else{
			if($logEnabled):
				Mage::log('El usuario no tiene un email configurado', null, self::RECOVER_CRON_LOG_FILE);
			endif;
		}
	}

}