<?php

class Brandlive_Mpexpress_Model_System_Config_Source_Order_Limit
{
    public function toOptionArray()
    {       
        return array(
			array('value' => 1, 'label' => '1 día'),
			array('value' => 2, 'label' => '3 días'),
			array('value' => 3, 'label' => '1 semana'),            
			array('value' => 4, 'label' => '3 semanas'),
			array('value' => 5, 'label' => '1 mes'),
			array('value' => 6, 'label' => '1 año')
	    );   
    }
}
