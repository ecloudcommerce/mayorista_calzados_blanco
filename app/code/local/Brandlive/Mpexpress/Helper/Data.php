<?php

class Brandlive_Mpexpress_Helper_Data extends Mage_Core_Helper_Abstract{
	
	const COLLECTION_MAX_LIMIT = 50;
	const MIN_EXPIRATION_DURATION  = 5;
	const MIN_ORDER_EXPIRATION_DURATION  = 5;


	public function getCancelationCronEnabled($website = null){
		return (bool)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/cancelation_cron_enabled');
	}

	public function getExpiresEnabled($website = null){
		return (bool)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/expires');
	}

	public function getExpirationDuration($website = null){
		$expiration =  (int)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/expiration_duration');
		if ($expiration < self::MIN_EXPIRATION_DURATION) $expiration = self::MIN_EXPIRATION_DURATION;
		return $expiration;
	}

	public function getCancelationCronExpiration($website = null){
		$expiration =  (int)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/cancelation_cron_expiration');
		if ($expiration < self::MIN_ORDER_EXPIRATION_DURATION) $expiration = self::MIN_ORDER_EXPIRATION_DURATION;
		return $expiration;
	}

	public function getCancelationCronCollectionLimit($website = null){
		$limit = (int)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/cancelation_cron_collection_limit');
		if ($limit > self::COLLECTION_MAX_LIMIT) $limit = self::COLLECTION_MAX_LIMIT;
		return $limit;
	}

	public function getCancelationCronOrderStatuses($website = null){
		$statuses = Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/cancelation_cron_order_statuses');
		return explode(',',$statuses);
	}

	public function getCancelationCronLogEnabled($website = null){
		return (bool)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/cancelation_cron_log');
	}

	public function getRecoverUnpaymentOrderEnabled($website = null){
		return (bool)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/recover_unpayment_order_enabled');
	}

	public function getDefaultTemplateEmail($website = null){
		return (bool)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/recover_unpayment_order_default_template_email');
	}	

	public function getRecoverUnpaymentOrderStatuses($website = null){
		$statuses = Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/recover_unpayment_order_statuses');
		return explode(',', $statuses);
	}

	public function getRecoverUnpaymentOrderEmailTime($website = null){
		return (int)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/recover_unpayment_order_email_time');
	}

	public function getRecoverCronCollectionLimit($website = null){
		$limit = (int)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/recover_cron_collection_limit');
		if ($limit > self::COLLECTION_MAX_LIMIT) $limit = self::COLLECTION_MAX_LIMIT;
		return $limit;
	}

	public function getRecoverUnpaymentOrderLimitFromTime($website = null){
		
		$limit = array(
			'1' => '1 days',   // 1 dia
			'2' => '3 days',   // 3 dias
			'3' => '1 weeks',  // 1 semana
			'4' => '3 weeks',  // 3 semanas
			'5' => '1 months', // 1 mes
			'6' => '1 years'   // 1 año 
		);

		$selected_value = Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/recover_unpayment_order_limit_from_time');

		return $limit[$selected_value];

	}

	public function getMailSubject($website = null){
		return Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/recover_unpayment_order_email_subject');
	}

	public function getSenderName($website = null){
		return Mage::app()->getWebsite($website)->getConfig('trans_email/ident_general/name');
	}

	public function getSenderEmail($website = null){
		return Mage::app()->getWebsite($website)->getConfig('trans_email/ident_general/email');
	}

	public function getRecoverCronLogEnabled($website = null){
		return (bool)Mage::app()->getWebsite($website)->getConfig('payment/mpexpress/recover_cron_log');
	}


}