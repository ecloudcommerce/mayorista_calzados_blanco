<?php
/**
 * @version   1.0 28-06-16
 * @author    Ecloud Solutions
 * @copyright Copyright (C) 2016 Ecloud Solutions
 */
?>
<?php
$installer = $this;
$setup = Mage::getResourceModel('catalog/eav_mysql4_setup','core_setup');

$installer->startSetup();

$setup->removeAttribute(Mage_Catalog_Model_Product::ENTITY,'guia_talles');
$setup->removeAttribute(Mage_Catalog_Model_Product::ENTITY,'como_cuidarlo');
$setup->removeAttribute(Mage_Catalog_Model_Product::ENTITY,'envio_gratis');
$setup->removeAttribute(Mage_Catalog_Model_Product::ENTITY,'composicion');

$setup->addAttribute(Mage_Catalog_Model_Product::ENTITY,'guia_talles',
	array(
		'group'=>'General',
		'type'=>'text',
		'label'=>'Guia de talles',
		'input'=>'text',
		'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'=>true,
		'required'=>false,
		'used_in_product_listing'=> 1,
		'user_defined'=>false
	));

$setup->addAttribute(Mage_Catalog_Model_Product::ENTITY,'como_cuidarlo',
	array(
		'group'=>'General',
		'type'=>'text',
		'label'=>'Cómo Cuidarlo',
		'input'=>'textarea',
		'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'=>true,
		'required'=>false,
		'used_in_product_listing'=> 1,
		'user_defined'=>false
	));
$setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'envio_gratis',
	array(
     	'group'    => 'General',
     	'label'    => 'Envio gratis',
     	'type' => 'int',
     	'input'    => 'boolean',
     	'visible'  => true,
     	'required' => false,
     	'used_in_product_listing' => 1,
     	'position' => 21,
     	'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'note'     => "Envío gratuito?"
	));

$setup->addAttribute(Mage_Catalog_Model_Product::ENTITY,'composicion',
	array(
		'group'=>'General',
		'type'=>'text',
		'label'=>'Composición',
		'input'=>'text',
		'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible'=>true,
		'used_in_product_listing' => 1,
		'required'=>false,
		'user_defined'=>false
	));

$installer->endSetup();


?>