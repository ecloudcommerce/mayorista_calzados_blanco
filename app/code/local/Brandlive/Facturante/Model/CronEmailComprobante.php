<?php

class Brandlive_Facturante_Model_CronEmailComprobante{

	const FACTURANTE_MAIL_LOG_FILE 		= 'facturante_email.log';
	const FACTURANTE_EMAIL_SENDED 		= 1;
	const FACTURANTE_EMAIL_NOT_SENDED 	= 0;

	public function enviarComprobanteMagento()
    {
        foreach (Mage::app()->getStores() as $store) {      
            $this->_enviarComprobanteMagento($store->getWebsiteId(), $store->getId());
        }   
    }

    protected function _enviarComprobanteMagento($website, $store)
    {
				Mage::app()->setCurrentStore($store);			
			
        $facturanteLog = Mage::helper('brandlive_facturante/log');
        $facturanteLog->log("Entra a ".__METHOD__, Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

        $helper = Mage::helper('brandlive_facturante/data');
        $helperFacturante = Mage::helper('facturante_invoicing');

        $enviarComprobanteFacturante = $helper->getEnviarComprobanteFacturante($website);
        $enviarComprobanteMageno = $helper->getEnviarComprobanteMagento($website);

        /* Si está setteado enviar comprobante x Facturante no hago nada */
        if($enviarComprobanteFacturante){
        	$facturanteLog->log('Envio de comprobante por Facturante está habiilitado, no se enviarán por Magento', Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);
        	return;	
        } 

        /* Si está setteado enviar comprobante x Magento y está deshabilitado enviar comprobante por Facturante */
        if($enviarComprobanteMageno && !$enviarComprobanteFacturante){

        	//Obtengo todas las ordenes que estén en estado Procesado y que no se haya enviado su comprobante
        	$orders = Mage::getModel('sales/order')->getCollection()
                    ->addAttributeToFilter('facturante_invoice_status', array('eq' => 'Procesado'))
                    ->addAttributeToFilter('afip_last_invoice_type', array('eq' => 'factura'))
										->addAttributeToFilter('store_id', array('eq' => $store))
                    ->addAttributeToFilter('facturante_email_sended', array('eq' => self::FACTURANTE_EMAIL_NOT_SENDED));

            $facturanteLog->log("Ordenes encontradas: ".count($orders), Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

			foreach ($orders as $order){

				$orderId = $order->getId();
				$facturanteLog->log("oderId: ".$orderId, Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);
				
				/** Con la info recogida, filtro la base y tomo el respectivo enlace al archivo si lo hubiere */
	            $collection = Mage::getModel('facturante_invoicing/order_invoicingstatus')->getCollection();
	            $collection->addFieldToFilter('order_id', array('attribute' => 'order_id', 'eq' => $orderId));
	            $collection->addFieldToFilter('status', array('attribute' => 'order_id', 'eq' => 'Procesado'));
	            
	            $facturanteLog->log("collection: ".count($collection), Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

	            /** Este foreach solo debería iterar una vez, puesto que no debería existir dos comprobantes con el mismo id y con las condiciones anteriormente establecidas. */
	            foreach ($collection as $comprobante)
	            {
	                $facturantePdfLink = $comprobante->getLink();
	                $facturanteLog->log("facturantePdfLink: ".$facturantePdfLink, Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

	                if($facturantePdfLink){
	                	$this->_sendMailToUser($order, $website, $facturantePdfLink);
	                }

	            }

			}

        }

    }

    protected function _sendMailToUser($order, $website, $facturantePdfLink){

		$facturanteLog = Mage::helper('brandlive_facturante/log');
        $facturanteLog->log("Entra a ".__METHOD__, Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

        $helper = Mage::helper('brandlive_facturante/data');
        $helperFacturante = Mage::helper('facturante_invoicing');

		if(!$order) return false;

		if($helper->getDefaultTemplateEmail($website)){

			$facturanteLog->log('Se va a enviar con el template por defecto', Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

			$emailTemplate = Mage::getModel('core/email_template')->loadDefault('facturante_email');			

			$emailTemplate->setTemplateSubject($helper->getEmailSubject($website));   

		}else{

			$emailTemplate = Mage::getModel('core/email_template')->loadByCode('Comprobante Facturante');

			if(!$emailTemplate->getTemplateId()){
	            	
				$facturanteLog->log('No existe el template Ordenes No Pagadas por lo cual se va a enviar con el template por defecto', Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

				$emailTemplate  = Mage::getModel('core/email_template')->loadDefault('facturante_email');			
				$emailTemplate->setTemplateSubject($helper->getEmailSubject($website));  

			}else{

	        	$facturanteLog->log('Se va a enviar con el template actualizado', Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

				$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);            
	            $emailTemplate->setTemplateSubject($emailTemplate->getTemplateSubject());  
	            $emailTemplate->setBody($processedTemplate);				
			}
        }				

		$emailTemplateVariables = array();
		
		//Create an array of variables to assign to template
		$emailTemplateVariables['facturantePdfLink'] = $facturantePdfLink;     

        $emailTemplate->setSenderName($helper->getSenderName($website));
        $emailTemplate->setSenderEmail($helper->getSenderEmail($website));

        //$facturanteLog->log('emailTemplate: '.print_r($emailTemplate,true), Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

		if ($order->getCustomerEmail()) {

				try {
					$emailTemplate->send($order->getCustomerEmail(), null, $emailTemplateVariables);
					
					$facturanteLog->log('Se envió el mail correctamente', Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);

					// Agrego comentario al historial
	                $comment = 'Se envió el comprobante de Facturante al usuario';
	                // Actualizo el campo indicando que ya se envio la notificacion para la orden 
	                $order->setData('facturante_email_sended', self::FACTURANTE_EMAIL_SENDED);            
	                
	                $history = $order->addStatusHistoryComment($comment);
	                $history->setIsCustomerNotified(true);
	                
	                $order->save();
				}
				catch (Exception $e) {
					//catch an unsuccessful email
					$facturanteLog->log('Ocurrió un error al enviar el mail', Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);
				}        
		}else{
			$facturanteLog->log('El usuario no tiene un email configurado', Zend_Log::INFO, self::FACTURANTE_MAIL_LOG_FILE);
		}
	}
	
}