<?php
class Brandlive_Facturante_Helper_Log{
	
	public function log($message, $level = null, $file = 'facturante.log'){
		$magento_version = Mage::getVersion();
		$php_version 	 = phpversion();
		Mage::log("[Mag:$magento_version]: ".$message, $level, $file);
	}

}