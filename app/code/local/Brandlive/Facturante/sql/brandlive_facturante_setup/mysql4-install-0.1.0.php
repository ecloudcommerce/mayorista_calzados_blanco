<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$this->addAttribute('order', 'facturante_email_sended', array(
	'type'          	=> Varien_Db_Ddl_Table::TYPE_SMALLINT,
	'label'         	=> 'Flag para envio de factura por email',
	'visible'       	=> false,
	'required'      	=> false,
	'visible_on_front' 	=> false,
	'user_defined'  	=> false,
	'default'			=> 0
));

$installer->endSetup();