<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Block_Adminhtml_Cron_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	

    public function __construct()
    {
        parent::__construct();
        $this->setId('wcooCronGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('date');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel("cron/schedule")->getCollection();
        $collection->getSelect()->order('schedule_id desc');
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('schedule_id', array(
            'header'=> Mage::helper('cron')->__('Schedule Id'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'schedule_id',
        ));

        $this->addColumn('job_code', array(
            'header'=> Mage::helper('cron')->__('Job Code'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'job_code',
        ));

        $this->addColumn('status', array(
            'header'=> Mage::helper('cron')->__('Status'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'status',
        ));

        $this->addColumn('messages', array(
            'header'=> Mage::helper('cron')->__('Messages'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'messages',
        ));

        $this->addColumn('scheduled_at', array(
            'header'=> Mage::helper('cron')->__('Scheduled at'),
            'width' => '80px',
            'type'  => 'datetime',
            'index' => 'scheduled_at',
        ));


        $this->addColumn('created_at', array(
            'header'=> Mage::helper('cron')->__('Created at'),
            'width' => '80px',
            'type'  => 'datetime',
            'index' => 'created_at',
        ));

        $this->addColumn('executed_at', array(
            'header'=> Mage::helper('cron')->__('Executed at'),
            'width' => '80px',
            'type'  => 'datetime',
            'index' => 'executed_at',
        ));

        $this->addColumn('finished_at', array(
            'header'=> Mage::helper('cron')->__('Finished at'),
            'width' => '80px',
            'type'  => 'datetime',
            'index' => 'finished_at',
        ));


        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

}
