<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Block_Adminhtml_Log_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	

    public function __construct()
    {
        parent::__construct();
        $this->setId('wcooLogGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('date');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getSingleton('wcooall/system_log_fs_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('time', array(
            'header'    => Mage::helper('wcooall')->__('Last Modification Time'),
            'index'     => 'time',
            'type'      => 'datetime',
            'width'     => 200
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('wcooall')->__('Name'),
            'index'     => 'name',
            'filter'    => false,
            'sortable'  => true,
            'width'     => 350
        ));

        $this->addColumn('size', array(
            'header'    => Mage::helper('wcooall')->__('Size, MegaBytes'),
            'index'     => 'size',
            'type'      => 'number',
            'sortable'  => true,
            'filter'    => false
        ));

       

        $this->addColumn('download', array(
            'header'    => Mage::helper('wcooall')->__('Download'),
            'format'    => '<a href="' . $this->getUrl('*/*/download', array('name' => '$name'))
                . '">$name</a> &nbsp;',
            'index'     => 'type',
            'sortable'  => false,
            'filter'    => false
        ));
        
         $this->addColumn('action', array(
                    'header'   => Mage::helper('wcooall')->__('Action'),
                    'type'     => 'action',
                    'width'    => '80px',
                    'filter'   => false,
                    'sortable' => false,
                    'actions'  => array(array(
                        'url'     => '#',
                        'caption' => Mage::helper('wcooall')->__('Delete file'),
                        'onclick' => 'setLocation(\''.$this->getUrl('*/*/delete', array('name' => '$name')).'\')'
                    )),
                    'index'    => 'type',
                    'sortable' => false
            ));

       

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

}
