<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Helper_Category extends Mage_Core_Helper_Abstract {

    protected $_categoryIdsByName = array();
    protected $_categoryIdByName = array();
    protected $_categoryNameById = array();
    protected $_categoriesById = array();

    protected function _loadAllCategories() {
        if (empty($this->_categoriesById)) {
            $categoryCollection = Mage::getModel('catalog/category')->getCollection();
            $categoryCollection->addAttributeToSelect('name');
            foreach ($categoryCollection as $category) {
                $this->_categoriesById[$category->getId()] = $category;
            }
            foreach ($categoryCollection as $category) {
                $name = array();
                $path = explode('/', $category->getPath());
                foreach ($path as $catId) {
                    if (isset($this->_categoriesById[$catId]) && $catId != 1 && $catId != Mage::app()->getStore(Mage::app()->getDefaultStoreView()->getId())->getRootCategoryId()) {
                        $name[] = $this->_categoriesById[$catId]->getName();
                    }
                }
                $name = implode('%', $name);
                $this->_categoryIdsByName[$name] = $path;
                $this->_categoryIdByName[$name] = $category->getId();
            }
            $this->_categoryNameById = array_flip($this->_categoryIdByName);
        }
    }
    
    
    
    public function getCategoryNameById($categoryId, $separator = '%', $limit=false) {
        $this->_loadAllCategories();
        if (!array_key_exists($categoryId, $this->_categoryNameById)) {
            return false;
        }
        $categoryName = $this->_categoryNameById[$categoryId];
        if($limit) {
            $categoryName = explode('%', $categoryName);
            $categoryName = array_slice($categoryName, 0, $limit, true);
            $categoryName = implode('%', $categoryName);
        }
        if($separator == '%') {
            return $categoryName;
        }
        if(!$separator) {
            return explode('%', $categoryName);
        }
        return str_replace('%', $separator, $categoryName);
    }


    public function getCategoryIdsByName($name, $importIfNotExists = false, $data = array()) {
        $this->_loadAllCategories();
        if (!array_key_exists($name, $this->_categoryIdsByName)) {
            if ($importIfNotExists) {
                $this->importCategory($name, $data);
                return $this->getCategoryIdsByName($name, false);
            } else {
                return false;
            }
        }
        return $this->_categoryIdsByName[$name];
    }
    
    public function getCategoryIdsById($categoryId) {
        $this->_loadAllCategories();
        $categoryName = $this->getCategoryNameById($categoryId);
        return $this->getCategoryIdsByName($categoryName);
    }
    

    public function getCategoryIdByName($name) {
        $this->_loadAllCategories();
        if (!array_key_exists($name, $this->_categoryIdByName)) {
            return false;
        }
        return $this->_categoryIdByName[$name];
    }

    public function importCategory($name, $data) {
        $this->_loadAllCategories();
        $categoryApi = Mage::getModel('catalog/category_api');
        $nameParts = explode('%', $name);
        $curName = '';
        $parentId = Mage::app()->getStore(Mage::app()->getDefaultStoreView()->getId())->getRootCategoryId();
        for ($i = 0; $i < count($nameParts); $i++) {
            if ($i != 0)
                $curName .= '%';
            $curName .= $nameParts[$i];
            $result = $this->getCategoryIdsByName($curName, false);
            if (!$result) {
                $categoryData = array(
                    'name' => $nameParts[$i],
                    'is_active' => 1,
                    'include_in_menu' => 1,
                    'is_anchor' => 1,
                    'available_sort_by' => false,
                    'default_sort_by' => false,
                    'path' => $this->_categoriesById[$parentId]->getPath()
                );
                foreach ($data as $k => $v)
                    $categoryData[$k] = $v;
                try {
                    $categoryId = $categoryApi->create($parentId, $categoryData);
                    $this->_categoriesById[$categoryId] = Mage::getModel('catalog/category')->load($categoryId);
                    $this->_categoryIdsByName[$curName] = explode('/', $this->_categoriesById[$categoryId]->getPath());
                    $parentId = $categoryId;
                } catch (Mage_Api_Exception $mae) {
                    throw $mae;
                }
            } else {
                $parentId = array_pop($result);
            }
        }
    }

}
