<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Helper_Attribute extends Mage_Core_Helper_Abstract {

    protected $_attributes = array();
    protected $_attributeSets = array();

    public function getAttribute($attributeCode, $entityType = 'catalog_product') {
        if (!isset($this->_attributes[$entityType])) {
            $this->_attributes[$entityType] = array();
        }
        /*if (is_numeric($attributeCode)) {
            $attributeId = $attributeCode;
        } else {
            $attributeId = Mage::getModel('eav/entity_attribute')->getIdByCode($entityType, $attributeCode);
        }*/
        if (!isset($this->_productAttributes[$entityType][$attributeCode])) {
            $this->_attributes[$entityType][$attributeCode] = Mage::getSingleton('eav/config')->getAttribute($entityType, $attributeCode);//Mage::getModel('eav/entity_attribute')->load($attributeId);
        }
        return $this->_attributes[$entityType][$attributeCode];
    }
    
    public function getAttributeDataForGrid($attributeCode, $attributeDataDefaults = array('align'=>'left')) {
        $attribute = $this->getAttribute($attributeCode);
        $attributeData = array(
            'header' => Mage::helper('wcooall')->__($attribute->getStoreLabel()?$attribute->getStoreLabel():$attributeCode),
            'index' => $attributeCode,
        );
        $attributeData = array_merge($attributeData, $attributeDataDefaults);
        if ($attribute->usesSource()) {
            $attributeData['type'] = 'options';
            $rawValues = $attribute->getSource()->getAllOptions(false);
            $values = array();
            foreach ($rawValues as $rawValue) {
                $values[$rawValue['value']] = $rawValue['label'];
            }
            $attributeData['options'] = $values;
        } else if($attribute->getFrontendInput() == 'price') {
            $attributeData['type'] = 'number';
        } else if($attributeCode == 'is_in_stock' || $attributeCode == 'manage_stock' || $attributeCode == 'use_config_backorders') {
            $attributeData['type'] = 'options';
            $attributeData['options'] = array(
                '1' => Mage::helper('catalog')->__('Yes'),
                '0' => Mage::helper('catalog')->__('No'),
            );
        } else if($attributeCode == 'backorders') {
            $options = array();
            foreach(Mage::getModel('cataloginventory/source_backorders')->toOptionArray() as $option) {
                $options[$option['value']] = $option['label'];
            }
            $attributeData['type'] = 'options';
            $attributeData['options'] = $options;
        }
        return $attributeData;
    }

    public function getAttributeOptionNameById($attributeCode, $optionId, $entityType = 'catalog_product') {
        $attribute = $this->getAttribute($attributeCode, $entityType);
        if (!$attribute || !$attribute->usesSource()) {
            return false;
        }
        if (!$attribute->getSourceModel()) {
            $attribute->setSourceModel('eav/entity_attribute_source_table');
        }
        $options = Mage::getModel($attribute->getSourceModel())->setAttribute($attribute)->getAllOptions(false);

        foreach ($options as $option) {
            if ($option['value'] == $optionId) {
                return $option['label'];
            }
        }
        return false;
    }

    public function getAttributeOptionIdByName($attributeCode, $optionName, $importIfNotExists=false, $entityType = 'catalog_product') {
        if (!trim($optionName)) {
            return false;
        }
        $attribute = $this->getAttribute($attributeCode, $entityType);
        $options = Mage::getModel('eav/entity_attribute_source_table')->setAttribute($attribute)->getAllOptions(false);
        foreach ($options as $option) {
            if ($option['label'] == $optionName) {
                return $option['value'];
            }
        }
        if ($importIfNotExists) {
            $this->importAttributeOption($attributeCode, $optionName);
            return $this->getAttributeOptionIdByName($attributeCode, $optionName, false);
        }
        return false;
    }
    
    function importAttributeOption($attributeCode, $optionName, $entityType = 'catalog_product') {
        $attribute = $this->getAttribute($attributeCode, $entityType);
        $option = array();
        $option['attribute_id'] = $attribute->getId();
        $option['value']['any_option_name'][0] = $optionName;

        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
        return;
    }

    public function getAttributeSetNameById($attributeSetId, $entityType = 'catalog_product') {
        if (!isset($this->_attributeSets[$attributeSetId])) {
            $this->_attributeSets[$attributeSetId] = Mage::getModel("eav/entity_attribute_set")->load($attributeSetId);
        }
        return $this->_attributeSets[$attributeSetId]->getAttributeSetName();
    }

}
