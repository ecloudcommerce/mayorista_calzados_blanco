<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Adminhtml_LogController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_All');
    }

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    
    public function downloadAction() {
        $name = $this->getRequest()->getParam('name');
        if(!$name) {
            $this->_redirect('*/*/index');
            return;
        }
        $fileName = Mage::getBaseDir('var') . DS . 'log' . DS . $name;
        if(!file_exists($fileName)) {
            $this->_redirect('*/*/index');
            return;
        }
        $this->_prepareDownloadResponse($name, $fileName);
    }

    
    public function deleteAction() {
        $name = $this->getRequest()->getParam('name');
        if(!$name) {
            $this->_redirect('*/*/index');
            return;
        }
        $fileName = Mage::getBaseDir('var') . DS . 'log' . DS . $name;
        if(!file_exists($fileName)) {
            $this->_redirect('*/*/index');
            return;
        }
        unlink($fileName);
        $this->_redirect('*/*/index');
    }
    

}
