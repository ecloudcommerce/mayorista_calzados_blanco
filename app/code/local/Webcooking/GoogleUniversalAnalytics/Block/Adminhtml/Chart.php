<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Block_Adminhtml_Chart extends Mage_Adminhtml_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('webcooking/googleuniversalanalytics/chart.phtml');
        $this->setQuery('visitors');
    }
    
    public function setPeriod($period) {
        $oneDay = 24*60*60;
        switch($period) {
            case 'today':
                $this->setFrom(date('Y-m-d'));
                $this->setTo(date('Y-m-d'));
                break;
            case 'yesterday':
                $this->setFrom(date('Y-m-d', time()-$oneDay));
                $this->setTo(date('Y-m-d', time()-$oneDay));
                break;
            case 'thisweek':
                $weekDay = date("w");
                $firstDayOfWeek = Mage::getStoreConfig('general/locale/firstday');
                if($firstDayOfWeek != 0 && $firstDayOfWeek < $weekDay) {
                    $weekDay += 7;
                }
                $diff = $firstDayOfWeek - $weekDay;
                $this->setFrom(date('Y-m-d', time() + $oneDay*$diff));
                $this->setTo(date('Y-m-d', time()));
                break;
            case 'thismonth':
                $this->setFrom(date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y'))));
                $this->setTo(date('Y-m-d', time()));
                break;
            case 'thisyear':
                $this->setFrom(date('Y-m-d', mktime(0, 0, 0, 1, 1, date('Y'))));
                $this->setTo(date('Y-m-d', time()));
                break;
            case (preg_match("%^last([0-9]+)days$%i", $period, $pregResult) ? $period : !$period):
                $days = $pregResult[1];
                $this->setFrom(date('Y-m-d', time()-$oneDay*$days));
                $this->setTo(date('Y-m-d'));
                break;
        }
    }
    
    public function getJsonChart() {
        return Mage::helper('googleuniversalanalytics/dashboard')->getChartData($this->getFrom(), $this->getTo(), $this->getQuery());
    }
    
    public function getHAxisTitle() {
        if($this->getData('h_axis_title')) {
            return $this->getData('h_axis_title');
        }
        if($this->getFrom() == $this->getTo()) {
            return $this->__('Hours');
        }
        return $this->__('Dates');
    }
    
    public function getVAxisTitle() {
        if($this->getData('v_axis_title')) {
            return $this->getData('v_axis_title');
        }
        return ucwords($this->getQuery());
    }
}
