<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Helper_Remarketing extends Mage_Core_Helper_Abstract {

    const PAGE_TYPE_PRODUCT = 'product';
    const PAGE_TYPE_CATEGORY = 'category';
    const PAGE_TYPE_HOME = 'home';
    const PAGE_TYPE_CART = 'cart';
    const PAGE_TYPE_OTHER = 'other';

    public function getPageType() {
        $request = Mage::app()->getRequest();
        if ($request->getModuleName() == 'catalog' && $request->getControllerName() == 'product' && $request->getActionName() == 'view') {
            $_product = Mage::registry('current_product');
            if ($_product->getId()) {
                return self::PAGE_TYPE_PRODUCT;
            } else {
                return self::PAGE_TYPE_OTHER;
            }
        }

        if ($request->getModuleName() == 'catalog' && $request->getControllerName() == 'category' && $request->getActionName() == 'view') {
            $_category = Mage::registry('current_category');
            if ($_category->getId()) {
                return self::PAGE_TYPE_CATEGORY;
            } else {
                return self::PAGE_TYPE_OTHER;
            }
        }

        if ($request->getModuleName() == 'checkout' && $request->getControllerName() == 'cart' && $request->getActionName() == 'index') {
            return self::PAGE_TYPE_CART;
        }

        if ($request->getModuleName() == 'cms' && $request->getControllerName() == 'index' && $request->getActionName() == 'index') {
            return self::PAGE_TYPE_HOME;
        }

        return self::PAGE_TYPE_OTHER;
    }

    
    
    public function getProdId() {
        $useProductId = Mage::getStoreConfigFlag('googleuniversalanalytics/remarketing/use_product_id');
        switch($this->getPageType()) {
            case self::PAGE_TYPE_PRODUCT:
                $_product = Mage::registry('current_product');
                return "'" . ($useProductId?$_product->getId():$_product->getSku()) . "'";
            case self::PAGE_TYPE_CART:
                $_cart = Mage::getSingleton('checkout/cart');
                $prodIds = array();
                if($_cart->getQuote()) {
                    foreach($_cart->getQuote()->getAllVisibleItems() as $_cartItem) {
                        $prodIds[] = '"' . ($useProductId?$_cartItem->getProductId():$_cartItem->getSku()) . '"';
                    }
                }
                return '[' . implode(',', $prodIds) . ']';
        }
        return "''";
    }
    
    
    public function getTotalValue() {
        switch($this->getPageType()) {
            case self::PAGE_TYPE_PRODUCT:
                $_product = Mage::registry('current_product');
                return round($_product->getFinalPrice()?$_product->getFinalPrice():$_product->getPrice(), 2);
            case self::PAGE_TYPE_CART:
                $_cart = Mage::getSingleton('checkout/cart');
                return round($_cart->getQuote()->getGrandTotal(), 2);
        }
        return '';
    }
}
