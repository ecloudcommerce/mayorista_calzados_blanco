<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Helper_Dashboard extends Mage_Core_Helper_Abstract {

    protected $_client = null;
    protected $_service = null;

    public function useOwnApiProject() {
        return
                Mage::getStoreConfig('googleuniversalanalytics/dashboard/client_id') &&
                Mage::getStoreConfig('googleuniversalanalytics/dashboard/client_secret') &&
                Mage::getStoreConfig('googleuniversalanalytics/dashboard/developer_key');
    }

    public function getAccessCode() {
        return Mage::getStoreConfig('googleuniversalanalytics/dashboard/access_code');
    }

    public function getClient() {
        if (is_null($this->_client)) {
            $this->_client = new Google_Client ();
            $this->_client->setScopes('https://www.googleapis.com/auth/analytics.readonly');
            $this->_client->setAccessType('offline');
            $this->_client->setApplicationName('Webcooking Google Universal Analytics');
            $this->_client->setRedirectUri('urn:ietf:wg:oauth:2.0:oob');

            if ($this->useOwnApiProject()) {
                $this->_client->setClientId(Mage::getStoreConfig('googleuniversalanalytics/dashboard/client_id'));
                $this->_client->setClientSecret(Mage::getStoreConfig('googleuniversalanalytics/dashboard/client_secret'));
                $this->_client->setDeveloperKey(Mage::getStoreConfig('googleuniversalanalytics/dashboard/developer_key'));
            } else {
                $this->_client->setClientId('201334511358-3t5reuj3eos0f3f1lbu21hoqcqbhpf3f.apps.googleusercontent.com');
                $this->_client->setClientSecret('Qi5etCqv2q6uqsJWshETSs6E');
                $this->_client->setDeveloperKey('AIzaSyBlbcJU6TM_OoZEPaOWXXsz5YHeB5ES67U');
            }

            try {
                if (Mage::getStoreConfig('googleuniversalanalytics/dashboard/access_token')/*Mage::getSingleton('adminhtml/session')->getGoogleOAuthAccessToken()*/) {
                    $this->_client->setAccessToken(Mage::getStoreConfig('googleuniversalanalytics/dashboard/access_token'));
                } else if($this->getAccessCode()) {
                    $authResult = $this->_client->authenticate($this->getAccessCode());
                    //var_dump($authResult);
                    $accessToken = $this->_client->getAccessToken();
                    //Mage::getSingleton('adminhtml/session')->setGoogleOAuthAccessToken($accessToken);
                    $config = new Mage_Core_Model_Config();
                    $config->saveConfig('googleuniversalanalytics/dashboard/access_token', $accessToken);
                    
                    $googleToken = json_decode ( $accessToken );
                    Mage::getSingleton('adminhtml/session')->setGoogleOAuthRefreshToken($googleToken->refresh_token);
                }
            } catch (Exception $e) {
                $config = new Mage_Core_Model_Config();
                $config->saveConfig('googleuniversalanalytics/dashboard/access_token', '');
                //Mage::helper('googleoauth2')->logException($e, 'gua');
            }
        }
        return $this->_client;
    }

    public function getService() {
        if(!$this->getClient()) {
            return false;
        }
        if (is_null($this->_service)) {
            $this->_service = new Google_Service_Analytics($this->getClient());
        }
        return $this->_service;
    }
    
    public function getSelectedProfile() {
        $profileId = Mage::getStoreConfig('googleuniversalanalytics/dashboard/profile');
        if(!$profileId) {
            foreach ($this->getProfiles() as $profile) {
                if($profile->getWebPropertyId() == Mage::getStoreConfig('googleuniversalanalytics/general/account')) {
                    $config = new Mage_Core_Model_Config();
                    $config->saveConfig('googleuniversalanalytics/dashboard/profile', $profile->getId());
                    return $profile->getWebPropertyId();
                }
            }
        }
        if(!$profileId) {
            foreach ($this->getProfiles() as $profile) {
                return $profile->getWebPropertyId();
            }
        }
        return $profileId;
    }

    public function getProfiles() {
        if (!$this->getService() || !$this->getService()->management_profiles) {
            return array();
        }
        try {
            $profiles = $this->getService()->management_profiles->listManagementProfiles('~all', '~all');
            return $profiles->getItems();
        } catch(Exception $e) {
            //Mage::helper('googleoauth2')->logException($e, 'gua');
        }
        return array();
    }

    public function getChartData($from, $to = false, $query = 'visitors', $projectId=false) {
        
        if(!$projectId) {
            $projectId = $this->getSelectedProfile();
        }
        if(!$projectId) {
            return false;
        }
        
        $metrics = 'ga:' . $query;

        if(!$from) {
            $from = date('Y-m-d');
        }
        if (!$to) {
            $to = $from;
        }

        if ($from == $to) {
            $dimensions = 'ga:hour';
        } else {
            $dimensions = 'ga:year,ga:month,ga:day';
        }

        $cacheId = 'gua_dash_' . $from . $to . $query . $projectId . '_cache';
        $tryCache = true;
        if ($to == date('Y-m-d') || $from == date('Y-m-d')) {
            $tryCache = false;
        }

        if ($tryCache) {
            $jsonData = Mage::app()->loadCache($cacheId);
            if($jsonData) {
                return $jsonData;
            }
        }
        try {
            $data = $this->getService()->data_ga->get('ga:' . $projectId, $from, $to, $metrics, array(
                'dimensions' => $dimensions
            ));
        } catch (Exception $e) {
            Mage::helper('googleoauth2')->logException($e, 'gua');
        }

        $jsonData = "";

        //var_dump($data);
        if ($dimensions == 'ga:hour') {
            for ($i = 0; $i < $data['totalResults']; $i ++) {
                $jsonData .= "['" . $data['rows'][$i][0] . ":00'," . round($data['rows'][$i][1], 2) . "],";
            }
        } else {
            for ($i = 0; $i < $data['totalResults']; $i ++) {
                $jsonData .= "['" . $data['rows'][$i][0] . "-" . $data['rows'][$i][1] . "-" . $data['rows'][$i][2] . "'," . round($data['rows'][$i][3], 2) . "],";
            }
        }
        
        
        if ($tryCache) {
            Mage::app()->saveCache($jsonData, $cacheId, array(
                'reports',
                'store'
            ));
        }
        //var_dump($jsonData);
        return $jsonData;
    }

}
