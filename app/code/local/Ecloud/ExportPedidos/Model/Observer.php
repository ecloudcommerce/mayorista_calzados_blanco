<?php
class Ecloud_ExportPedidos_Model_Observer
{

			public function ExportOrder(Varien_Event_Observer $observer)
			{
				$order = $observer->getEvent()->getOrder();
				$file = Mage::getModel('exporter/exportorders')->exportOrdersObserver($order);
			}
		
}
