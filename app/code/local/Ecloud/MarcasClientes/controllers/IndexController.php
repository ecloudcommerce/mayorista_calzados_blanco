<?php
class Ecloud_MarcasClientes_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$notLoggedUserGroup = 0;
		$noneGroup = -1;
		$defaultGroup = -2;

		$loggedIn = Mage::getSingleton("customer/session")->isLoggedIn();
		$customerGroupId = $loggedIn ? Mage::getSingleton("customer/session")->getCustomer()->getGroupId() : $notLoggedUserGroup;

		$allowedCategories = array();
		$categories = Mage::helper('catalog/category')->getStoreCategories();

		foreach ($categories as $cat) {
			$_cat = Mage::getModel('catalog/category')->load($cat->getId());
			$notAllowedGroups = $_cat->getData("groupscatalog2_groups");

			if ($notAllowedGroups) {
				if (in_array($defaultGroup, $notAllowedGroups)) {
					$notAllowedGroups = explode(",", Mage::getStoreConfig("netzarbeiter_groupscatalog2/general/category_default_hide"));
				}
				if (!in_array($customerGroupId, $notAllowedGroups) || in_array($noneGroup, $notAllowedGroups)) {
					$allowedCategories[] = $_cat->getUrlKey();
				}
			} else {
				$allowedCategories[] = $_cat->getUrlKey();
			}
		}

		$jsonResponse = json_encode($allowedCategories);
		$this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
		$this->getResponse()->setBody($jsonResponse);
		return;
	}
}
