<?php

$installer = $this;
$installer->startSetup();
		
$installer->addAttribute("catalog_product", "ordenapp", array(
    'label'                     => 'Orden en App',
    'note'                      => '',
    'input'                     => 'text',
    'type'                      => 'varchar',
    'class'                     => '',
    'global'                    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                   => true,
    'required'                  => false,
    'user_defined'              => false,
    'default'                   => '',
    'apply_to'                  => 'simple,configurable',
    'visible_on_front'          => false,
    'is_configurable'           => false,
    'is_html_allowed_on_front'  => false,
    'wysiwyg_enabled'           => false,
    'used_for_promo_rules'      => false,
    'used_for_sort_by'          => 0,
    'group'                     => 'General'
));
$installer->endSetup();
			 