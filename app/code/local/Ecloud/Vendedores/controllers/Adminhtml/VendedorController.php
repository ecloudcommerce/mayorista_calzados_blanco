<?php

class Ecloud_Vendedores_Adminhtml_VendedorController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
	{
		//return Mage::getSingleton('admin/session')->isAllowed('vendedores/vendedor');
		return true;
	}

	protected function _initAction()
	{
		$this->loadLayout()->_setActiveMenu("vendedores/vendedor")->_addBreadcrumb(Mage::helper("adminhtml")->__("Vendedor  Manager"),Mage::helper("adminhtml")->__("Vendedor Manager"));
		return $this;
	}
	public function indexAction() 
	{
		$this->_title($this->__("Vendedores"));
		$this->_title($this->__("Manager Vendedor"));

		$this->_initAction();
		$this->renderLayout();
	}
	public function editAction()
	{			    
		$this->_title($this->__("Vendedores"));
		$this->_title($this->__("Vendedor"));
		$this->_title($this->__("Edit Item"));
		
		$id = $this->getRequest()->getParam("id");
		$model = Mage::getModel("vendedores/vendedor")->load($id);

		if ($model->getId()) {
			Mage::register("vendedor_data", $model);
			$this->loadLayout();
			$this->_setActiveMenu("vendedores/vendedor");
			$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Vendedor Manager"), Mage::helper("adminhtml")->__("Vendedor Manager"));
			$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Vendedor Description"), Mage::helper("adminhtml")->__("Vendedor Description"));
			$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
			$this->_addContent($this->getLayout()->createBlock("vendedores/adminhtml_vendedor_edit"))->_addLeft($this->getLayout()->createBlock("vendedores/adminhtml_vendedor_edit_tabs"));
			$this->renderLayout();
		} 
		else {
			Mage::getSingleton("adminhtml/session")->addError(Mage::helper("vendedores")->__("Item does not exist."));
			$this->_redirect("*/*/");
		}
	}

	public function newAction()
	{

		$this->_title($this->__("Vendedores"));
		$this->_title($this->__("Vendedor"));
		$this->_title($this->__("New Item"));

		$id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("vendedores/vendedor")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("vendedor_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("vendedores/vendedor");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Vendedor Manager"), Mage::helper("adminhtml")->__("Vendedor Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Vendedor Description"), Mage::helper("adminhtml")->__("Vendedor Description"));


		$this->_addContent($this->getLayout()->createBlock("vendedores/adminhtml_vendedor_edit"))->_addLeft($this->getLayout()->createBlock("vendedores/adminhtml_vendedor_edit_tabs"));

		$this->renderLayout();

	}
	public function saveAction()
	{

		$post_data=$this->getRequest()->getPost();


		if ($post_data) {

			try {

				$post_data['marcas'] = implode(',',$this->getRequest()->getPost('marcas')); 	
				$model = Mage::getModel("vendedores/vendedor")
				->addData($post_data)
				->setId($this->getRequest()->getParam("id"))
				->save();


				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Vendedor was successfully saved"));
				Mage::getSingleton("adminhtml/session")->setVendedorData(false);

				if ($this->getRequest()->getParam("back")) {
					$this->_redirect("*/*/edit", array("id" => $model->getId()));
					return;
				}
				$this->_redirect("*/*/");
				return;
			} 
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
				Mage::getSingleton("adminhtml/session")->setVendedorData($this->getRequest()->getPost());
				$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
				return;
			}

		}
		$this->_redirect("*/*/");
	}



	public function deleteAction()
	{
		if( $this->getRequest()->getParam("id") > 0 ) {
			try {
				$model = Mage::getModel("vendedores/vendedor");
				$model->setId($this->getRequest()->getParam("id"))->delete();
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
				$this->_redirect("*/*/");
			} 
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
				$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
			}
		}
		$this->_redirect("*/*/");
	}

	
	public function massRemoveAction()
	{
		try {
			$ids = $this->getRequest()->getPost('ids', array());
			foreach ($ids as $id) {
				$model = Mage::getModel("vendedores/vendedor");
				$model->setId($id)->delete();
			}
			Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
		}
		catch (Exception $e) {
			Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
		}
		$this->_redirect('*/*/');
	}
	
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'vendedor.csv';
			$grid       = $this->getLayout()->createBlock('vendedores/adminhtml_vendedor_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'vendedor.xml';
			$grid       = $this->getLayout()->createBlock('vendedores/adminhtml_vendedor_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
	}
