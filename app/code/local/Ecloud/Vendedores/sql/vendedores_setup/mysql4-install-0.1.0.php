<?php

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('vendedores/vendedor'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        'auto_increment' => true,
        ), 'ID')
    ->addColumn('codigo', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(        
        'nullable'  => false,
        ), 'Codigo')
    ->addColumn('nombre', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(        
        'nullable'  => false,
        ), 'Nombre')
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Email')
    ->addColumn('marcas', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Marcas');

$installer->getConnection()->createTable($table);