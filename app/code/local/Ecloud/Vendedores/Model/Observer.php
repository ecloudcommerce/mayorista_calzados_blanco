<?php
class Ecloud_Vendedores_Model_Observer {

	public function enviarEmailVendedor($observer) {
		if(Mage::getStoreConfig('vendedores/emailvendedores/active')) {
			$order = $observer->getEvent()->getOrder();
			$store_name = Mage::app()->getStore()->getName();
			$storeId = Mage::app()->getStore()->getId();
			$vars = array('orderId' => $order->getIncrementId(), 'storename' => $store_name, 'order' => $order);

			$templateId = Mage::getStoreConfig('vendedores/emailvendedores/id_email');
			$senderName = Mage::getStoreConfig('trans_email/ident_support/name');
			$senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');

			$sender = array('name' => $senderName, 'email' => $senderEmail);
			
			$cliente = Mage::getModel('customer/customer')->load($order->getCustomerId());
			
			if($cliente->getId() && $cliente->getVendedor() != "" && is_numeric($cliente->getVendedor())) {
				$vendedor = Mage::getModel('vendedores/vendedor')->load($cliente->getVendedor());
				if($vendedor->getId()) {
					$email = Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $vendedor->getEmail(), $vendedor->getNombre(), $vars, $storeId);
				}
			}
		}
	}
}