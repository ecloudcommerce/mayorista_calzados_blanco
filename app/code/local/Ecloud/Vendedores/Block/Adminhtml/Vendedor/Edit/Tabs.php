<?php
class Ecloud_Vendedores_Block_Adminhtml_Vendedor_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("vendedor_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("vendedores")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("vendedores")->__("Item Information"),
				"title" => Mage::helper("vendedores")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("vendedores/adminhtml_vendedor_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
