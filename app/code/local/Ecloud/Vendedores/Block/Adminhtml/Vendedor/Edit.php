<?php
	
class Ecloud_Vendedores_Block_Adminhtml_Vendedor_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "vendedores";
				$this->_controller = "adminhtml_vendedor";
				$this->_updateButton("save", "label", Mage::helper("vendedores")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("vendedores")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("vendedores")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("vendedor_data") && Mage::registry("vendedor_data")->getId() ){

				    return Mage::helper("vendedores")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("vendedor_data")->getId()));

				} 
				else{

				     return Mage::helper("vendedores")->__("Add Item");

				}
		}
}