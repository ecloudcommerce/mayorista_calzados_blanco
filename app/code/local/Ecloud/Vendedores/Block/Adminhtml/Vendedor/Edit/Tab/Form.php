<?php
class Ecloud_Vendedores_Block_Adminhtml_Vendedor_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("vendedores_form", array("legend"=>Mage::helper("vendedores")->__("Item information")));

						$fieldset->addField("codigo", "text", array(
						"label" => Mage::helper("vendedores")->__("Codigo"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "codigo",
						));
					
						$fieldset->addField("nombre", "text", array(
						"label" => Mage::helper("vendedores")->__("Nombre"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "nombre",
						));
					
						$fieldset->addField("email", "text", array(
						"label" => Mage::helper("vendedores")->__("Email"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "email",
						));

						$fieldset->addField("cod_zona", "text", array(
						"label" => Mage::helper("vendedores")->__("Código de Zona"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "cod_zona",
						));

						$fieldset->addField("marcas", "multiselect", array(
						"label" => Mage::helper("vendedores")->__("Marcas"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "marcas",							
			            'values' => array(
				            	'0' => array(
				            			'label' => 'Athix', 
				            			'value' => '0'),
				            	'1' => array(
				            			'label' => 'Diadora', 
				            			'value' => '1'),
				            	'2' => array(
				            			'label' => 'Modare', 
				            			'value' => '2'),
				            	'3' => array(
				            			'label' => 'Via Marte', 
				            			'value' => '3'),
				            	'4' => array(
				            			'label' => 'Kidy', 
				            			'value' => '4'),
				            	'5' => array(
				            			'label' => 'Vanner', 
				            			'value' => '5'),
				            	'6' => array(
				            			'label' => 'Namoro', 
				            			'value' => '6'),
				            	'7' => array(
				            			'label' => 'Athix Flexy', 
				            			'value' => '7'),
				            	'8' => array(
				            			'label' => 'Athix Winter', 
				            			'value' => '8'),
				            	'9' => array(
				            			'label' => 'Athix College', 
				            			'value' => '9'),
				            	'10' => array(
				            			'label' => 'Diadora Lifestyle', 
				            			'value' => '10'),
				            	'11' => array(
				            			'label' => 'Diadora College', 
				            			'value' => '11'),
				            	'12' => array(
				            			'label' => 'Havaianas', 
				            			'value' => '12'),
				            	'13' => array(
				            			'label' => 'Hard Top', 
				            			'value' => '13'),
				            	'14' => array(
				            			'label' => 'Usaflex', 
				            			'value' => '14')
							)
						));
					

				if (Mage::getSingleton("adminhtml/session")->getVendedorData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getVendedorData());
					Mage::getSingleton("adminhtml/session")->setVendedorData(null);
				} 
				elseif(Mage::registry("vendedor_data")) {
				    $form->setValues(Mage::registry("vendedor_data")->getData());
				}
				return parent::_prepareForm();
		}
}
