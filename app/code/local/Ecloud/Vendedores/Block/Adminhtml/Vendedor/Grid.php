<?php

class Ecloud_Vendedores_Block_Adminhtml_Vendedor_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("vendedorGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("vendedores/vendedor")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("vendedores")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));
                
				$this->addColumn("nombre", array(
				"header" => Mage::helper("vendedores")->__("Nombre"),
				"index" => "nombre",
				));
				$this->addColumn("email", array(
				"header" => Mage::helper("vendedores")->__("Email"),
				"index" => "email",
				));
				$this->addColumn("codigo", array(
				"header" => Mage::helper("vendedores")->__("Codigo"),
				"index" => "codigo",
				));
				$this->addColumn("cod_zona", array(
				"header" => Mage::helper("vendedores")->__("Código de Zona"),
				"index" => "cod_zona",
				));
				// $this->addColumn("marcas", array(
				// "header" => Mage::helper("vendedores")->__("Marcas"),
				// "index" => "marcas",
				// "type"	=> "options",
				// 'options' => array(
				//       0 => $this->__('Athix'),
				//       1 => $this->__('Diadora'),
				//       2 => $this->__('Modare'),
				//       3 => $this->__('Via Marte'),
				// 	),
				// ));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_vendedor', array(
					 'label'=> Mage::helper('vendedores')->__('Remove Vendedor'),
					 'url'  => $this->getUrl('*/adminhtml_vendedor/massRemove'),
					 'confirm' => Mage::helper('vendedores')->__('Are you sure?')
				));
			return $this;
		}
			

}