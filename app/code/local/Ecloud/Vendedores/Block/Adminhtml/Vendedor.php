<?php


class Ecloud_Vendedores_Block_Adminhtml_Vendedor extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_vendedor";
	$this->_blockGroup = "vendedores";
	$this->_headerText = Mage::helper("vendedores")->__("Vendedor Manager");
	$this->_addButtonLabel = Mage::helper("vendedores")->__("Add New Item");
	parent::__construct();
	
	}

}