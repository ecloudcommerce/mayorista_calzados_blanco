<?php
class Ecloud_Vendedores_Helper_Data extends Mage_Core_Helper_Abstract {

	public function getVendedoresDropdownHtml() {
		$vendedores = Mage::getModel('vendedores/vendedor')->getCollection();

		//Armo el html del select
		$html = '<select name="vendedor_select" id="vendedor_select" class="required-entry">';
		$html .= '<option value="">Seleccione un vendedor...</option>';
		foreach ($vendedores as $vendedor) {
			$html .= '<option value="'.$vendedor->getId().'">'.$vendedor->getNombre().'</option>';
		}
		$html .= '</select>';

		return $html;
	}

	public function sendEmailVendedor($order, $cod_vendedor) {
		if(Mage::getStoreConfig('vendedores/emailvendedores/active')) {
			$store_name = Mage::app()->getStore()->getName();
			$storeId = Mage::app()->getStore()->getId();
			$vars = array('orderId' => $order->getIncrementId(), 'storename' => $store_name, 'order' => $order);

			$templateId = Mage::getStoreConfig('vendedores/emailvendedores/id_email');
			$senderName = Mage::getStoreConfig('trans_email/ident_support/name');
			$senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');

			$sender = array('name' => $senderName, 'email' => $senderEmail);
			
			$cliente = Mage::getModel('customer/customer')->load($order->getCustomerId());

			$vendedor = Mage::getModel('vendedores/vendedor')->getCollection()->addFieldToFilter('cod_zona',$cod_vendedor[0])->load();

			if($cliente->getId() && count($vendedor)) {
				if($vendedor->getFirstItem()->getCodZona()) {
					$email = Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $vendedor->getFirstItem()->getEmail(), $cliente->getName(), $vars, $storeId);
				}
			}
		}
	}
}