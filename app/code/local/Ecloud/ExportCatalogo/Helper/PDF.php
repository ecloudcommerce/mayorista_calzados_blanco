<?php
require_once(Mage::getBaseDir('lib') . '/fpdf/fpdf.php');

class Ecloud_ExportCatalogo_Helper_PDF extends FPDF
{
	protected $imgSize;
	protected $colHeight;
	protected $headerHeight;
	protected $pageMarginL;
	protected $pageMarginR;
	protected $pageMarginT;
	protected $pageMarginB;
	protected $cellMargin;
	protected $columnsWidth = [];

	public function __construct()
	{
		$orientation = "L";
		$size = "A4";
		$measureUnit = "mm";
		parent::__construct($orientation, $measureUnit, $size);
		$this->SetFillColor(0, 255, 255);
		$this->SetFont('Arial', '', 11);
		$this->imgSize = 20;
		$this->colHeight = 20;
		$this->headerHeight = 8;
		$this->pageMarginL = 10;
		$this->pageMarginR = 10;
		$this->pageMarginT = 10;
		$this->pageMarginB = 10;
		$this->cellMargin = 3;
	}

	protected function getPageMarginX()
	{
		return $this->pageMarginL + $this->pageMarginR;
	}

	protected function getPageMarginY()
	{
		return $this->pageMarginT + $this->pageMarginB;
	}

	protected function fitColumnsToData($header, $data)
	{
		for ($i = 0; $i < count($header); $i++) {
			$this->columnsWidth[] = $this->GetStringWidth($header[$i]);
		}
		// Ajusta el ancho de columnas según los datos de cada una
		foreach ($data as $product) {
			foreach ($product as $attr) {
				$atributos = array_keys($attr);
				for ($i = 0; $i < count($attr); $i++) {
					$atributo = $atributos[$i];
					if ($atributo == "img") {
						$size = $this->imgSize + $this->cellMargin;
					} else {
						$size = $this->GetStringWidth($attr[$atributo]);
					}
					if ($size > $this->columnsWidth[$i]) {
						$this->columnsWidth[$i] = $size + $this->cellMargin;
					}
				}
			}
		}
	}

	protected function fitColumnsToPage()
	{
		// Adapta las columnas al tamaño total de la hoja
		$allColumnsWidth = array_sum($this->columnsWidth);
		$pageWidth = $this->GetPageWidth() - $this->getPageMarginX();	// Ancho de página menos márgenes
		foreach ($this->columnsWidth as $key => $size) {
			$this->columnsWidth[$key] = ($pageWidth * $size) / $allColumnsWidth;
		}
	}

	protected function writeHeader($disciplina, $atributos)
	{
		// Escribe la disciplina y la cabecera de atributos
		$this->Cell(0, $this->headerHeight, $disciplina, 1, 0, "C", true);
		$this->Ln();
		for ($i = 0; $i < count($this->columnsWidth); $i++) {
			$this->Cell($this->columnsWidth[$i], $this->headerHeight, $atributos[$i], 1);
		}
	}

	protected function nextProductNeedsNewPage()
	{
		// Determina si la siguiente fila de producto se va a crear en una nueva página
		$nextProductHeight = $this->colHeight + 26;
		return ($this->getY() + $nextProductHeight) > ($this->GetPageHeight() - $this->getPageMarginY());
	}

	protected function insertImageCell($image, $columnNumber)
	{
		// Inserta una imágen
		$xInitialPosition = $this->getX();
		$cellLeftMargin = ($this->columnsWidth[$columnNumber] - $this->imgSize) / 2;
		$this->Cell($this->columnsWidth[$columnNumber], $this->imgSize + ($this->cellMargin * 2), "", 1);
		$this->Image($image, $xInitialPosition + $cellLeftMargin, $this->getY() + 4, $this->imgSize, 0);
	}

	public function reportTable($header, $data)
	{
		// Table de reporte
		$this->fitColumnsToData($header, $data);
		$this->fitColumnsToPage();

		$counter = 0;
		foreach ($data as $disciplina => $productos) {
			$this->writeHeader($disciplina, $header);
			$this->Ln();
			foreach ($productos as $producto) {
				if ($this->nextProductNeedsNewPage()) {
					// Si el producto se va a escfribir en una nueva página, escribe las cabeceras
					$this->AddPage();
					$this->writeHeader($disciplina, $header);
					$this->Ln();
				}
				$atributos = array_keys($producto);
				for ($i = 0; $i < count($this->columnsWidth); $i++) {
					// Escribe las celdas con los atributos del producto
					$atributo = $atributos[$i];
					if ($atributo == "img") {
						$this->insertImageCell($producto["img"], $i);
					} else
						$this->Cell($this->columnsWidth[$i], $this->imgSize + ($this->cellMargin * 2), $producto[$atributo], 1);
				}
				$this->Ln();
			}
			// Añade una página nueva después de cada disciplina (excepto la última)
			if (++$counter < count($data))
				$this->AddPage();
		}
	}
}
