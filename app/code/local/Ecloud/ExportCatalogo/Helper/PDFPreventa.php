<?php
require_once(Mage::getBaseDir('lib') . '/fpdf/fpdf.php');

class Ecloud_ExportCatalogo_Helper_PDFPreventa extends FPDF
{
	protected $colHeight;
	protected $textHeight;
	protected $headerHeight;
	protected $imgWidth;
	protected $imgHeight;
	protected $imgCellHeight;
	protected $pageMarginL;
	protected $pageMarginR;
	protected $pageMarginT;
	protected $pageMarginB;
	protected $cellMargin;
	protected $columnSpacing;
	protected $columnsWidth = [];

	public function __construct()
	{
		$orientation = "P";
		$size = "A4";
		$measureUnit = "mm";
		parent::__construct($orientation, $measureUnit, $size);
		$this->SetFillColor(0, 255, 255);
		$this->SetFont('Arial', '', 11);
		// $this->colHeight = 20;
		$this->textHeight = 7;
		$this->headerHeight = 7;
		$this->imgWidth = 58;
		$this->imgHeight = 46;
		$this->imgCellHeight = 45;
		// $this->headerHeight = 8;
		$this->pageMarginL = 10;
		$this->pageMarginR = 10;
		$this->pageMarginT = 10;
		$this->pageMarginB = 10;
		$this->cellMargin = 3;
		$this->columnSpacing = 10;
	}

	protected function getPageMarginX()
	{
		return $this->pageMarginL + $this->pageMarginR;
	}

	protected function getPageMarginY()
	{
		return $this->pageMarginT + $this->pageMarginB;
	}

	protected function fullWidthCell($text)
	{
		$width = ($this->GetPageWidth() - $this->getPageMarginX());
		$this->Cell($width, $this->textHeight, $text, 1);
	}

	protected function halfWidthCell($text)
	{
		$width = ($this->GetPageWidth() - $this->getPageMarginX()) / 2;
		$this->Cell($width, $this->textHeight, $text, 1);
	}

	protected function halfWidthImageCell($image)
	{
		$width = ($this->GetPageWidth() - $this->getPageMarginX()) / 2;
		$cellMarginX = ($width - $this->imgWidth) / 2;
		$cellMarginY = ($this->imgCellHeight - $this->imgHeight) / 2;
		$initialX = $this->GetX() + $cellMarginX;
		$initialY = $this->getY() + $cellMarginY;
		$this->Cell($width, $this->imgCellHeight, "");
		$this->Image($image, $initialX, $initialY, $this->imgWidth, $this->imgHeight);
	}

	protected function quarterWidthCell($text, $align = "L", $margin)
	{
		$width = ($this->GetPageWidth() - $this->getPageMarginX() - $this->columnSpacing) / 4;
		if($margin && $margin == "L"){
			$this->Cell($this->columnSpacing/2, $this->textHeight, " ", 0, 0, $align);
		}
		$this->Cell($width, $this->textHeight, $text, 0, 0, $align);
		if($margin && $margin == "R"){
			$this->Cell($this->columnSpacing/2, $this->textHeight, " ", 0, 0, $align);
		}
	}

	protected function headerCell($text, $border = 0, $img, $align = "R")
	{
		$this->MultiCell(0, $this->headerHeight, $text, $border, $align);
		try {
			if ($img !== null) {
				$cellMarginX = 0;
				$cellMarginY = 0;
				$initialX = $this->GetX() + $cellMarginX;
				$initialY = ($this->getY() + $cellMarginY) - $this->headerHeight - 10;
				$this->Image($img, $initialX, $initialY, 0, 14);
			}
		} catch (Exception $e) {
			Mage::log("Error en export de catálogo de preventa: " . $e->getMessage());
		}
	}

	protected function addTwoProducts($prod1, $prod2)
	{
		$this->halfWidthImageCell($prod1["img"]);
		$this->halfWidthImageCell($prod2["img"]);
		$this->Ln();
		$this->quarterWidthCell($prod1["sku"], "L");
		$this->quarterWidthCell($prod1["marca"] . " " . $prod1["descripcion"], "R", "R");
		$this->quarterWidthCell($prod2["sku"], "L", "L");
		$this->quarterWidthCell($prod2["marca"] . " " . $prod2["descripcion"], "R");
		$this->Ln();
		$this->quarterWidthCell("talle: " . $prod1["talle"], "L");
		$this->quarterWidthCell("$" . $prod1["costo"], "R", "R");
		$this->quarterWidthCell("talle: " . $prod2["talle"], "L", "L");
		$this->quarterWidthCell("$" . $prod2["costo"], "R");
		$this->Ln();
		$this->quarterWidthCell("color" . $prod1["currentColor"] . ": " . $prod1["color"], "L");
		$this->quarterWidthCell("", "L", "R");
		$this->quarterWidthCell("color" . $prod2["currentColor"] . ": " . $prod2["color"], "L", "L");
		$this->quarterWidthCell("", "R", "R");
		$this->Ln();
		$this->quarterWidthCell($prod1["material"], "L");
		$this->quarterWidthCell("", "L", "R");
		$this->quarterWidthCell($prod2["material"], "L", "L");
		$this->quarterWidthCell("", "L", "R");
	}

	protected function addSingleProduct($prod)
	{
		$this->halfWidthImageCell($prod["img"]);
		$this->Ln();
		$this->quarterWidthCell($prod["sku"], "L");
		$this->quarterWidthCell($prod["marca"] . " " . $prod["descripcion"], "R", "R");
		$this->Ln();
		$this->quarterWidthCell("talle: " . $prod["talle"], "L");
		$this->quarterWidthCell("$" . $prod["costo"], "R", "R");
		$this->Ln();
		$this->quarterWidthCell("color" . $prod["currentColor"] . ": " . $prod["color"], "L");
		$this->Ln();
		$this->quarterWidthCell($prod["material"], "L");
		$this->Ln();
	}

	protected function getBrandImage($brandName)
	{
		$brandCode = strtolower(str_replace(" ", "-", $brandName));
		$imageMap = array(
			// brandName => imgFile
			"athix" => "athix.png",
			"diadora" => "Logo_diadora.jpg",
			"vanner" => "vanner.jpg",
			"via-marte" => "viamarte.jpg",
			"kidy" => "kidy.jpg",
			"athix-flexy" => "flexy_logo.png",
			"diadora-lifestyle lifestyle" => "logo_diadora_life_style.jpg",
			"namoro" => "logo_namoro.jpg",
			"hard-top" => "hardtop.jpeg",
		);
		if (array_key_exists($brandCode, $imageMap)) {
			return Mage::getBaseDir("media") . "/wysiwyg/home/brands/" . $imageMap[$brandCode];
		} else {
			return null;
		}
	}

	public function reportTable($data)
	{
		/* 
		// Estructura de $data
		array(
			[Linea] => array(
				[Página] => array(
					[Producto] => array(
						[id] =>
						[img] =>
						[sku] =>
						[descripcion] =>
						[talle] =>
						[costo] =>
						[color] =>
						[material] =>
					),
					[Producto] => array(
						[id] =>
						[img] =>
						[sku] =>
						[descripcion] =>
						[talle] =>
						[costo] =>
						[color] =>
						[material] =>
					)
				),
				["header"] => array(
					[Talle1] => Cantidad
					[Talle2] => Cantidad
					[Talle3] => Cantidad
				)
			)
		) 
		*/

		$collectionMessage = Mage::getStoreConfig("export_catalogo/file_customization/collection_message");
		$counter = 0;
		foreach ($data as $linea => $paginas) {
			$headerText = $collectionMessage . "\n" . $linea;
			$headerText = utf8_decode($headerText);
			foreach ($paginas as $numPag => $pagina) {
				if ($numPag !== "header") {
					$marca = $pagina[0]["marca"];
					$this->SetFont('Arial', '', 11);
					$this->headerCell($headerText, $this->headerHeight, $this->getBrandImage($marca));
					// Cambia el tamaño de texto solo para las headers
					$this->SetFont('Arial', '', 10);
					$this->Ln();
					$skipNext = false;
					$color = 0;
					$currentColorSku = $pagina[0]["sku"];
					for ($numProd = 0; $numProd < count($pagina); $numProd += 2) {
						// Los productos se escriben de a pares en el pdf
						$producto = $pagina[$numProd];
						if ($currentColorSku == $producto["sku"]) {
							$color++;
						} else {
							$color = 1;
							$currentColorSku = $producto["sku"];
						}
						$producto["currentColor"] = $color;
						if (key_exists($numProd + 1, $pagina)) {
							$nextProd = $pagina[$numProd + 1];
							if ($currentColorSku == $nextProd["sku"]) {
								$color++;
							} else {
								$color = 1;
								$currentColorSku = $nextProd["sku"];
							}
							$nextProd["currentColor"] = $color;
							$this->addTwoProducts($producto, $nextProd);
							$skipNext = true;
						} else {
							$this->AddSingleProduct($producto);
							$skipNext = false;
						}
						$this->Ln();
						$this->Ln();
					}
					// Añade una página nueva después de cada disciplina (excepto la última)
					// if (++$counter < count($data))
					$this->AddPage();
				}
			}
		}
	}
}
