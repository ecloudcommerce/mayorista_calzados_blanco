<?php

require_once(Mage::getModuleDir('controllers','Mage_Adminhtml').DS.'Catalog'.DS.'ProductController.php');
require_once(Mage::getBaseDir('lib')  . '/PhpExcel/Classes/PHPExcel.php');
class Ecloud_ExportCatalogo_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController{

    protected function getAttributeId($attributeCode){
        
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = "SELECT `eav_attribute`.attribute_id, `eav_attribute`.backend_type, `eav_attribute`.frontend_input 
                FROM `eav_attribute`  
                WHERE `eav_attribute`.attribute_code = '".$attributeCode."' 
                AND  `eav_attribute`.entity_type_id = '4'";
        $results = $readConnection->fetchAll($query);
        if(count($results) > 0){
            return $results[0];
        }
        else {
            return null;
        }
    }

    public function getAttrDinamicProduct ($productId, $attribute_code, $attribute_type){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = "SELECT `entity_table`.value 
            FROM `catalog_product_entity_".$attribute_type."` AS `entity_table`
            WHERE `entity_table`.entity_id = '".$productId."' 
            AND `entity_table`.attribute_id = '".$attribute_code."'";

        $results = $readConnection->fetchAll($query);

        if(count($results) > 0){
            return $results[0]['value'];
        }else{
            return false;
        }
    }

    function getDdAttribute ($productId, $attribute_code, $attribute_type){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = "SELECT `eav_table`.value 
                FROM `catalog_product_entity_".$attribute_type."` AS `entity_table`  
                    INNER JOIN `eav_attribute_option_value` AS `eav_table` ON  `entity_table`.value = `eav_table`.option_id
                WHERE `entity_table`.entity_id = '".$productId."' 
                AND `entity_table`.attribute_id = '".$attribute_code."'";
        $results = $readConnection->fetchAll($query);
        if(count($results) > 0){
            return $results[0]['value'];
        }else{
            return false;
        }
    }

    public function getStock ($productId){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $stock_query = "SELECT `cataloginventory_stock_item`.`qty` 
                        FROM `cataloginventory_stock_item` 
                        WHERE `cataloginventory_stock_item`.`product_id` = '".$productId."'";
        $results = $readConnection->fetchAll($stock_query);
        if(count($results) > 0){
            return $results[0]['qty'];
        }else{
            return false;
        }
    }

    function getSku($productId) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = "SELECT `entity`.sku
        FROM `catalog_product_entity` AS `entity`
        WHERE `entity`.entity_id = '".$productId."'";
        $results = $readConnection->fetchAll($query);
    
        if(count($results) > 0){
            return $results[0]['sku'];
        }else{
            return false;
        }
    }
    function getWebsites($productId, $website_name) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $website_query = "SELECT website_id FROM `core_website` where name = '".$website_name."'";
        $result_website = $readConnection->fetchAll($website_query);
        $website_id = $result_website[0]['website_id'];
        
        $productwebsites_query = "SELECT website_id FROM `catalog_product_website` WHERE product_id = '".$productId."'";
        $results_productwebsites = $readConnection->fetchAll($productwebsites_query);
        
        if(count($results_productwebsites) > 0){
            $flat_results = (object) array('websites' => array());
            array_walk_recursive($results_productwebsites, create_function('&$v, $k, &$t', '$t->websites[] = $v;'), $flat_results);
            $product_websites = $flat_results->websites;
            if(in_array($website_id, $product_websites)){
                return true;
            }
            else{
                return false;
            }
            
        }else{
            return false;
        }
    }

    function getMediaGallery($productId) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = "SELECT `catalog_product_entity_media_gallery`.value  
            FROM `catalog_product_entity_media_gallery` 
            WHERE `entity_id` = '".$productId."'";
        $results = $readConnection->fetchAll($query);
    
        if(count($results) > 0){
            return $results[0]['value'];
        }else{
            return false;
        }
    }
    public function massExportCsvAction()
    {
        $pvp_attr = $this->getAttributeId('msrp');
        $costo_attr = $this->getAttributeId('price');
        $talle_attr = $this->getAttributeId('talle');
        $color_attr = $this->getAttributeId('color');
        $descripcion_attr = $this->getAttributeId('name');
        $disciplina_attr = $this->getAttributeId('disciplina');
        $material_attr = $this->getAttributeId('material');
        $equivalenciaEquis_attr = $this->getAttributeId('equivalencia_equis');
        $productIds = $this->getRequest()->getParam('product');
        $sku_stock = array();
        $archivo = array();
        foreach($productIds as $id){
            $row = array();
            $sku = $this->getSku($id);
            $img = $this->getMediaGallery($id);
            $img = Mage::getBaseDir('media') . DS.'catalog'.DS.'product'.$img;
            $pvp = $this->getAttrDinamicProduct($id, $pvp_attr['attribute_id'], $pvp_attr['backend_type']);
            $pvp = number_format($pvp, 0);
            $costo = $this->getAttrDinamicProduct($id, $costo_attr['attribute_id'], $costo_attr['backend_type']);
            $costo = number_format($costo, 0);
            $talle = $this->getAttrDinamicProduct($id, $talle_attr['attribute_id'], $talle_attr['backend_type']);
            $color = $this->getAttrDinamicProduct($id, $color_attr['attribute_id'], $color_attr['backend_type']);
            $descripcion = $this->getAttrDinamicProduct($id, $descripcion_attr['attribute_id'], $descripcion_attr['backend_type']);
            $material = $this->getAttrDinamicProduct($id, $material_attr['attribute_id'], $material_attr['backend_type']);
            $equivalenciaEquis = $this->getAttrDinamicProduct($id, $equivalenciaEquis_attr['attribute_id'], $equivalenciaEquis_attr['backend_type']);
            $disciplina = $this->getDdAttribute($id, $disciplina_attr['attribute_id'], $disciplina_attr['backend_type']);

            $stock = $this->getStock($id);
            $stock = number_format($stock, 0);
            $sku_stock[$sku]['antes']=$stock;

            if (!file_exists($img) || $img == "" || is_dir($img)) {
                $row['img']=Mage::getBaseDir('skin') . "/adminhtml/default/default/bl/customgrid/images/catalog/product/placeholder.jpg";
            }
            else{
                $row['img']=$img;
            }
            $row['sku']=$sku;
            $row['equivalencia_equis']=$equivalenciaEquis;
            $row['descripcion']=$descripcion;
            $row['color']=$color;
            $row['material']=$material;
            $row['talle']=$talle;
            $row['stock']=$stock;
            $row['costo']=$costo;
            $row['pvp']=$pvp;

            $archivo[$disciplina][$id]=$row;

        }

        $objPHPExcel = new PHPExcel();
        $worksheet = $objPHPExcel->getActiveSheet();
   
        $headers = array('IMAGEN', 'COD', 'COD EQUIS', 'DESCRIPCION', 'COLOR', 'MATERIAL', 'NUM', 'STOCK', '$ COSTO', '$ PVP');

        $disciplina_style = array(
            'font'  => array(
                'bold'  => true
            ),        
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFFF00')
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('rgb' => '000000')
                )
            )
        );
        $headers_style = array(
            'font'  => array(
                'bold'  => true
            ),        
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '808080')
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('rgb' => '000000')
                )
            )
        );

        $common_styles = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                )
            )
        );

        foreach($archivo as $disciplina => $products){
            $last_used=$worksheet->getHighestDataRow();
            $next_line = $last_used==1?1:$last_used+3;

            $worksheet->mergeCells('A'.$next_line.':J'.$next_line);
            $worksheet->setCellValueByColumnAndRow(0,$next_line,$disciplina)->getStyle('A'.$next_line.':J'.$next_line)
                ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $worksheet->getStyle('A'.$next_line.':J'.$next_line)->applyFromArray($disciplina_style);
            $columnHeader = 0;            
            foreach($headers as $tittle) {
                $worksheet->setCellValueByColumnAndRow($columnHeader,$next_line+1, $tittle);
                $columnHeader++;
            }
            $worksheet->getStyle('A'.($next_line+1).':J'.($next_line+1))->applyFromArray($headers_style);
            
            $start = $worksheet->getHighestDataRow()+1;
            $cell = 'A'.(string)$start;
            $worksheet->fromArray($products, NULL, $cell);

            foreach($products as $id => $data){
                $worksheet->getStyle("A".$start.":J".$start)->applyFromArray($common_styles);

                $worksheet->setCellValue('A'.$start, '');
                if(!isset($data['img']) or $data['img']==''){
                    continue;
                }
                $objDrawingPType = new PHPExcel_Worksheet_Drawing();
                $objDrawingPType->setWorksheet($objPHPExcel->getActiveSheet());
                $objDrawingPType->setName("Thumbnail");
                $objDrawingPType->setPath($data['img']);
                $objDrawingPType->setHeight(150);
                $objDrawingPType->setWidth(150);
                $objDrawingPType->setCoordinates('A'.$start);
                $objDrawingPType->setOffsetX(1);
                $objDrawingPType->setOffsetY(1);
                
                $worksheet->getRowDimension($start)->setRowHeight(150);
                $start++;
            }
        }

        $total_row=$worksheet->getHighestDataRow();
        $worksheet->getColumnDimension('A')->setWidth(50);
        foreach(range('A','J') as $columnID) {
            if($columnID !== 'A'){
                $worksheet->getColumnDimension($columnID)->setAutoSize(true);
            }
            $worksheet->getStyle($columnID.'1:'.$columnID.$total_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        }

        $filename = 'Catalogo_Excel.xls';
        // Redirigir la salida al navegador web de un cliente ( Excel5 )
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$filename");
        header('Cache-Control: max-age=0');
        // Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
        header('Cache-Control: max-age=1');

        // Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

        // Si no existe la carpeta masterdata, la creo.
        if (!file_exists('../../media/catalog')) {shell_exec('mkdir ../../media/catalog/');}

        //Creo archivo excel .xls
        $objWriter->save($_SERVER['DOCUMENT_ROOT'].DS.'media'.DS.'catalog'.DS.$filename);
        Mage::log("export de catalogo: " . new DateTime(), null, "catalog_export.log", true);
        try{
            foreach($productIds as $id){
                $sku = $this->getSku($id);
                $sku_stock[$sku]['despues']=number_format($this->getStock($id), 0);
                $sku_stock[$sku]['dif']=$sku_stock[$sku]['antes']-$sku_stock[$sku]['despues'];
                Mage::log($sku, null, "catalog_export.log", true);
                if($sku_stock[$sku]['dif']){
                    Mage::log($sku_stock[$sku], null, "catalog_export.log", true);
                }
            }
        }
        catch(Exception $e){
            Mage::log("Error recuperando stock desp de export: " . $e->getMessage(), null, "catalog_export.log", true);
        }
    }
    
    // public function massExportPrevCsvAction()
    // {
    //     $pvp_attr = $this->getAttributeId('msrp');
    //     $costo_attr = $this->getAttributeId('price');
    //     $talle_attr = $this->getAttributeId('talle');
    //     $tarea_attr = $this->getAttributeId('tarea');
    //     $color_attr = $this->getAttributeId('color');
    //     $descripcion_attr = $this->getAttributeId('name');
    //     $disciplina_attr = $this->getAttributeId('disciplina');
    //     $linea_attr = $this->getAttributeId('linea');
    //     $material_attr = $this->getAttributeId('material');
    //     $productIds = $this->getRequest()->getParam('product');
        
    //     $archivo = array();
    //     foreach($productIds as $id){
    //         if(! $this->getWebsites($id, 'preventa')){
    //             continue;
    //         }
    //         $row = array();
    //         $sku = $this->getSku($id);
    //         $img = $this->getMediaGallery($id);
    //         $img = Mage::getBaseDir('media') . DS.'catalog'.DS.'product'.$img;
    //         $pvp = $this->getAttrDinamicProduct($id, $pvp_attr['attribute_id'], $pvp_attr['backend_type']);
    //         $pvp = number_format($pvp, 0);
    //         $costo = $this->getAttrDinamicProduct($id, $costo_attr['attribute_id'], $costo_attr['backend_type']);
    //         $costo = number_format($costo, 0);
    //         $talle = $this->getAttrDinamicProduct($id, $talle_attr['attribute_id'], $talle_attr['backend_type']);
    //         $tarea = $this->getAttrDinamicProduct($id, $tarea_attr['attribute_id'], $tarea_attr['backend_type']);
    //         $color = $this->getAttrDinamicProduct($id, $color_attr['attribute_id'], $color_attr['backend_type']);
    //         $descripcion = $this->getAttrDinamicProduct($id, $descripcion_attr['attribute_id'], $descripcion_attr['backend_type']);
    //         $linea = $this->getAttrDinamicProduct($id, $linea_attr['attribute_id'], $linea_attr['backend_type']);
    //         $material = $this->getAttrDinamicProduct($id, $material_attr['attribute_id'], $material_attr['backend_type']);
    //         // $disciplina = $this->getDdAttribute($id, $disciplina_attr['attribute_id'], $disciplina_attr['backend_type']);

    //         $stock = $this->getStock($id);
    //         $stock = number_format($stock, 0);

    //         if (!file_exists($img) || $img == "" || is_dir($img)) {
    //             $row['img']=Mage::getBaseDir('skin') . "/adminhtml/default/default/bl/customgrid/images/catalog/product/placeholder.jpg";
    //         }
    //         else{
    //             $row['img']=$img;
    //         }
    //         $row1['sku']=$sku;
    //         $row1['descripcion']=$descripcion;
    //         $row2['talle']=$talle;
    //         $row2['costo']=$costo;
    //         $row3['color']=$color;
    //         $row3['material']=$material;
    //         // $row['tarea']=$tarea;
    //         // $row['stock']=$stock;
    //         // $row['pvp']=$pvp;

    //         $archivo[$linea][$id]=$row;

    //     }
                            
                        
    //     Mage::log($archivo);
    //     $objPHPExcel = new PHPExcel();
    //     $worksheet = $objPHPExcel->getActiveSheet();
   
    //     // $headers = array('IMAGEN', 'COD', 'DESCRIPCION', 'COLOR', 'MATERIAL', 'NUM', 'STOCK', '$ COSTO', '$ PVP');

    //     $disciplina_style = array(
    //         'font'  => array(
    //             'bold'  => true
    //         ),        
    //         'fill' => array(
    //             'type' => PHPExcel_Style_Fill::FILL_SOLID,
    //             'color' => array('rgb' => 'FFFF00')
    //         ),
    //         'borders' => array(
    //             'allborders' => array(
    //                 'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
    //                 'color' => array('rgb' => '000000')
    //             )
    //         )
    //     );
    //     $headers_style = array(
    //         'font'  => array(
    //             'bold'  => true
    //         ),        
    //         'fill' => array(
    //             'type' => PHPExcel_Style_Fill::FILL_SOLID,
    //             'color' => array('rgb' => '808080')
    //         ),
    //         'borders' => array(
    //             'allborders' => array(
    //                 'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
    //                 'color' => array('rgb' => '000000')
    //             )
    //         )
    //     );

    //     $common_styles = array(
    //         'borders' => array(
    //             'allborders' => array(
    //                 'style' => PHPExcel_Style_Border::BORDER_THIN,
    //                 'color' => array('rgb' => '000000')
    //             )
    //         )
    //     );

    //     foreach($archivo as $linea => $products){
    //         $last_used=$worksheet->getHighestDataRow();
    //         $next_line = $last_used==1?1:$last_used+3;

    //         $worksheet->mergeCells('A'.$next_line.':B'.$next_line);
    //         $worksheet->setCellValueByColumnAndRow(0,$next_line,'Linea: '.$linea)->getStyle('A'.$next_line.':I'.$next_line)
    //             ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
    //             ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            // $worksheet->getStyle('A'.$next_line.':B'.$next_line)->applyFromArray($disciplina_style);
            // $next_line += 1;
            // $worksheet->mergeCells('A'.$next_line.':B'.$next_line);
            // $worksheet->setCellValueByColumnAndRow(0,$next_line,$products)->getStyle('A'.$next_line.':I'.$next_line)
            //     ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            //     ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            // $worksheet->getStyle('A'.$next_line.':B'.$next_line)->applyFromArray($disciplina_style);
            // $columnHeader = 0;            
            // foreach($headers as $tittle) {
            //     $worksheet->setCellValueByColumnAndRow($columnHeader,$next_line+1, $tittle);
            //     $columnHeader++;
            // }
            // $worksheet->getStyle('A'.($next_line+1).':I'.($next_line+1))->applyFromArray($headers_style);
            
    //         $start = $worksheet->getHighestDataRow()+1;
    //         $cell = 'A'.(string)$start;
    //         $worksheet->fromArray($products, NULL, $cell);

            // foreach($products as $id => $data){
            //     $worksheet->getStyle("A".$start.":I".$start)->applyFromArray($common_styles);

            //     $worksheet->setCellValue('A'.$start, '');
            //     if(!isset($data['img']) or $data['img']==''){
            //         continue;
            //     }
            //     $objDrawingPType = new PHPExcel_Worksheet_Drawing();
            //     $objDrawingPType->setWorksheet($objPHPExcel->getActiveSheet());
            //     $objDrawingPType->setName("Thumbnail");
            //     $objDrawingPType->setPath($data['img']);
            //     $objDrawingPType->setHeight(150);
            //     $objDrawingPType->setWidth(150);
            //     $objDrawingPType->setCoordinates('A'.$start);
            //     $objDrawingPType->setOffsetX(1);
            //     $objDrawingPType->setOffsetY(1);
                
            //     $worksheet->getRowDimension($start)->setRowHeight(150);
            //     $start++;
            // }
    //     }

    //     $total_row=$worksheet->getHighestDataRow();
    //     $worksheet->getColumnDimension('A')->setWidth(50);
    //     foreach(range('A','I') as $columnID) {
    //         if($columnID !== 'A'){
    //             $worksheet->getColumnDimension($columnID)->setAutoSize(true);
    //         }
    //         $worksheet->getStyle($columnID.'1:'.$columnID.$total_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    //     }

    //     $filename = 'Catalogo_Preventa_Excel.xls';
    //     // Redirigir la salida al navegador web de un cliente ( Excel5 )
    //     header('Content-Type: application/vnd.ms-excel');
    //     header("Content-Disposition: attachment; filename=$filename");
    //     header('Cache-Control: max-age=0');
    //     // Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
    //     header('Cache-Control: max-age=1');

    //     // Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
    //     header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    //     header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    //     header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    //     header ('Pragma: public'); // HTTP/1.0

    //     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    //     $objWriter->save('php://output');

    //     // Si no existe la carpeta masterdata, la creo.
    //     if (!file_exists('../../media/catalog')) {shell_exec('mkdir ../../media/catalog/');}

    //     //Creo archivo excel .xls
    //     $objWriter->save($_SERVER['DOCUMENT_ROOT'].DS.'media'.DS.'catalog'.DS.$filename);
    // }
    
    public function massExportPdfAction()
    {
        $pvp_attr = $this->getAttributeId('msrp');
        $costo_attr = $this->getAttributeId('price');
        $talle_attr = $this->getAttributeId('talle');
        $color_attr = $this->getAttributeId('color');
        $descripcion_attr = $this->getAttributeId('name');
        $disciplina_attr = $this->getAttributeId('disciplina');
        $material_attr = $this->getAttributeId('material');
        $productIds = $this->getRequest()->getParam('product');
        Mage::log('Productos a exportar en PdfAction: ', null, "catalog_export.log", true);
        $archivo = array();
        foreach($productIds as $id){
            $sku = $this->getSku($id);
            Mage::log('SKU: '.$sku, null, "catalog_export.log", true);
            $row = array();
            $sku = $this->getSku($id);
            $img = $this->getMediaGallery($id);
            $img = Mage::getBaseDir('media') . DS.'catalog'.DS.'product'.$img;
            $pvp = $this->getAttrDinamicProduct($id, $pvp_attr['attribute_id'], $pvp_attr['backend_type']);
            $pvp = number_format($pvp, 0);
            $costo = $this->getAttrDinamicProduct($id, $costo_attr['attribute_id'], $costo_attr['backend_type']);
            $costo = number_format($costo, 0);
            $talle = $this->getAttrDinamicProduct($id, $talle_attr['attribute_id'], $talle_attr['backend_type']);
            $color = $this->getAttrDinamicProduct($id, $color_attr['attribute_id'], $color_attr['backend_type']);
            $descripcion = $this->getAttrDinamicProduct($id, $descripcion_attr['attribute_id'], $descripcion_attr['backend_type']);
            $material = $this->getAttrDinamicProduct($id, $material_attr['attribute_id'], $material_attr['backend_type']);
            $disciplina = $this->getDdAttribute($id, $disciplina_attr['attribute_id'], $disciplina_attr['backend_type']);
            $disciplina = utf8_decode($disciplina);

            if (!file_exists($img) || $img == "" || is_dir($img)) {
                $row['img']=Mage::getBaseDir('skin') . "/adminhtml/default/default/bl/customgrid/images/catalog/product/placeholder.jpg";
            }
            else{
                $row['img']=$img;
            }
            $stock = $this->getStock($id);
            $stock = number_format($stock, 0);
            Mage::log('Stock antes en foreach: '.$stock, null, "catalog_export.log", true);
            $row['sku']=$sku;
            $row['descripcion']=utf8_decode($descripcion);
            $row['color']=utf8_decode($color);
            $row['material']=$material;
            $row['talle']=$talle;
            $row['stock']=$stock;
            $row['costo']=$costo;
            $row['pvp']=$pvp;

            $archivo[$disciplina][$id]=$row;
            Mage::log('Stock despues en foreach: '.$this->getStock($id), null, "catalog_export.log", true);
        }
        $headers = array('IMAGEN', 'COD', 'DESCRIPCION', 'COLOR', 'MATERIAL', 'NUM', 'STOCK', '$ COSTO', '$ PVP');

        try{
            $pdf = Mage::helper("exportcatalogo/PDF");
            $pdf->AddPage();
            $pdf->reportTable($headers,$archivo);
            $pdf->Output("Catalogo_PDF.pdf","D");
        }
        catch(Exception $e){
            Mage::log("Exception exporting pdf: " . $e->getMessage(), null, "catalog_export.log", true);
        }
        try{
            foreach($productIds as $id){
                $sku = $this->getSku($id);
                Mage::log('SKU: '.$sku);
                Mage::log('Stock despues del export: '.$this->getStock($id), null, "catalog_export.log", true);
            }
        }
        catch(Exception $e){
            Mage::log("Error recuperando stock desp de export: " . $e->getMessage(), null, "catalog_export.log", true);
        }
    }
    public function massExportPrevPdfAction()
    {
        // Atributos
        $pvp_attr = $this->getAttributeId('msrp');
        $costo_attr = $this->getAttributeId('price');
        $talle_attr = $this->getAttributeId('talle');
        $tarea_attr = $this->getAttributeId('tarea');
        $color_attr = $this->getAttributeId('color');
        $descripcion_attr = $this->getAttributeId('name');
        $linea_attr = $this->getAttributeId('linea');
        $material_attr = $this->getAttributeId('material');
        $marca_attr = $this->getAttributeId('marca');
        $productIds = $this->getRequest()->getParam('product');

        $archivo = array();
        $pageCounter = 0;   // La página dentro de cada línea de producto
        $productCounter = 0;  // El producto dentro de cada página (0..6)
        foreach($productIds as $id){
            if(! $this->getWebsites($id, 'preventa')){
                continue;
            }
            // Valores de los atributos
            $sku = $this->getSku($id);
            $img = $this->getMediaGallery($id);
            $img = Mage::getBaseDir('media') . DS.'catalog'.DS.'product'.$img;
            $pvp = $this->getAttrDinamicProduct($id, $pvp_attr['attribute_id'], $pvp_attr['backend_type']);
            $pvp = number_format($pvp, 0);
            $costo = $this->getAttrDinamicProduct($id, $costo_attr['attribute_id'], $costo_attr['backend_type']);
            $costo = number_format($costo, 2);
            $talle = $this->getAttrDinamicProduct($id, $talle_attr['attribute_id'], $talle_attr['backend_type']);
            $color = $this->getAttrDinamicProduct($id, $color_attr['attribute_id'], $color_attr['backend_type']);
            $descripcion = $this->getAttrDinamicProduct($id, $descripcion_attr['attribute_id'], $descripcion_attr['backend_type']);
            $marca = $this->getAttrDinamicProduct($id, $marca_attr['attribute_id'], $marca_attr['backend_type']);
            $tarea = $this->getAttrDinamicProduct($id, $tarea_attr['attribute_id'], $tarea_attr['backend_type']);
            $material = $this->getAttrDinamicProduct($id, $material_attr['attribute_id'], $material_attr['backend_type']);
            $linea = $this->getAttrDinamicProduct($id, $linea_attr['attribute_id'], $linea_attr['backend_type']);


            $row = array();
            if (!file_exists($img) || $img == "" || is_dir($img)) {
                $row['img']=Mage::getBaseDir('skin') . "/adminhtml/default/default/bl/customgrid/images/catalog/product/placeholder.jpg";
            }
            else{
                $row['img']=$img;
            }
            // $stock = $this->getStock($id);
            // $stock = number_format($stock, 0);

            $row['id']=$id;
            $row['sku']=$sku;
            $row['descripcion']=$descripcion;
            $row['color']=utf8_decode($color);
            $row['material']=$material;
            $row['talle']=$talle;
            // $row['stock']=$stock;
            $row['costo']=$costo;
            // $row['pvp']=$pvp;
            $row['tarea']=$tarea;
            $row['marca']=$marca;
            $archivo[$linea][$id]=$row;

        }
        foreach ($archivo as $linea => $rows) {
            // Organiza los productos en páginas de 6 productos cada una
            $pageCounter = 0;
            $productCounter = 0;
            foreach ($rows as $idProd => $row) {
                if($productCounter == 6){
                    $pageCounter ++;
                    $productCounter = 0;
                }
                $archivo2[$linea][$pageCounter][$productCounter] = $row;
                $productCounter ++;
            }
        }

        try{
            $pdf = Mage::helper("exportcatalogo/PDFPreventa");
            $pdf->AddPage();
            $pdf->reportTable($archivo2);
            $pdf->Output("Catalogo_Preventa_PDF.pdf","D");
        }
        catch(Exception $e){
            Mage::log("Exception exporting pdf: " . $e->getMessage());
        }
    }
}