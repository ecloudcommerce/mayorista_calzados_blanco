<?php
class Ecloud_IntegracionEquis_Block_Adminhtml_Date extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $date = new Varien_Data_Form_Element_Datetime();
        $format = "dd/MM/yyyy H:m";

        $data = array(
            'name'      => $element->getName(),
            'html_id'   => $element->getId(),
            'disabled'  => 'disabled',
            'style'     => ''
        );

        $date->setData($data);
        $date->setValue($element->getValue());
        $date->setFormat($format);
        $date->setForm($element->getForm());

        return $date->getElementHtml();
    }
}
