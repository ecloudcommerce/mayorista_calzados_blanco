<?php

class Ecloud_IntegracionEquis_Block_Adminhtml_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View
{
	public function __construct()
	{
		parent::__construct();
		$this->_addButton('order_sync', array(
			'label'    => Mage::helper('sales')->__('Sincronizar con Equis'),
			'onclick'  => 'setLocation(\'' . $this->getSyncUrl() . '\')',
		));
	}

	public function getSyncUrl()
	{
		return $this->getUrl('*/ordersync');
	}
}
