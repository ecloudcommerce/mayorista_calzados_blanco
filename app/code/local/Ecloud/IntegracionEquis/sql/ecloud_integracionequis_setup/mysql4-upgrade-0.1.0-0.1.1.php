<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn(
	$installer->getTable('sales/order'),
	'equis_status',
	array(
		'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
		'length' => '50',
		'comment' => 'Estado de Equis',
		'label' => 'Estado de Equis'
	)
);

$installer->endSetup();
