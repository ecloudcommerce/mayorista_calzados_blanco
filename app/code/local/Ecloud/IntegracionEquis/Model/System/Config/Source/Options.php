<?php
class Ecloud_IntegracionEquis_Model_System_Config_Source_Options
{
	public function toOptionArray()
	{
		$options = array(
			array('value' => 'MY', 'label' => 'MY - Mayorista'),
			array('value' => 'MN', 'label' => 'MN - Minorista'),
		);
		return $options;
	}
}
