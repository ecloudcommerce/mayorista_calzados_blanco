<?php
class Ecloud_IntegracionEquis_Model_System_Config_Source_Conditions
{
	public function toOptionArray()
	{
		$conditions = array(
			array('value' => 'T', 'label' => 'T - Todos los productos'),
			array('value' => 'R', 'label' => 'R - Recientemente modificados (desde el último acceso)'),
			array('value' => 'D', 'label' => 'D - Modificados desde'),
		);
		return $conditions;
	}
}
