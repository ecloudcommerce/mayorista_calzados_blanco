<?php

class Raveinfosys_Exporter_Model_Exportorders extends Raveinfosys_Exporter_Model_Exporter
{

    const ENCLOSURE = '"';
    const DELIMITER = ';';

    public function exportOrders($orders)
    {
        if(count($orders) > 1 ){
            $fileName = 'order_export_' . date("dmY_His") . '.csv';    
        }else{
            $order_for_name = Mage::getModel('sales/order')->load($orders[0]);
            $fileName = 'order_export_'.$order_for_name->getIncrementId().'_'. date("dmY_His") . '.csv';
        }
        
        $fp = fopen(Mage::getBaseDir('export') . '/' . $fileName, 'w');

        //$this->writeHeadRow($fp);
        foreach ($orders as $order) {
            $order = Mage::getModel('sales/order')->load($order);
            $this->writeOrder($order, $fp);
        }

        fclose($fp);

        return $fileName;
    }

    public function exportOrdersObserver($order)
    {
        $fileName = 'order_export_'.$order->getIncrementId().'_'. date("dmY_His") . '.csv';
        
        $fp = fopen(Mage::getBaseDir('export') . '/' . $fileName, 'w');

        //$this->writeHeadRow($fp);
        $this->writeOrder($order, $fp);

        fclose($fp);

        return $fileName;
    }

    protected function writeHeadRow($fp)
    {
        fputcsv($fp, $this->getHeadRowValues(), self::DELIMITER, self::ENCLOSURE);
    }

    protected function writeOrder($order, $fp)
    {
        //Obtengo campos del Customer
        $customers = $this->getCustomerOrderValues($order);
        //Obtengo campos del Comunes
        $common = $this->getCommonOrderValues($order);
        $blank = $this->getBlankOrderValues($order);

        $orderItems = $order->getItemsCollection();

        $itemInc = 0;
        $data = array();
        $count = 0;
        $catalog_product = Mage::getModel('catalog/product');
        $vendedor = 0;
        $contador = 0;
        $pedido_web = false;

        $vendedor = $this->getVendedorOrderValues($order);

        //Chequeo si es pedido website o app. Los pedidos app son de storeid = 0
        if($order->getStoreId() != 0){
            $pedido_web = true;
        } 

        foreach ($orderItems as $item) {

            $contador++;

            $product = $catalog_product->loadByAttribute('sku',$this->getItemSku($item));

            //Si es Web, busco la marca del primer producto para definir el Vendedor
            if($contador == 1 && $pedido_web){

                $customer = Mage::getModel("customer/customer")->setWebsiteId(1)->loadByEmail($order->getCustomerEmail());

                if($product->getAttributeText('manufacturer')){
                    switch ($product->getAttributeText('manufacturer')) {
                        case "Via Marte":
                            $vendedor = $customer->getVendedorCalzados();
                            break;
                        case "Modare":
                            $vendedor = $customer->getVendedorCalzados();
                            break;
                        case "Namoro":
                            $vendedor = $customer->getVendedorCalzados();  
                            break;
                        case "Kidy":
                            $vendedor = $customer->getVendedorCalzados();  
                            break;
                        case "Vanner":
                            $vendedor = $customer->getVendedorCalzados(); 
                            break;
                        case "Hard Top":
                            $vendedor = $customer->getVendedorCalzados(); 
                            break;
                        case "Diadora":
                            $vendedor = $customer->getVendedorDiadora(); 
                            break;
                        case "Diadora College":
                            $vendedor = $customer->getVendedorDiadora(); 
                            break;
                        case "Diadora Lifestyle":
                            $vendedor = $customer->getVendedorDiadora(); 
                            break;
                        case "Athix":
                            $vendedor = $customer->getVendedorAthix(); 
                            break;
                        case "Athix College":
                            $vendedor = $customer->getVendedorAthix(); 
                            break;
                        case "Athix Flexy":
                            $vendedor = $customer->getVendedorAthix(); 
                            break;
                        case "Athix Winter":
                            $vendedor = $customer->getVendedorAthix(); 
                            break;
                        case "Usaflex":
                            $vendedor = $customer->getVendedorAthix(); 
                            break;
                        default:
                            $vendedor = '1111'; 
                            break;
                    }
                }else{
                    if($product->getData('marca') != ''){
                       switch ($product->getData('marca')) {
                           case "Via Marte":
                               $vendedor = $customer->getVendedorCalzados();
                               break;
                           case "Modare":
                               $vendedor = $customer->getVendedorCalzados();
                               break;
                           case "Namoro":
                               $vendedor = $customer->getVendedorCalzados();  
                               break;
                           case "Kidy":
                               $vendedor = $customer->getVendedorCalzados();  
                               break;
                           case "Vanner":
                               $vendedor = $customer->getVendedorCalzados(); 
                               break;
                            case "Hard Top":
                                $vendedor = $customer->getVendedorCalzados(); 
                                break;
                           case "Diadora":
                               $vendedor = $customer->getVendedorDiadora(); 
                               break;
                           case "Diadora College":
                               $vendedor = $customer->getVendedorDiadora(); 
                               break;
                           case "Diadora Lifestyle":
                               $vendedor = $customer->getVendedorDiadora(); 
                               break;
                           case "Athix":
                               $vendedor = $customer->getVendedorAthix(); 
                               break;
                           case "Athix College":
                               $vendedor = $customer->getVendedorAthix(); 
                               break;
                           case "Athix Flexy":
                               $vendedor = $customer->getVendedorAthix(); 
                               break;
                           case "Athix Winter":
                               $vendedor = $customer->getVendedorAthix(); 
                               break;
                            case "Usaflex":
                               $vendedor = $customer->getVendedorAthix(); 
                               break;
                            default:
                                $vendedor = '1111'; 
                                break;
                       } 
                    }
                }

                //Chequeo si no es ninguna de las marcas, trae el CP de Billing en un array (App). Si es, pasa dentro de un array.
                if(is_array($vendedor)){
                    $vendedor = array('0');
                }else{
                    $vendedor = array($vendedor);    
                }
                
            }

            $record = array_merge($customers,$this->getOrderItemValues($item, $order, ++$itemInc, $product),$common,$vendedor);
            fputcsv($fp, $record, self::DELIMITER, self::ENCLOSURE);
            $count++;
        }

        //Enviar mail al vendedor
        if(is_array($vendedor)){
            Mage::helper("vendedores")->sendEmailVendedor($order, $vendedor);
        }

    }

    protected function getHeadRowValues()
    {
        return array(
            "cod_cta",
            "cod_articulo",
            "cantidad_modulos",
            "cod_color",
            "cod_material",
            "cod_tarea",
            "nro_pedido",
            "fecha",
            "cod_vendedor",
            "preventa"
        );
    }

    protected function getCustomerOrderValues($order)
    {
        $cod_cta = '';
        $customer = Mage::getModel("customer/customer")->setWebsiteId(1)->loadByEmail($order->getCustomerEmail());
        if($customer->getCodCta() != ''){
            $cod_cta = $customer->getCodCta();
        }else{
           $customer = Mage::getModel("customer/customer")->setWebsiteId(0)->loadByEmail($order->getCustomerEmail()); 
           $cod_cta = $customer->getCodCta();
        }

        if($cod_cta == null){
            $cod_cta = '9999';
        }

        return array(
            $cod_cta
        );
    }

    //Common orders value

    protected function getCommonOrderValues($order)
    {

        $order_date = substr($order->getData('created_at'),0,strrpos($order->getData('created_at'),' '));
        $array_date = explode('-', $order_date);
        $result_date = $array_date[2].'/'.$array_date[1].'/'.$array_date[0];

        return array(
            $order->getIncrementId(),
            $result_date
        );
    }

    protected function getVendedorOrderValues($order)
    {   
        if($order->getBillingAddress() != ''){
            return array(
                $order->getBillingAddress()->getPostcode()
            );    
        }else{
            $null = '0';
            return array(
            $null
            );
        }
        
    }

    protected function getBlankOrderValues($order)
    {
        return array(
            '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
            '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
            '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
    }

    //To return the array of ordered items
    protected function getOrderItemValues($item, $order, $itemInc = 1, $product)
    {
        $codigo = $this->getItemSku($item);
        $cod_col = '';
        $cod_mat = '';
        $cod_tarea = '';
        $preventa = '';
        if($product != ''){
            if($product->getCodigo() != ''){
                $codigo = $product->getCodigo();    
            }
            $cod_col = $product->getCodCol();
            $cod_mat = $product->getCodMat();
            $preventa = $product->getPresale();
            $cod_tarea = $product->getCodTarea();
        }
        return array(
            $codigo,
            (int) $item->getQtyOrdered(),
            $cod_col,
            $cod_mat,
            $preventa,
            $cod_tarea
        );
    }

}
