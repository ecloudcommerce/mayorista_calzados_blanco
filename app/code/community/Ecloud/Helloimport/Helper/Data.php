<?php

class Ecloud_Helloimport_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    protected $_texturePath;
    
    public function __construct()
    {
        $this->_texturePath = 'wysiwyg/helloimport/texture/default/';
    }

    public function getCfgGroup($group, $storeId = NULL)
    {
        if ($storeId)
            return Mage::getStoreConfig('helloimport/' . $group, $storeId);
        else
            return Mage::getStoreConfig('helloimport/' . $group);
    }
    
    public function getCfgSectionDesign($storeId = NULL)
    {
        if ($storeId)
            return Mage::getStoreConfig('helloimport_design', $storeId);
        else
            return Mage::getStoreConfig('helloimport_design');
    }

    public function getCfgSectionSettings($storeId = NULL)
    {
        if ($storeId)
            return Mage::getStoreConfig('helloimport_settings', $storeId);
        else
            return Mage::getStoreConfig('helloimport_settings');
    }
    
    public function getTexturePath()
    {
        return $this->_texturePath;
    }

    public function getCfg($optionString)
    {
        return Mage::getStoreConfig('helloimport_settings/' . $optionString);
    }
}
