<?php 
    class Ecloud_Helloimport_Adminhtml_ImportController extends Mage_Adminhtml_Controller_Action{ 
        public function indexAction() {
            $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/helloimport_settings/"));
        }
        public function blocksAction() {
            $isoverwrite = Mage::helper('helloimport')->getCfg('install/overwrite_blocks');
            Mage::getSingleton('helloimport/import_cms')->importCms('cms/block', 'blocks', $isoverwrite);
            $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/helloimport_settings/"));
        }
        public function pagesAction() {
            $isoverwrite = Mage::helper('helloimport')->getCfg('install/overwrite_pages');
            Mage::getSingleton('helloimport/import_cms')->importCms('cms/page', 'pages', $isoverwrite);
            $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/helloimport_settings/")); 
        }
    }
?>