<?php
require_once 'processor.php';
class Bl_Error_Processor extends Error_Processor
{    
    public function __construct()
    {
		parent::__construct();
        $_skin =  (!empty($_SERVER['MAGE_RUN_CODE'])) ? $_SERVER['MAGE_RUN_CODE'] : 'base';
        $this->_setSkin($_skin);
    }
    

    /**
     * Process 503 error. Modificacion para enviarle un codigo 307 en lugar de 503. De esta forma NO lo toma varnish
    */
    public function process503()
    {
        $this->pageTitle = 'Error 503: Service Unavailable';
        //$this->_sendHeaders(503);
        $this->_sendHeaders(307);
        $this->_renderPage('503.phtml');
    }

    /**
     * Process report. Modificacion para enviarle un codigo 307 en lugar de 503. De esta forma NO lo toma varnish
    */
    public function processReport()
    {
        $this->pageTitle = 'There has been an error processing your request';
        //$this->_sendHeaders(503);
        $this->_sendHeaders(307);

        $this->showErrorMsg = false;
        $this->showSentMsg  = false;
        $this->showSendForm = false;
        $this->reportAction = $this->_config->action;
        $this->_setReportUrl();

        if($this->reportAction == 'email') {
            $this->showSendForm = true;
            $this->sendReport();
        }
        $this->_renderPage('report.phtml');
    }
}
